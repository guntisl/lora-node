

find_package(Git)
if(GIT_FOUND AND EXISTS "${CMAKE_SOURCE_DIR}/.git")
    add_custom_target(make_version_str
            COMMAND ${CMAKE_SOURCE_DIR}/get_version_str.sh git
            ${CMAKE_SOURCE_DIR}/${SOURCE_DIR}/version_str.h
            COMMENT "Generate version string")
else()
    message(WARNING "Git not found, cannot set version info")
    add_custom_target(make_version_str
            COMMAND ${CMAKE_SOURCE_DIR}/get_version_str.sh no_git
            ${CMAKE_SOURCE_DIR}/${SOURCE_DIR}/version_str.h
            COMMENT "Generate version string")
endif()