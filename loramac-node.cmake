
SET(LORAMAC_DEFS
        -DREGION_EU868
        -DSX1261MBXBAS
        -DUSE_MODEM_FSK)

SET(LORAMAC_INC
        ${LORAMAC_DIR}/src/radio
        ${LORAMAC_DIR}/src/system
        ${LORAMAC_DIR}/src/mac
        ${LORAMAC_DIR}/src/boards)

SET(LORAMAC_SRC
        ${LORAMAC_DIR}/src/radio/sx126x/radio.c
        ${LORAMAC_DIR}/src/radio/sx126x/sx126x.c
        ${LORAMAC_DIR}/src/mac/region/Region.c
        ${LORAMAC_DIR}/src/mac/region/RegionCommon.c
        ${LORAMAC_DIR}/src/mac/region/RegionEU868.c
        ${LORAMAC_DIR}/src/mac/region/RegionUS915.c
        ${LORAMAC_DIR}/src/mac/LoRaMacClassB.c
        ${LORAMAC_DIR}/src/mac/LoRaMac.c
        ${LORAMAC_DIR}/src/mac/LoRaMacAdr.c
        ${LORAMAC_DIR}/src/mac/LoRaMacConfirmQueue.c
        ${LORAMAC_DIR}/src/mac/LoRaMacFCntHandler.c
        ${LORAMAC_DIR}/src/mac/LoRaMacCrypto.c
        ${LORAMAC_DIR}/src/mac/LoRaMacCommands.c
        ${LORAMAC_DIR}/src/mac/LoRaMacSerializer.c
        ${LORAMAC_DIR}/src/mac/LoRaMacParser.c
        ${LORAMAC_DIR}/src/system/delay.c
        ${LORAMAC_DIR}/src/system/gpio.c
        ${LORAMAC_DIR}/src/system/nvmm.c
        ${LORAMAC_DIR}/src/peripherals/soft-se/aes.c
        ${LORAMAC_DIR}/src/peripherals/soft-se/cmac.c
        ${LORAMAC_DIR}/src/peripherals/soft-se/soft-se.c)
