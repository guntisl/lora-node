
# Common cmake functions
function(COPY_POST target src dst)
    add_custom_command(TARGET ${target} POST_BUILD
            COMMAND cp ${src} ${dst})
endfunction()

function(COPY_PRE target src dst)
    add_custom_command(TARGET ${target} PRE_BUILD
            COMMAND cp ${src} ${dst})
endfunction()


function(GEN_IHEX target src dst)
    add_custom_command(TARGET ${target} POST_BUILD
            COMMAND ${TOOLCHAIN_DIR}/arm-none-eabi-objcopy -O ihex ${src} ${dst}
            COMMENT "Generate ihex for target '${target}'")
endfunction()

function(GEN_BIN target src dst)
    add_custom_command(TARGET ${target} POST_BUILD
            COMMAND ${TOOLCHAIN_DIR}/arm-none-eabi-objcopy
                -O binary ${src} ${dst}
            COMMENT "Generate binary image for target '${target}'")
endfunction()

function(GEN_ASM target src dst)
    add_custom_command(TARGET ${target} POST_BUILD
            COMMAND ${TOOLCHAIN_DIR}/arm-none-eabi-objdump
                -l -S -C ${src} > ${dst}
            COMMENT "Generate disassembly file for '${target}'")
endfunction()

function(GEN_SYM_LIST target src dst)
    add_custom_command(TARGET ${target} POST_BUILD
            COMMAND ${TOOLCHAIN_DIR}/arm-none-eabi-nm --print-size --size-sort
                --radix=d -C ${src} > ${dst}
            COMMENT "Generate sorted symbol list for '${target}'")
endfunction()

function(PRINT_SECT_SIZES target)
    add_custom_command(TARGET ${target} POST_BUILD
            COMMAND ${TOOLCHAIN_DIR}/arm-none-eabi-size
                -A ${target} | grep -e .text -e  .data -e .bss -e .heap
                -e .noinit -e .stack
            COMMENT "Executable section data for '${target}':")
endfunction()

function(PRINT_FILE_SIZE target file msg)
    add_custom_command(TARGET ${target} POST_BUILD
            COMMAND wc -c ${file}
            COMMENT "${msg}")
endfunction()


function (CREATE_TARGET
        target_id
        exclude_all
        defs
        c_flags
        cxx_flags
        link_flags
        libs
        depend
        out_dir
        include
        src)

    SET(MAP_FILE    ${target_id}.map)

    if(exclude_all)
        SET(EXCL_FLAG       EXCLUDE_FROM_ALL)
    endif()
    # create target itself
    add_executable(${target_id} ${EXCL_FLAG} ${src})

    target_compile_definitions(${target_id} PRIVATE ${defs})

    # C specific compile flags
    foreach(flag ${c_flags})
        target_compile_options(${target_id} PRIVATE
                $<$<COMPILE_LANGUAGE:C>:${flag}>)
    endforeach(flag)

    # C++ specific compile flags
    foreach(flag ${cxx_flags})
        target_compile_options(${target_id} PRIVATE
                $<$<COMPILE_LANGUAGE:CXX>:${flag}>)
    endforeach(flag)

    set_property(TARGET ${target_id} APPEND_STRING
            PROPERTY LINK_FLAGS "-Wl,-Map=${MAP_FILE} ${link_flags}")

    target_link_libraries(${target_id} ${libs})

    target_include_directories(${target_id} PRIVATE ${include})

    if(depend)
        add_dependencies(${target_id} ${depend})
    endif()

    message("-------------------------------------")
    message("Target        : ${target_id}")
    message("c flags       : ${c_flags}")
    message("c++ flags     : ${cxx_flags}")
    message("link flags    : ${link_flags}")
    message("definitions   : ${defs}")
    message("libs          : ${libs}")
    message("includes      : ${include}")
    message("src           : ${src}")
    message("-------------------------------------")

    SET(TGT_OUT             ${out_dir}/${target_id})
    SET(TGT_ELF             ${PROJECT_BINARY_DIR}/${target_id})
    SET(TGT_MAP             ${PROJECT_BINARY_DIR}/${MAP_FILE})

            # make dir to place output of target
    file(MAKE_DIRECTORY     ${TGT_OUT})

    # copy binary file
    COPY_POST(${target_id} ${TGT_ELF} ${TGT_OUT}/${target_id}.elf)

    # copy map file
    COPY_POST(${target_id} ${TGT_MAP} ${TGT_OUT}/${MAP_FILE})

    # make iHex file
    GEN_IHEX(${target_id} ${TGT_ELF} ${TGT_OUT}/${target_id}.hex)

    # make binary file
    GEN_BIN(${target_id} ${TGT_ELF} ${TGT_OUT}/${target_id}.bin)

    # make disassm
    GEN_ASM(${target_id} ${TGT_ELF} ${TGT_OUT}/${target_id}.lst)

    # make symbol list
    GEN_SYM_LIST(${target_id} ${TGT_ELF} ${TGT_OUT}/${target_id}.symbols)

    # print sections
    PRINT_SECT_SIZES(${target_id})

    # print image size
    PRINT_FILE_SIZE(${target_id} ${TGT_OUT}/${target_id}.bin "FLASH image size:")

endfunction()
























