#!/bin/sh
cd ~/
openocd -f /usr/share/openocd/scripts/interface/stlink.cfg -c 'transport select hla_swd' -f /usr/share/openocd/scripts/target/stm32wbx.cfg -c "program /home/pi/lora-node/out/lora_node_release/lora_node_release.elf" -c "reset" -c "exit"
