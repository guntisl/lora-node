
#ifndef dev_console_h
#define dev_console_h

#include "terminal.h"

#ifdef EN_SERIAL_CONSOLE
extern term::Terminal serial_console;
#endif

#endif
