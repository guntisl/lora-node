
#ifndef eeprom_emulator_h
#define eeprom_emulator_h


#include <cstdint>
#include <cstdlib>
#include <limits>
#include <cassert>
#include <cstring>
#include "internal_flash.h"
#include "eeprom_emulator.h"
#include "debug_log.h"

// TODO: implement full record address cache (optional with switch)

#define VERSION_CODE                    0x01
#define ACTIVE_BLOCK_CODE               0x4341
#define OLD_BLOCK_CODE                  0x0000000000444c4f


struct BlockHeader {
    struct ActiveHeader {
        uint16_t ver_code;
        uint16_t page_size;
        uint16_t active_code;
        uint8_t block_size_pages;
        uint8_t rec_usage_percent;
    }__attribute__((packed)) active;
    uint64_t old;
}__attribute__((packed));

static_assert((sizeof(BlockHeader::active) % MCU_PROG_WORD_SIZE) == 0,
              "bad header member size must be multiple of PROG_WORD_SIZE");

static_assert((sizeof(BlockHeader) % MCU_PROG_WORD_SIZE) == 0,
              "bad header size, size must be multiple of PROG_WORD_SIZE");

struct MemRecord {
    uint16_t virtual_addr;
    uint8_t data[6];
}__attribute__((packed));

static_assert((sizeof(MemRecord) % MCU_PROG_WORD_SIZE) == 0,
              "bad memory record size, size must be multiple of PROG_WORD_SIZE");

template<size_t PAGE_CNT, size_t PAGE_SIZE, size_t USABLE_PERCENT>
class Block {
    size_t rec_idx;
    size_t my_index;
    uint8_t *block_address;
    bool active_block;
    bool old_block;
    uint8_t *phy_mem_start;

    struct RecCacheLine {
        decltype(MemRecord::virtual_addr) vma;      //!< virtual memory address
        uint8_t *lma;                               //!< logical memory address
    };

    RecCacheLine rec_addr_cache;

    uint8_t *addrFromIndex(size_t index) {
        return block_address + sizeof(BlockHeader) + index * sizeof(MemRecord);
    }

public:
    static constexpr size_t max_rec_count =
        (PAGE_SIZE - sizeof(BlockHeader)) * PAGE_CNT / sizeof(MemRecord);

    static constexpr size_t rec_data_size = sizeof(MemRecord::data);
    static_assert(USABLE_PERCENT >= 50 && USABLE_PERCENT <= 90,
                  "valid range is 50..90%");
    static constexpr size_t usable_rec_count = max_rec_count * USABLE_PERCENT
                                               / 100;

    Block() = default;

    void init(size_t block_index, uint8_t *mem_start) {
        rec_idx = 0;
        phy_mem_start = mem_start;
        my_index = block_index;
        resetCache();
        MSG_DEBUG(CONF, "block %ld init", my_index);
        block_address = phy_mem_start + my_index * PAGE_SIZE * PAGE_CNT;

        // parse block header
        MSG_INFO(CONF, "reading state");
        BlockHeader header;
        memcpy((uint8_t *) &header, block_address, sizeof(header));

        if (header.active.ver_code == VERSION_CODE) {
            if (header.active.block_size_pages == PAGE_CNT) {
                if (header.active.rec_usage_percent == USABLE_PERCENT) {
                    if (header.active.page_size == PAGE_SIZE) {
                        active_block = (header.active.active_code
                                        == ACTIVE_BLOCK_CODE);
                        old_block = (header.old == OLD_BLOCK_CODE);
                        if (active_block) {
                            while (rec_idx < max_rec_count) {
                                // read records and check if valid
                                auto address = addrFromIndex(rec_idx);
                                MemRecord rec = readRec(address);
                                if (isErasedRec(rec)) {
                                    break;
                                }
                                assert(isValidRec(rec));
                                rec_idx++;
                            }
                            if (old_block) {
                                assert(rec_idx == max_rec_count);
                                MSG_INFO(CONF, "block %ld is old", my_index);
                            }
                            else {
                                MSG_INFO(CONF, "block %ld is active", my_index);
                            }
                            MSG_INFO(CONF, "block %ld record index %d",
                                     my_index, rec_idx);
                            return;
                        }
                        else {
                            MSG_ERROR(CONF, "block %ld no active block",
                                      my_index);
                        }
                    }
                    else {
                        MSG_ERROR(CONF, "block %ld different page size",
                                  my_index);
                    }
                }
                else {
                    MSG_ERROR(CONF, "block %ld different usage percent",
                              my_index);
                }
            }
            else {
                MSG_ERROR(CONF, "block %ld different block size", my_index);
            }
        }
        else {
            if (header.active.ver_code !=
                std::numeric_limits<decltype(header.active.ver_code)>::max()) {

                MSG_ERROR(CONF, "block %ld different version code", my_index);
            }
            else {
                if(verifyIsErased()) {
                    MSG_INFO(CONF, "block %ld is erased", my_index);
                    return;
                }
            }
        }
        MSG_INFO(CONF, "erasing block %ld", my_index);
        erase();
    }

    bool verifyIsErased() {
        auto word = (uint32_t *) block_address;
        for(size_t i = 0; i < PAGE_SIZE * PAGE_CNT; i += sizeof(*word)) {
            if(*word != std::numeric_limits<uint32_t>::max()) {
                return false;
            }
            word++;
        }
        return true;
    }

    void resetCache() {
        MSG_DEBUG(CONF, "reset cache");
        rec_addr_cache.vma = std::numeric_limits<decltype(rec_addr_cache.vma)>::max();
    }

    void updateCache(uint8_t *rec_lma, decltype(MemRecord::virtual_addr) rec_vma) {
        MSG_DEBUG(CONF, "update cache");
        rec_addr_cache.vma = rec_vma;
        rec_addr_cache.lma = rec_lma;
    }

    uint8_t *searchCache(decltype(MemRecord::virtual_addr) rec_vma) {
        if (rec_addr_cache.vma == rec_vma) {
            MSG_DEBUG(CONF, "cache hit");
            return rec_addr_cache.lma;
        }
        return nullptr;
    }

    void erase() {
        MSG_DEBUG(CONF, "block %ld erase", my_index);
        for (size_t i = 0; i < PAGE_CNT; i++) {
            flash::erasePage((uint32_t) (block_address + i * MCU_PAGE_SIZE));
        }
        active_block = false;
        old_block = false;
        rec_idx = 0;
        resetCache();
    }

    void setActive() {
        MSG_DEBUG(CONF, "block %ld set active", my_index);
        decltype(BlockHeader::active) active;
        active.active_code = ACTIVE_BLOCK_CODE;
        active.ver_code = VERSION_CODE;
        active.page_size = PAGE_SIZE;
        active.block_size_pages = PAGE_CNT;
        active.rec_usage_percent = USABLE_PERCENT;
        flash::write((uint32_t) (block_address + offsetof(BlockHeader, active)),
                     (uint8_t *) &active, sizeof(active));
        active_block = true;
    }

    void setOld() {
        MSG_DEBUG(CONF, "block %ld set old", my_index);
        decltype(BlockHeader::old) old_code = OLD_BLOCK_CODE;
        flash::write((uint32_t) (block_address + offsetof(BlockHeader, old)),
                     (uint8_t *) &old_code, sizeof(old_code));
        old_block = true;
    }

    void writeRec(MemRecord &rec) {
        MSG_DEBUG(CONF, "block %ld write rec, vma <%04X>", my_index,
                  rec.virtual_addr);
        assert(!isFull());
        auto address = addrFromIndex(rec_idx);
        flash::write((uint32_t) address, (uint8_t *) &rec, sizeof(rec));
        rec_idx++;
        updateCache(address, rec.virtual_addr);
    }

    bool isValidRec(MemRecord &record) {
        return record.virtual_addr < usable_rec_count * sizeof(MemRecord::data);
    }

    bool isErasedRec(MemRecord &record) {
        return record.virtual_addr == std::numeric_limits<decltype(MemRecord::virtual_addr)>::max();
    }

    MemRecord popTopRec() {
        assert(rec_idx > 0);
        resetCache();
        rec_idx--;
        auto address = addrFromIndex(rec_idx);
        MemRecord rec = readRec(address);
        MSG_DEBUG(CONF, "block %ld pop rec <%04X>", my_index, rec.virtual_addr);
        return rec;
    }

    uint8_t *getRecLMA(uint32_t rec_vma) {
        MSG_DEBUG(CONF, "block %ld searching for rec vma <%04X>", my_index, rec_vma);
        // check cache for record
        if (auto lma = searchCache(rec_vma)) {
            return lma;
        }
        // find record with address searching back from index till start
        auto idx = rec_idx;
        while (idx-- > 0) {
            auto rec_lma = addrFromIndex(idx);
            uint16_t rec_virtual_addr;

            memcpy((uint8_t *) &rec_virtual_addr,
                   rec_lma + offsetof(MemRecord, virtual_addr),
                   sizeof(rec_virtual_addr));

            if (rec_vma == rec_virtual_addr) {
                MSG_DEBUG(CONF, "rec found at lma %08X", rec_lma);
                updateCache(rec_lma, rec_vma);
                return rec_lma;
            }
        }
        MSG_DEBUG(CONF, "no record found");
        return nullptr;
    }

    MemRecord readRec(uint8_t *rec_lma) {
        MemRecord rec;
        memcpy((uint8_t *) &rec, rec_lma, sizeof(rec));
        return rec;
    }

    size_t getIndex() {
        return my_index;
    }

    bool isEmpty() {
        return rec_idx == 0;
    }

    bool isFull() {
        return rec_idx == max_rec_count;
    }

    bool isErased() {
        return !isActive() && !isOld();
    }

    bool isOld() {
        return active_block && old_block;
    }

    bool isActive() {
        return active_block && !old_block;
    }
};

/**
 * @tparam BLCK_SIZE Block size as number of flash pages [min = 1]
 * @tparam BLCK_COUNT Total number of blocks [min = 2]
 */
template<size_t BLCK_SIZE, size_t BLCK_COUNT, size_t PAGE_SIZE, size_t BLCK_REC_UASEG>
class EepromEmulator {
    Block<BLCK_SIZE, PAGE_SIZE, BLCK_REC_UASEG> *active_block;
    Block<BLCK_SIZE, PAGE_SIZE, BLCK_REC_UASEG> *old_block;
    Block<BLCK_SIZE, PAGE_SIZE, BLCK_REC_UASEG> blocks[BLCK_COUNT];
    uint8_t *mem_address;

    static constexpr size_t max_usable_recs = Block<BLCK_SIZE, PAGE_SIZE, BLCK_REC_UASEG>::usable_rec_count;
    static constexpr size_t recs_per_block = Block<BLCK_SIZE, PAGE_SIZE, BLCK_REC_UASEG>::max_rec_count;

    static constexpr size_t usable_mem_size = max_usable_recs
                                              * sizeof(MemRecord::data);

    static constexpr size_t new_recs_per_old_block = recs_per_block
                                                     - max_usable_recs;
    static constexpr size_t remainder = recs_per_block % new_recs_per_old_block;
    static constexpr size_t old_recs_per_new = recs_per_block
                                               / new_recs_per_old_block
                                               + (remainder == 0 ? 0 : 1);

    Block<BLCK_SIZE, PAGE_SIZE, BLCK_REC_UASEG> *getActiveBlock() {
        return active_block;
    }

    Block<BLCK_SIZE, PAGE_SIZE, BLCK_REC_UASEG> *getOldBlock() {
        return old_block;
    }

    void makeNewActiveBlock() {
        assert(!old_block);
        active_block->setOld();
        old_block = active_block;
        auto index = old_block->getIndex();
        index++;
        if (index >= BLCK_COUNT) {
            index = 0;
        }
        assert(blocks[index].isErased());
        active_block = &blocks[index];
        active_block->setActive();
    }

    void eraseOldBlock() {
        getOldBlock()->erase();
        old_block = nullptr;
    }

    constexpr size_t oldRecsPerNew() {
        return old_recs_per_new;
    }

public:
    EepromEmulator(uint8_t *address) noexcept
        : active_block(nullptr), old_block(nullptr), mem_address(address) {

        static_assert(BLCK_SIZE >= 1, "minimal block size 1 flash memory page");
        static_assert(BLCK_COUNT >= 2, "minimum 2 blocks needed");

        reInit();
    }

    void reInit() {
        active_block = nullptr;
        old_block = nullptr;

        MSG_INFO(CONF, "init eeprom emulator");
        MSG_INFO(CONF, "usable memory: %d", usable_mem_size);
        MSG_INFO(CONF, "records in block: %d", recs_per_block);
        MSG_INFO(CONF, "old rec tests per new write: %d", old_recs_per_new);

        for (size_t i = 0; i < BLCK_COUNT; i++) {
            blocks[i].init(i, mem_address);
        }

        bool bad_state = false;
        for (size_t i = 0; i < BLCK_COUNT; i++) {
            if (blocks[i].isActive()) {
                if (active_block != nullptr) {
                    bad_state = true;
                }
                active_block = &blocks[i];
            }
            if (blocks[i].isOld()) {
                if (old_block != nullptr) {
                    bad_state = true;
                }
                old_block = &blocks[i];
            }
        }

        if (bad_state) {
            MSG_ERROR(CONF, "bad block state, erasing all blocks");
            for (auto &blck : blocks) {
                blck.erase();
            }
            active_block = &blocks[0];
            active_block->setActive();
        }
        else if (!active_block) {
            MSG_WARN(CONF, "no active block found");
            size_t active_index = 0;
            if (old_block != nullptr) {
                MSG_INFO(CONF, "have old block, index %ld", old_block->getIndex());
                old_block = &blocks[old_block->getIndex()];
                active_index = old_block->getIndex() + 1;
                if (active_index >= BLCK_COUNT) {
                    active_index = 0;
                }
            }
            MSG_INFO(CONF, "new active block index %ld", active_index);
            active_block = &blocks[active_index];
            active_block->setActive();
        }
        else if (old_block) {
            MSG_INFO(CONF, "active and old block found, "
                           "checking old block move state");
            for (size_t i = 0; i < old_block->max_rec_count; i++) {
                auto old_blck_rec = old_block->popTopRec();
                auto rec_lma = active_block->getRecLMA(old_blck_rec.virtual_addr);
                if (!rec_lma) {
                    // no recent record found - write to active page
                    assert(!active_block->isFull());
                    active_block->writeRec(old_blck_rec);
                }
                if (old_block->isEmpty()) {
                    eraseOldBlock();
                    break;
                }
                if (!rec_lma) {
                    break;
                }
            }
        }
        MSG_INFO(CONF, "--- eeprom driver ready ---");
    }

    void read(uint32_t address, uint8_t *data, size_t length) {
        MSG_DEBUG(CONF, "eeprom read <%08X>", address);

        assert(address + length <= size());

        auto active = getActiveBlock();
        assert(active);

        for (size_t i = 0; i < length;) {
            auto data_remaining = length - i;
            auto rd_addr = address + i;
            auto in_rec_offset = rd_addr % sizeof(MemRecord::data);
            auto rec_vma = rd_addr - in_rec_offset;
            auto copy_len = sizeof(MemRecord::data) - in_rec_offset;
            if (copy_len > data_remaining) copy_len = data_remaining;
            auto rec_ptr = active->getRecLMA(rec_vma);
            auto old_block = getOldBlock();
            if (!rec_ptr && old_block) {
                rec_ptr = old_block->getRecLMA(rec_vma);
            }
            if (rec_ptr) {
                memcpy(data + i,
                       rec_ptr + offsetof(MemRecord, data) + in_rec_offset,
                       copy_len);
            }
            else {
                memset(data + i, 0xFF, copy_len);
            }
            i += copy_len;
        }
        MSG_DUMP(CONF, "eeprom read data:", address);
        HEX_MSG(LEV_DUMP, CONF, data, length);
    }

    void write(uint32_t address, uint8_t *data, size_t length) {
        MSG_DEBUG(CONF, "eeprom write <%08X>", address);
        HEX_MSG(LEV_DUMP, CONF, data, length);

        assert(address + length <= size());

        MemRecord rec;

        while (length > 0) {
            auto in_rec_offset = address % sizeof(MemRecord::data);
            auto write_len = sizeof(MemRecord::data) - in_rec_offset;
            if (write_len > length) write_len = length;

            rec.virtual_addr = address - in_rec_offset;
            if (in_rec_offset > 0 || write_len < sizeof(rec.data)) {
                // read old value for partial update
                read(rec.virtual_addr, rec.data, sizeof(rec.data));
            }

            // modify record
            memcpy(rec.data + in_rec_offset, data, write_len);

            auto active = getActiveBlock();
            assert(active);
            // write modified record to active block
            active->writeRec(rec);

            auto old = getOldBlock();
            if (old) {
                for (size_t i = 0; i < oldRecsPerNew(); i++) {
                    auto old_blck_rec = old->popTopRec();
                    auto rec_lma = active->getRecLMA(old_blck_rec.virtual_addr);
                    if (!rec_lma) {
                        // no recent record found - write to active page
                        assert(!active->isFull());
                        active->writeRec(old_blck_rec);
                    }
                    if (old->isEmpty()) {
                        eraseOldBlock();
                        break;
                    }
                }
            }

            if (active->isFull()) {
                makeNewActiveBlock();
            }

            length -= write_len;
            address += write_len;
            data += write_len;
        }
    }

    void eraseAll() {
        MSG_WARN(CONF, "erasing all eeprom");
        for (size_t i = 0; i < BLCK_COUNT; i++) {
            blocks[i].erase();
        }
        MSG_INFO(CONF, "re-init");
        reInit();
    }

    constexpr size_t size() {
        return usable_mem_size;
    }
};

#define EE_EMUL_MAX_BLOCK_USAGE_PERCENT         75

extern
EepromEmulator<EE_EMUL_BLCK_SIZE,
    EE_EMUL_BLCK_COUNT,
    MCU_PAGE_SIZE,
    EE_EMUL_MAX_BLOCK_USAGE_PERCENT> eeprom_driver;

#endif

