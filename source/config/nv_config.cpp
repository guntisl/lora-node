

#include "eeprom_emulator.h"
#include "nv_config.h"
#include "debug_log.h"
#include "string.h"

#define ADD_BANK(name)          [name] = getOffset(name)

namespace nvconf {

constexpr size_t getOffset(BankID id) {
    return bankSizeSum(id) - bank_size[id];
}

constexpr uint16_t bank_offset[] = {
    ADD_BANK(NVC_BANK_MAIN),
    ADD_BANK(NVC_BANK_LORA),
    ADD_BANK(NVC_BANK_LORA_MAC),
    ADD_BANK(NVC_BANK_CCS811),
    ADD_BANK(NVC_BANK_DIGIO),
    ADD_BANK(NVC_BANK_ENV_SENS),
    ADD_BANK(NVC_BANK_APP),
    ADD_BANK(NVC_BANK_RND)
};

void configReset() {
    eeprom_driver.eraseAll();
}

void readBank(BankID bank, uint32_t address, void *buffer,
                    size_t length) {
    STRONG_ASSERT(address + length <= bank_size[bank], "need %d", length);

    eeprom_driver.read(address + bank_offset[bank], (uint8_t *) buffer, length);
}

void writeBank(BankID bank, uint32_t address, void *buffer,
                     size_t length) {
    STRONG_ASSERT(address + length <= bank_size[bank], "need %d", length);

    eeprom_driver.write(address + bank_offset[bank], (uint8_t *) buffer, length);
}

void read(uint32_t address, void *buffer, size_t length) {
    MSG_DUMP(CONF, "read config <%04X> :", address);
    // read from cache only
    STRONG_ASSERT(address + length <= eeprom_driver.size(), , "");

    eeprom_driver.read(address, (uint8_t *) buffer, length);

    HEX_MSG(LEV_DUMP, CONF, buffer, length);
}

void write(uint32_t address, void *buffer, size_t length) {
    auto ptr = (uint8_t *) buffer;

    MSG_DUMP(CONF, "write config <%04X> :", address);
    HEX_MSG(LEV_DUMP, CONF, ptr, length);
    // write in flash and update cache
    STRONG_ASSERT(address + length <= eeprom_driver.size(), "");

    eeprom_driver.write(address, (uint8_t *) buffer, length);
}

size_t getMemorySize() {
    return eeprom_driver.size();
}

}
