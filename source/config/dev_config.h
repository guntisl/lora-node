
#ifndef dev_config_h
#define dev_config_h

#include <stdint.h>
#include <sensors/REED_SWITCH.h>
#include "app.h"
#include "digital_io.h"
#include "env_sensors.h"
#include "BUTTON.h"
#include "HDC2010.h"
#include "lora.h"

#define CONF_LAYOUT_VERSION_NUM         1

struct __attribute__((aligned(4))) DeviceConfig {
    struct __attribute__((aligned(4))) Data {
        app::Config app;
        lora::LoraConf lora;
        struct __attribute__((aligned(4))) Sensors {
            sensor::ccs811::Config ccs811;
            sensor::hdc2010::Config hdc2010;
            sensor::opt3001::Config opt3001;
            sensor::env::EnvSensConfig env_sens;
            sensor::digio::DigIoConfig digital_io;
        } sensors;
        uint32_t rnd_seed;
    }data;

    uint16_t crc;
    uint8_t version;              //!< config layout version
};

extern const DeviceConfig dev_config;
extern const DeviceConfig::Data &config;

#endif
