
#ifndef nv_config_h
#define nv_config_h

#include <stdlib.h>
#include <stdint.h>
#include <crypto/misc_crypto.h>

// definitions of banks in global scope
enum BankID {
    NVC_BANK_MAIN = 0,
    NVC_BANK_LORA,
    NVC_BANK_LORA_MAC,
    NVC_BANK_CCS811,
    NVC_BANK_DIGIO,
    NVC_BANK_ENV_SENS,
    NVC_BANK_APP,
    NVC_BANK_RND
};

namespace nvconf {

constexpr uint16_t bank_size[] = {
    [NVC_BANK_MAIN] = 20,
    [NVC_BANK_LORA] = 60,
    [NVC_BANK_LORA_MAC] = 1250,
    [NVC_BANK_CCS811] = 15,
    [NVC_BANK_DIGIO] = 28,
    [NVC_BANK_ENV_SENS] = 60,
    [NVC_BANK_APP] = 16,
    [NVC_BANK_RND] = 8
};


constexpr size_t bankSizeSum(BankID id) {
    return id == 0 ? bank_size[0] : bankSizeSum((BankID) (id - 1)) +
                                    bank_size[id];
}


void configReset();
size_t getMemorySize();

void readBank(BankID bank, uint32_t address, void *buffer, size_t length);
void writeBank(BankID bank, uint32_t address, void *buffer, size_t length);

template<typename T>
void writeBank(BankID bank, uint32_t address, T &obj) {
    nvconf::writeBank(bank, address, &obj, sizeof(obj));
}

template<typename T>
T readBank(BankID bank, uint32_t address) {
    T obj;
    nvconf::readBank(bank, address, &obj, sizeof(obj));
    return obj;
}

void read(uint32_t address, void *buffer, size_t length);
void write(uint32_t address, void *buffer, size_t length);

template<typename T>
void write(uint32_t address, T &obj) {
    nvconf::write(address, &obj, sizeof(obj));
}

template<typename T>
T read(uint32_t address) {
    T obj;
    nvconf::read(address, &obj, sizeof(obj));
    return obj;
}

template<typename T>
size_t writeBankSafe(BankID bank, uint32_t address, T &obj) {
    struct WriteStruct {
        T obj;
        uint16_t crc;
    };

    WriteStruct stored_obj = {
        .obj = obj,
        .crc = crc::crc16_CCITT(&obj, sizeof(obj))
    };

    writeBank(bank, address, stored_obj);

    return sizeof(WriteStruct);
}

template<typename T>
T readBankSafe(BankID bank, uint32_t address, bool &crc_status) {
    struct WriteStruct {
        T obj;
        uint16_t crc;
    };

    T data = readBank<T>(bank, address);

    auto stored_crc = readBank<uint16_t>(bank,
            address + offsetof(WriteStruct, crc));
    auto actual_crc = crc::crc16_CCITT(&data, sizeof(data));
    crc_status = stored_crc == actual_crc;

    return data;
}

}


#endif
