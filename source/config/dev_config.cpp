
/**
 * @brief Device specific configuration (serial numbers, keys etc.)
 * @details Here is defined default testing values. For production
 * image must be modified with appropriate config.
 */

#include <crypto/misc_crypto.h>
#include "dev_config.h"
#include "LoRaMac.h"


__attribute__((used,section(".dev_config")))
const DeviceConfig dev_config = {
    .data = {
        .app = {
            .user = {
                .wait_n_reports = 1
            }
        },
        .lora = {
            .fixed = {
                .dev_eui = LORAWAN_DEVICE_EUI,
                .app_key = LORAWAN_APP_KEY,

                .FNwkSIntKey = LORAWAN_F_NWK_S_INT_KEY,
                .SNwkSIntKey = LORAWAN_S_NWK_S_INT_KEY,
                .NwkSEncKey = LORAWAN_NWK_S_ENC_KEY,
                .AppSKey = LORAWAN_APP_S_KEY,

                .tx_duty_ms = APP_TX_DUTYCYCLE,
                .tx_duty_rnd_ms = APP_TX_DUTYCYCLE_RND,
                .data_rate = LORAWAN_DATARATE,
                .max_rx_error_ms = MAX_RX_ERROR_ms,
                .restore_context = RESTORE_SAVED_CONTEXT
            },
            .user = {
                .otaa_mode = OVER_THE_AIR_ACTIVATION,
                .public_netw = LORAWAN_PUBLIC_NETWORK,
                .duty_limit_enable = LORAWAN_DUTYCYCLE_ON,
                .adr_enable = LORAWAN_ADR_ON,
                .retransmit_count = RETRANSMIT_CNT,
                .dev_class = LORAWAN_CLASS,
                .join_eui = LORAWAN_JOIN_EUI,
                .drop_server_cnt = LORA_REJOIN_LIMIT
            }
        },
        .sensors = {
            .ccs811 = {
                .use_baseline = false
            },
            .hdc2010 = {
                .temp_resol = sensor::hdc2010::Config::RES_14b,
                .humid_resol = sensor::hdc2010::Config::RES_14b
            },
            .opt3001 = {
                .high_resolution = true
            },
            .env_sens = {
                .fixed = {
                    .en_module = 1
                },
                .user = {
                    .en_triggers_bits = 0b00000000,
                    .temp_trig_high = 320,
                    .temp_trig_low = 100,
                    .humid_trig_high = 85,
                    .humid_trig_low = 200,
                    .co2_trig_high = 1000,
                    .co2_trig_low = 250,
                    .illum_trig_high = 1200,
                    .illum_trig_low = 100,
                    .sampling_inerval = 50
                }
            },
            .digital_io = {
                .fixed = {
                    .inp_count = 4,
                    .en_module = 0
                },
                .user = {
                    .en_pulse_counter = { 1, 1, 0, 0 },
                    .pulse_divider = { 1, 5, 1, 1 },
                    .trigger_pol = { 0, 0, 0, 0 },
                    .en_event_trigger = { 0, 0, 1, 1 },
                    .state_report_int = 1
                }
            }
        },
        .rnd_seed = 0x51AF5721
    },
    .crc = 0,
    .version = CONF_LAYOUT_VERSION_NUM,
};

const DeviceConfig::Data &config = dev_config.data;

class CheckDevConfig {
public:
    // validate device configuration on startup
    CheckDevConfig() {
        STRONG_ASSERT(dev_config.version == CONF_LAYOUT_VERSION_NUM,
                      "Wrong config layout version");

#ifdef CHECK_CONFIG_CRC
        auto crc = crc::crc16_CCITT(&dev_config.data,
                                    sizeof(DeviceConfig::data));

        STRONG_ASSERT(crc == dev_config.crc, "Wrong config layout CRC");
#endif
    }
};

CheckDevConfig init_check;
