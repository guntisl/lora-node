
#include "internal_flash.h"
#include "eeprom_emulator.h"

EepromEmulator<EE_EMUL_BLCK_SIZE,
    EE_EMUL_BLCK_COUNT,
    MCU_PAGE_SIZE,
    EE_EMUL_MAX_BLOCK_USAGE_PERCENT> eeprom_driver(flash::eeprom_start());
