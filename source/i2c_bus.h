
#ifndef i2c_bus_h
#define i2c_bus_h

#include "stm32wbxx.h"
#include "sys_defines.h"
#include <stdlib.h>
#include <string.h>

namespace i2c_bus {

void master_tx(uint8_t address, uint8_t* data, size_t len);
void master_rx(uint8_t address, uint8_t* data, size_t len);


template<uint8_t ADDR>
void writeReg(uint8_t addr) {
    i2c_bus::master_tx(ADDR, &addr, 1);
}

template<uint8_t ADDR, typename T>
void writeReg(uint8_t addr, const T& value) {
    uint8_t cmd_buf[sizeof(T) + 1];
    cmd_buf[0] = addr;
    memcpy(&cmd_buf[1], (uint8_t*)&value, sizeof(value));

    i2c_bus::master_tx(ADDR, cmd_buf, sizeof(cmd_buf));
}

template<typename T, size_t ADDR>
T readReg(uint8_t addr) {
    i2c_bus::master_tx(ADDR, &addr, sizeof(addr));

    T reg_data;
    i2c_bus::master_rx(ADDR, (uint8_t*)&reg_data, sizeof(reg_data));

    return reg_data;
}


template<uint8_t ADDR, void (*setupTask)(bool)>
void writeReg(uint8_t addr) {
    setupTask(true);
    writeReg<ADDR>(addr);
    setupTask(false);
}

template<uint8_t ADDR, void (*setupTask)(bool), typename T>
void writeReg(uint8_t addr, const T& value) {
    setupTask(true);
    writeReg<ADDR>(addr, value);
    setupTask(false);
}

template<typename T, size_t ADDR, void (*setupTask)(bool)>
T readReg(uint8_t addr) {
    setupTask(true);
    auto reg_data = readReg<T,ADDR>(addr);
    setupTask(false);

    return reg_data;
}



}

#endif
