#include <cstdint>
#include <cstring>
#include <stm32wbxx_ll_gpio.h>
#include <array>
#include <limits>
#include <sys_config.h>
#include <sys/syscalls.h>
#include "sensors/sensor_data.h"
#include "debug_log.h"
#include "debug_config.h"
#include "i2c_bus.h"
#include "sys_time.h"
#include "HDC2010.h"
#include "env_sensors.h"

#define HDC2010_ADDR                0x40

#define HDC2010_ID                  2000

#define T_POWER_ON_ms               1

/// start of measurement data registers
#define MEASURE_DATA_REG            0x00
#define IRQ_STATUS_REG              0x04
#define IRQ_EN_REG                  0x07
#define CONFIG_REG                  0x0E
#define MEASURE_CONF_REG            0x0F
#define DEVICE_ID_REG               0xFE

#define DRDY_EN                     (1 << 7)
#define DRDY_STATUS                 (1 << 7)
#define DRDY_PIN_EN                 (1 << 2)
#define MEAS_TRIG                   1

using i2c_bus::readReg;
using i2c_bus::writeReg;
/**
 * @brief Abstraction to use I2C register read/write templates
 *  passes device I2C bus address and function to setup other gpio and
 *  delays before starting I2C transaction
 */
#define HDC2010                     HDC2010_ADDR

using namespace sensor::env;

namespace sensor {
namespace hdc2010 {

bool data_ready;
TempHumData last_sample;
Hysteresis temp_trig_hyst;
Hysteresis humid_trig_hyst;


void init() {
    LL_EXTI_InitTypeDef exti_config;

    MSG_INFO(STH, "init");

    // wait power on delay
    auto time = sys_time::getTime();
    if(time < T_POWER_ON_ms) {
        sys_time::delay(T_POWER_ON_ms - time);
    }

    // check device ID
    auto dev_id = readReg<uint16_t,HDC2010>(DEVICE_ID_REG);
   ASSERT(dev_id == HDC2010_ID, "bad device ID %d", dev_id);

    // enable data ready IRQ
   writeReg<HDC2010>(IRQ_EN_REG, (uint8_t)DRDY_EN);

    // set basic config
    writeReg<HDC2010>(CONFIG_REG, (uint8_t)DRDY_PIN_EN);

    exti_config.Line_0_31 = HDC2010_EXTI_LINE;
    exti_config.Line_32_63 = 0;
    exti_config.LineCommand = ENABLE;
    exti_config.Mode = LL_EXTI_MODE_IT;
    exti_config.Trigger = LL_EXTI_TRIGGER_FALLING;
    LL_EXTI_Init(&exti_config);

    // clear any active int
    readReg<uint32_t,HDC2010>(MEASURE_DATA_REG);
    LL_EXTI_ClearFlag_0_31(HDC2010_EXTI_LINE);
}

void startMeasure(const Config& config) {
    STRONG_ASSERT(config.temp_resol < 3, "%d out of range", config.temp_resol);
    STRONG_ASSERT(config.humid_resol < 3, "%d out of range", config.humid_resol);

    data_ready = false;

    uint8_t config_reg = config.temp_resol << 6 | config.humid_resol << 4
                         | MEAS_TRIG;
    writeReg<HDC2010>(MEASURE_CONF_REG, config_reg);
}

bool isDataReady() {
    return data_ready;
}

void irqHandler() {
    auto measurement = readReg<uint32_t,HDC2010>(MEASURE_DATA_REG);
    auto temp_raw = (uint16_t)measurement;
    auto humidity_raw = (uint16_t)(measurement >> 16);
    auto scale = std::numeric_limits<uint16_t>::max();

    // calculate temperature
    auto temp = (uint8_t)((temp_raw * 165 - 40 * scale) / scale);
    auto temp_remainder = (temp_raw * 165 - 40 * scale) % scale;
    auto temp_fract = (uint16_t)(temp_remainder * 1000 / scale);

    // calculate humidity
    auto humid = (uint8_t)(humidity_raw * 100 / scale);
    auto hum_remainder = humidity_raw * 100 % scale;
    auto humid_fract = (uint8_t)(hum_remainder * 1000 / scale);

            MSG_DEBUG(STH, "#############humid and temp = %d, h = %d ",temp,humid);
            MSG_DEBUG(STH, "#############humid and temp");
    // copy only now to not block optimizations, because target is volatile
    last_sample.temp_whole = temp;
    last_sample.temp_mili_fraction = temp_fract;
    last_sample.humidity_whole = humid;
    last_sample.humidity_fraction = humid_fract;
    data_ready = true;

            MSG_DEBUG(STH, "#############humid and temp = %d, h = %d ",temp,humid);
    if (sensor::env::user_conf.en_triggers_bits &
        1u << sensor::env::EnvSensConfig::User::TEMP_TRIG_OBIT) {

        int16_t temp_x10 = last_sample.temp_whole * 10 +
                            last_sample.temp_mili_fraction / 100;

        // check triggers
        auto temp_trig_status = temp_trig_hyst.testValue(temp_x10,
             sensor::env::user_conf.temp_trig_high,
             sensor::env::user_conf.temp_trig_low);

        if (temp_trig_status != Hysteresis::NO_TRIG) {
            MSG_DEBUG(STH, "-----------------================temp trig=================-------------");
            setEnvTrigger(StateReport::EventTrigBitNum::TEMP);
        }
    }

    if (sensor::env::user_conf.en_triggers_bits &
        1u << sensor::env::EnvSensConfig::User::HUMID_TRIG_OBIT) {

        auto humid_trig_status = humid_trig_hyst.testValue(humid,
            sensor::env::user_conf.humid_trig_high,
            sensor::env::user_conf.humid_trig_low);

        if(humid_trig_status != Hysteresis::NO_TRIG) {
            MSG_DEBUG(STH, "humid trig");
            setEnvTrigger(StateReport::EventTrigBitNum::HUMID);
        }
    }


}

TempHumData getLastSample() {
    MaskIrqForScope irq_guard(IRQ_PRI__SENSORS);
    return last_sample;
}


}
}
