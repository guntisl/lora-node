
#ifndef pulse_counter_h
#define pulse_counter_h

#include <cstdlib>
#include "sys/semaphore.h"

namespace sensor {
namespace digio {

struct __attribute__((aligned(4))) DigIoConfig {
    struct Fixed {
        uint8_t inp_count;
        uint8_t en_module;
    };
    struct User {
        uint8_t en_pulse_counter[4];
        uint16_t pulse_divider[4];
        uint8_t trigger_pol[4];
        uint8_t en_event_trigger[4];
        uint16_t state_report_int;
    };

    Fixed fixed;
    User user;
};

struct StateReport {
    enum EventTrigBitNum {
        PINA = 0,
        PINB,
        PINC,
        PIND,
        TIME
    };

    sys_time::SysTime timestamp;
    uint32_t counter[4];
    uint8_t inp_state;
    uint8_t event_trig;
};

extern DigIoConfig::User user_conf;

void init();
StateReport getStateReport();
void setOutput(bool state, size_t output);

}
}

#endif

