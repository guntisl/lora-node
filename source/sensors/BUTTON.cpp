
#include <stdint.h>
#include "debug_log.h"
#include "sys_time.h"
#include "config/dev_config.h"
#include "sys_config.h"
#include "config/nv_config.h"
#include "crypto/misc_crypto.h"
#include "string.h"
#include "i2c_bus.h"
#include "BUTTON.h"
#include "stm32wbxx_ll_gpio.h"
#include "sys_defines.h"
#include "syscalls.h"
#include "sys/misc/fifo_buffer.h"
#include "env_sensors.h"
#include "sensor_data.h"

#define CCS811_ADDR                 0x5A
#define CCS811_HW_ID                0x81

#define T_POWER_ON_ms               80
#define T_RESET_ms                  5
#define T_APP_START_ms              5
#define T_NWAKE_TO_I2C_us           60
#define T_MIN_NWAKE_HIGH_us         25
#define T_ENV_PRELOAD_ms            50
#define T_TEMP_HUM_MEAS_ms          20

#define T_MODE_DOWN_SWITCH_min      10
#define T_WARMUP_TIME_min           20
#define T_STABIL_TIME_min           45
#define T_BASE_SAVE_INT_min         720

#define STATUS_REG                  0x00
#define MEAS_MODE_REG               0x01
#define ALG_RESULT_DATA             0x02
#define ENV_DATA_REG                0x05
#define BASELINE_REG                0x11
#define HW_ID_REG                   0x20
#define HW_VERSION_REG              0x21
#define APP_START                   0xF4
#define SW_RESET_REG                0xFF

#define APP_VALID                   (1 << 4)
#define FW_MODE                     (1 << 7)
#define INTERRUPT                   (1 << 3)

#define TRIG_HYST                   10

#if T_ENV_PRELOAD_ms > 999
#error "Warning minimal period for co2 data can be 1s"
#endif
#if T_TEMP_HUM_MEAS_ms >= T_ENV_PRELOAD_ms
#error "Environment data will be at same time as gas IRQ - too late"
#endif
void interupt_flag_handler(void);
#define ROTATION_NONE  0;
#define ROTATION_LEFT  1;
#define ROTATION_RIGHT 2;
uint8_t current_dt_status_pina = 0;
uint8_t current_dt_status_pinb = 0;
uint8_t flag_change = 0;
uint8_t last_dt_status = 0;
uint8_t i,j,k,r,p;
volatile int enc_cnt;
extern uint8_t presses;
extern uint8_t device_type;
uint8_t bee_power = 0;
uint8_t food_level = 0;
uint8_t led3_state = 0;
signed int encoder_phase = 0;
int counter = 0;
long unsigned aState;
long unsigned aLastState =  (GPIOA->IDR & ( 1 << 1 ));

uint8_t shift_reg_high[9][8]  = {{1,1,1,1,1,1,1,1},{1,0,0,0,0,0,0,0},{1,0,0,0,1,0,0,0},{1,1,0,0,1,0,0,0},{1,1,0,1,1,0,0,0},{1,1,0,1,1,1,0,0},{1,1,0,1,1,1,1,0},{1,1,1,1,1,1,1,0},{1,1,1,1,1,1,1,1}};
uint8_t shift_reg_low[8][8]  = {{0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0},{1,1,0,0,0,0,0,0},{1,1,0,0,0,0,0,0},{1,1,0,0,0,0,0,0},{1,1,0,0,0,0,0,0},{1,1,0,0,0,0,0,0},{1,1,0,0,0,0,0,0}};
using i2c_bus::readReg;
using i2c_bus::writeReg;

using namespace sensor::env;

namespace sensor {
namespace ccs811 {

struct Event {
    enum EventId : uint32_t {
        NO_TASK = 0,
        SET_MODE,
        MEASURE_ENV_DATA,
        UPDATE_ENV_DATA
    };

    sys_time::TimerElement timer;
    Mode new_mode;
    EventId type;
};

struct AlgResultData {
    uint8_t eCO2_high;
    uint8_t eCO2_low;
    uint8_t eTVOC_high;
    uint8_t eTVOC_low;
    uint8_t status;
};

struct BaselineData {
    uint16_t baseline_mode1;    // bit1
    uint16_t baseline_mode2;    // bit2
    uint16_t baseline_mode3;    // bit3
    // bitfield indicating which mode has stored baseline
    // bit cleared is baseline stored
    uint8_t stored_baselines;
};

class BaselineControl {
    BaselineData& bline;
public:
    explicit BaselineControl(BaselineData& baselines) : bline(baselines) {}

    void readBaselineData();
    void storeBaselineData();
    void deleteBaseline();
    bool haveBaseline(Mode mode);
    void setBaseline(Mode mode);
    void checkBaseline();
};

Event event;
bool sample_ready;
Mode current_mode;
sys_time::SysTime mode_switch_time;
GasData last_sample;
bool data_valid;
bool baseline_set;
BaselineData baselines;
sys_time::SysTime baseline_save_time;
BaselineControl bline_cntrl(baselines);

Hysteresis gas_trig_hyst;

/**
 * @brief Convert register values from big endian to little
 */
void convertMeasureData(const AlgResultData& reg_data);
static void setupCom(bool entry);
static void setWAKE(bool state);
static void ccs811Event(void *param);
static void setWorkMode(Mode new_mode);
static void startEventTimer(Event::EventId type, Event& event, uint32_t timeout);
uint32_t convEnvDataToRegVal(const sensor::hdc2010::TempHumData& env_data);

/**
 * @brief Abstraction to use I2C register read/write templates
 *  passes device I2C bus address and function to setup other gpio and
 *  delays before starting I2C transaction
 */
#define CCS811                      CCS811_ADDR,setupCom


void init(Mode init_mode) {
    LL_EXTI_InitTypeDef exti_config;

    // configure IRQ to service sensor
    MSG_INFO(SCO2, "SCO --------###############init############################");

    // read baseline data from config
    bline_cntrl.readBaselineData();

    // set configured work mode
    current_mode = init_mode;
    uint8_t mode_reg = current_mode << 4 | INTERRUPT;
    MSG_INFO(SCO2, "config mode %d reg %02X", current_mode, mode_reg);
    mode_switch_time = sys_time::getTime();

    // configure BUTTON nINT pin exti IRQ
    LL_GPIO_InitTypeDef pin_config;
    LL_GPIO_StructInit(&pin_config);
    pin_config.Pin = BUTTON_PIN;
    pin_config.Mode = LL_GPIO_MODE_INPUT;
    pin_config.Pull = LL_GPIO_PULL_UP;
    LL_GPIO_Init(BUTTON_PORT, &pin_config);
    
    
    exti_config.Line_0_31 = BUTTON_EXTI_LINE;
    exti_config.Line_32_63 = 0;
    exti_config.LineCommand = ENABLE;
    exti_config.Mode = LL_EXTI_MODE_IT;
    exti_config.Trigger = LL_EXTI_TRIGGER_FALLING;
    LL_EXTI_Init(&exti_config);

    LL_EXTI_ClearFlag_0_31(BUTTON_EXTI_LINE);

    MSG_INFO(SCO2, "####SCO ------init done ---####");
    // ready to accep measurements --
}

void switchMode(Mode new_mode, bool ensure_idle_time) {
    if (current_mode != Mode::OFF
        && new_mode > current_mode && ensure_idle_time) {

        // need sensor idle time
        setWorkMode(Mode::OFF);
        MSG_WARN(SCO2, "waiting 10 min before new mode is enabled");

        event.new_mode = new_mode;
        startEventTimer(Event::EventId::SET_MODE, event,
                        MIN_TO_ms(T_MODE_DOWN_SWITCH_min));
    }
    else {
        // if disabled just enable new mode
        setWorkMode(new_mode);
    }
}

Mode getCurMode() {
    return current_mode;
}

static void setWorkMode(Mode new_mode) {
    MSG_DEBUG(SCO2, "set mode %d", new_mode);
    current_mode = new_mode;
    uint8_t mode_reg = current_mode << 4 | INTERRUPT;
    MSG_DUMP(SCO2, "config mode %d reg %02X", current_mode, mode_reg);
   // writeReg<CCS811>(MEAS_MODE_REG, mode_reg);
    mode_switch_time = sys_time::getTime();
    baseline_save_time = mode_switch_time;
    data_valid = false;
    baseline_set = false;
}

uint32_t convEnvDataToRegVal(const sensor::hdc2010::TempHumData& env_data) {
    // convert from decimal fraction to binary 1/512
    uint32_t hum_fract = ((env_data.humidity_fraction * 512U + 500u) / 1000u);
    uint32_t temp_fract = ((env_data.temp_mili_fraction * 512U + 500u) / 1000u);

    uint32_t env_data_reg = (env_data.humidity_whole << 1) & 0x000000FFu;
    env_data_reg |= (hum_fract >> 8) & 0x00000001u;
    env_data_reg |= (hum_fract << 8) & 0x0000FF00u;

    env_data_reg |= (env_data.temp_whole << 17) & 0x00FF0000u;
    env_data_reg |= (temp_fract << 8) & 0x00010000u;
    env_data_reg |= (temp_fract << 24) & 0xFF000000u;

    return env_data_reg;
}

void writeEnvData(const sensor::hdc2010::TempHumData& env_data) {
    MSG_DUMP(SCO2, "update temp humidity");

//    uint32_t env_data_reg = convEnvDataToRegVal(env_data);
   // writeReg<CCS811>(ENV_DATA_REG, env_data_reg);
}

bool isDataReady() {
    auto status = sample_ready;
    sample_ready = false;
    return status;
}

GasData getLastSample() {
    MaskIrqForScope irq_guard(IRQ_PRI__SENSORS);
    return last_sample;
}

static void ccs811Event(void *param) {
    MSG_DUMP(SCO2, "BUTTON event");

    auto event = (Event *) param;
    switch (event->type) {
    case Event::EventId::SET_MODE: {
        MaskIrqForScope irq_off(IRQ_PRI__SENSORS);
        // cool down expired - can switch to new work mode
        setWorkMode(event->new_mode);
        break;
    }
    case Event::EventId::MEASURE_ENV_DATA: {
        MSG_DUMP(SCO2, "start temp/humid measure");
        MaskIrqForScope irq_off(IRQ_PRI__SENSORS);
        hdc2010::startMeasure(config.sensors.hdc2010);
        startEventTimer(Event::EventId::UPDATE_ENV_DATA, *event,
                        T_TEMP_HUM_MEAS_ms);
        break;
    }
    case Event::EventId::UPDATE_ENV_DATA: {
        ASSERT(hdc2010::isDataReady(), "t/h measure failed");
        {
            MaskIrqForScope irq_off(IRQ_PRI__SENSORS);
            last_sample.temp_hum = hdc2010::getLastSample();
            writeEnvData(last_sample.temp_hum);
        }
        MSG_DEBUG(SCO2, "t=%d.%03u h=%d.%03u", last_sample.temp_hum.temp_whole,
                  last_sample.temp_hum.temp_mili_fraction,
                  last_sample.temp_hum.humidity_whole,
                  last_sample.temp_hum.humidity_fraction);
        break;
    }
    default:
        ASSERT(0, "bad BUTTON event id %d", event->type);
        break;
    }
}
/**
 * @brief CCS811 gas sensor event
 */



void rotary_read(void) {



     switch (enc_cnt)
     {
         case 1: //MSG_WARN(ENV, "-========= LED 1 ON  ========");
                         set_led(1,HIGH);
                         set_level(presses,1);
                         MSG_WARN(ENV, "--= %d, %d =--",bee_power,food_level);
                         break;
         case 2:  // MSG_WARN(ENV, "-========= LED 2 ON ========");
                         set_led(2,HIGH);
                         set_level(presses,2);
                         MSG_WARN(ENV, "--= %d, %d =--",bee_power,food_level);
                         break;
         case 3: // MSG_WARN(ENV, "-========= LED 3 ON ========");
                         set_led(3,HIGH);
                         set_level(presses,3);
                         MSG_WARN(ENV, "--= %d, %d =--",bee_power,food_level);
                         break;
         case 4: //MSG_WARN(ENV, "-========= LED 4 ON ========");
                         set_led(4,HIGH);
                         set_level(presses,4);
                         MSG_WARN(ENV, "--= %d, %d =--",bee_power,food_level);
                         break;
         case 5: //MSG_WARN(ENV, "-========= LED 5 ON ========");
                         set_led(5,HIGH);
                         set_level(presses,5);
                         MSG_WARN(ENV, "--= %d, %d =--",bee_power,food_level);
                         break;
         case 6: //MSG_WARN(ENV, "-======== LED 6 ON ========");
                         set_led(6,HIGH);
                         set_level(presses,6);
                         MSG_WARN(ENV, "--= %d, %d =--",bee_power,food_level);
                         break;
         case 7: //MSG_WARN(ENV, "-======== LED 7 ON ========");
                         set_led(7,HIGH);
                         set_level(presses,7);
                         MSG_WARN(ENV, "--= %d, %d =--",bee_power,food_level);
                         break;
         case 8: //MSG_WARN(ENV, "-======== LED 8 ON ========");
                         set_led(8,HIGH);
                         set_level(presses,8);
                         MSG_WARN(ENV, "--= %d, %d =--",bee_power,food_level);
                         break;
         default: //MSG_WARN(ENV, "-========= LEDs Off ========");
                         set_led(0,HIGH);
                         set_level(presses,0);
                         MSG_WARN(ENV, "--= %d, %d =--",bee_power,food_level);
                         break;
         }

    sys_time::delay(2000);
    set_led(0,LOW);

}



void rotary_irqHandler() {





last_dt_status = (GPIOA->IDR & ( 1 << 6 )); 


    while((GPIOA->IDR & ( 1 << 6 )) == 0) {
        current_dt_status_pina = (GPIOA->IDR & ( 1 << 1));
        flag_change = 1;

    }
    if (flag_change == 1) {
        if (last_dt_status == 0 && current_dt_status_pina == 2) {
            //enc_cnt = enc_cnt + 3;
            enc_cnt++;
//            MSG_WARN(ENV, "--=turn right %d, %d, %d =--",last_dt_status,current_dt_status_pina, enc_cnt);
            rotary_read();
        }
        if (last_dt_status == 0 && current_dt_status_pina == 0) {
            enc_cnt--;
           // enc_cnt = enc_cnt - 3;
  //          MSG_WARN(ENV, "--=turn left %d, %d, %d =--",last_dt_status,current_dt_status_pina, enc_cnt);
            rotary_read();
        }

    }


        }

void irqHandler() {
    // read sensor data
    // set next environment update time

        sys_time::delay(1000);
        if ((GPIOA->IDR & ( 1 << 0 )) == 1)
        {


            MSG_WARN(ENV, "-= short press =-");

            if (device_type == 3){
               // enc_cnt = enc_cnt + 3;
                enc_cnt++;
                if (enc_cnt > 8){
                    enc_cnt = 0;

                }
                rotary_read();
                sys_time::delay(2000);
                set_led(0,LOW);


            } else {

                presses ^= 1;
                switch (presses)
                {
                    case 0: MSG_WARN(ENV, "-= presses = 0 =-");
                            LL_GPIO_TogglePin(LED2_PORT, LED2_PIN);
                            break;
                    case 1: MSG_WARN(ENV, "-= presses = 1 =-");
                            LL_GPIO_TogglePin(LED2_PORT, LED2_PIN);

                            break;
                    default:MSG_WARN(ENV, "-= presses =  0 =-");
                            presses = 0;
                            LL_GPIO_TogglePin(LED2_PORT, LED2_PIN);
                            break;
                }

            }

        }
        else {
            MSG_WARN(ENV, "-= Long press =-");
        //    GPIOA->ODR ^= LED3_PIN;
            led3_state ^= 1;
            LL_GPIO_TogglePin(LED3_PORT, LED3_PIN);
        }

}

void BaselineControl::checkBaseline() {
    // only if using baseline
    if(config.sensors.ccs811.use_baseline) {
        //check if need to set stored baseline
        if (!baseline_set && haveBaseline(current_mode)) {
            if (sys_time::getElapsed(mode_switch_time) >
                MIN_TO_ms(T_STABIL_TIME_min)) {
                // restore baseline value
                setBaseline(current_mode);
                baseline_set = true;
            }
        }

        // check if need to store current baselin in NV memory
        if (sys_time::getElapsed(baseline_save_time) >
            MIN_TO_ms(T_BASE_SAVE_INT_min)) {
            // save baseline in NV config
            auto cur_baseline = readReg<uint16_t,CCS811>(BASELINE_REG);

            switch (current_mode) {
            case Mode::ON_1s:
                baselines.baseline_mode1 = cur_baseline;
                break;
            case Mode::PULSE_10s:
                baselines.baseline_mode2 = cur_baseline;
                break;
            case Mode::PULSE_60s:
                baselines.baseline_mode3 = cur_baseline;
                break;
            default:
                FATAL_ERROR("No baseline supported for this mode");
            }

            baselines.stored_baselines &= ~(1 << current_mode);
            storeBaselineData();
            MSG_INFO(SCO2, "baseline saved");

            baseline_save_time = sys_time::getTime();
        }
    }
}

bool BaselineControl::haveBaseline(Mode mode) {
    return baselines.stored_baselines & mode;
}

void BaselineControl::setBaseline(Mode mode) {
    uint16_t baseline;

    MSG_INFO(SCO2, "setting baseline");

    switch (~baselines.stored_baselines & (1 << mode)) {
    case 1 << Mode::ON_1s:
        baseline = baselines.baseline_mode1;
        break;
    case 1 << Mode::PULSE_10s:
        baseline = baselines.baseline_mode2;
        break;
    case 1 << Mode::PULSE_60s:
        baseline = baselines.baseline_mode3;
        break;
    default:
        MSG_WARN(SCO2, "no saved baseline found, mode %d", mode);
        return;
    }

    //write baseline to device
    writeReg<CCS811>(BASELINE_REG, baseline);

    MSG_INFO(SCO2, "set baseline mode %d", mode);
}

void BaselineControl::readBaselineData() {
    bool valid;
    bline = nvconf::readBankSafe<BaselineData>(NVC_BANK_CCS811, 0, valid);

    if(!valid) {
        // discard all baselines
        bline.stored_baselines = 0xFF;
    }
}

void BaselineControl::storeBaselineData() {
    nvconf::writeBankSafe(NVC_BANK_CCS811, 0, baselines);
}

void BaselineControl::deleteBaseline() {
    bline.stored_baselines = 0xFF;
    nvconf::writeBankSafe(NVC_BANK_CCS811, 0, baselines);
}

void discardBaseline() {
    // discard saved baseline value
    bline_cntrl.deleteBaseline();
}

void convertMeasureData(const AlgResultData& reg_data) {
    last_sample.eCO2_ppm = reg_data.eCO2_low | reg_data.eCO2_high << 8;
    last_sample.eTVOC_ppb = reg_data.eTVOC_low | reg_data.eTVOC_high << 8;
    last_sample.is_valid = data_valid;
}

static void startEventTimer(Event::EventId type, Event& event, uint32_t timeout) {
    event.type = type;
    if(sys_time::isRunning(&event.timer)) {
        // disable if running
        sys_time::stopTimer(&event.timer);
    }
    sys_time::startTimer(&event.timer, timeout,
                         (sys_time::TimerCallback) ccs811Event,
                         (size_t) &event);
}

static void setupCom(bool entry) {
    if(entry) {
        sys_time::delay_us(T_MIN_NWAKE_HIGH_us);
        setWAKE(false);
        sys_time::delay_us(T_NWAKE_TO_I2C_us);
    }
    else {
        setWAKE(true);
    }
}

static void setWAKE(bool state) {
}

} // namespace ccs811
} // namspace sensor







void toogle_reg_clk(void) {

    LL_GPIO_SetOutputPin(REG_CLK_PORT, REG_CLK_PIN);
   // sys_time::delay(10);
    LL_GPIO_ResetOutputPin(REG_CLK_PORT, REG_CLK_PIN);
   // sys_time::delay(10);
}

void reg_latch(void){

    LL_GPIO_SetOutputPin(REG_LATCH_PORT, REG_LATCH_PIN);       //set high STCP
   // sys_time::delay(10);
    LL_GPIO_ResetOutputPin(REG_LATCH_PORT, REG_LATCH_PIN);       //set high STCP
   // sys_time::delay(10);
}


void set_led(uint8_t led, uint8_t val){
    for (uint8_t i = 0; i < 8; i++){
       if (val == 1) {
           reg_data_in(shift_reg_high[led][i]);
         //  sys_time::delay(10);

       } else if (val == 0){
           reg_data_in(shift_reg_low[led][i]);
       //    sys_time::delay(10);
       }
       toogle_reg_clk(); 
     //  sys_time::delay(10);

    }
    reg_latch();
    sys_time::delay(10);

}

void reg_data_in(uint8_t val){
    if (val == 1){
        LL_GPIO_SetOutputPin(REG_DATA_IN_PORT, REG_DATA_IN_PIN);     //clear STCP

    } else if (val == 0) {

        LL_GPIO_ResetOutputPin(REG_DATA_IN_PORT, REG_DATA_IN_PIN);     //clear STCP
    }
}

void clear_shift_reg(uint8_t val) {

    reg_data_in(LOW);
    for(k=val; k > 0; k--){
        toogle_reg_clk();
    }
    reg_latch();
}
void set_level(uint8_t val, uint8_t level){

    if (val == 0) {
        bee_power = level;
    } else if (val == 1){
        food_level = level;
    } 
}
