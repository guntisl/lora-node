
#ifndef HDC2010_h
#define HDC2010_h

namespace sensor {
namespace hdc2010 {

struct  __attribute__((aligned(4))) Config {
    enum Resolution : uint8_t {
        RES_14b = 0,
        RES_11b,
        RES_8b
    };

    Resolution temp_resol;
    Resolution humid_resol;
};

struct TempHumData {
    uint8_t temp_whole;
    uint16_t temp_mili_fraction;
    uint8_t humidity_whole;
    uint16_t humidity_fraction;
};

void init();
void startMeasure(const Config& config);
bool isDataReady();
TempHumData getLastSample();

void irqHandler();

}
}

#endif
