
#ifndef env_sensors_h
#define env_sensors_h

#include "sys/misc/hysteresis.h"
#include "sensors/REED_SWITCH.h"
#include "sensors/BUTTON.h"

#include "stm32wbxx_ll_adc.h"
#include "stm32wbxx_ll_crs.h"
#include "stm32wbxx_ll_rcc.h"
#include "stm32wbxx_ll_bus.h"
#include "stm32wbxx_ll_system.h"
#include "stm32wbxx_ll_exti.h"
#include "stm32wbxx_ll_cortex.h"
#include "stm32wbxx_ll_utils.h"
#include "stm32wbxx_ll_pwr.h"
#include "stm32wbxx_ll_dma.h"
#include "stm32wbxx_ll_gpio.h"



#define USE_TIMEOUT       0


#define HIGH 1
#define LOW  0
void Error_Handler(void);

/* USER CODE BEGIN EFP */
/* IRQ Handler treatment */
void AdcGrpRegularOverrunError_Callback(void);





void rotary_encoder_update(void);
void write_reg(uint8_t leds);
void set_level(uint8_t val, uint8_t level);
void toogle_reg_clk(void);
void reg_latch(void);
void set_led(uint8_t led, uint8_t val);
void reg_data_in(uint8_t val);
void clear_shift_reg(uint8_t val);





namespace sensor {
namespace env {

struct __attribute__((aligned(4))) EnvSensConfig {
    struct Fixed {
        uint8_t en_module;
    };
    struct User {
        enum EnTriggerBitNum {
            TEMP_TRIG_OBIT = 0,
            HUMID_TRIG_OBIT,
            ILLUM_TRIG_OBIT,
            CO2_TRIG_OBIT
        };

        uint8_t en_triggers_bits;
        int16_t temp_trig_high;        // 0.1 deg
        int16_t temp_trig_low;
        uint8_t humid_trig_high;        // 1 %
        uint8_t humid_trig_low;
        uint16_t co2_trig_high;         // 1ppm
        uint16_t co2_trig_low;
        uint32_t illum_trig_high;       // 1lux
        uint32_t illum_trig_low;
        uint16_t sampling_inerval;
    };

    Fixed fixed;
    User user;
};

struct StateReport {
    enum EventTrigBitNum {
        CO2 = 0,
        TEMP,
        HUMID,
        ILLUM,
        TIME
    };

    sys_time::SysTime timestamp;
    sensor::ccs811::GasData gas;
    sensor::opt3001::OpticalData illum;
    uint8_t event_trig;
};

extern EnvSensConfig::User user_conf;
//extern uint8_t presses=3;
void init();
StateReport getStateReport();
void setEnvTrigger(StateReport::EventTrigBitNum trigger);


}
}

#endif
