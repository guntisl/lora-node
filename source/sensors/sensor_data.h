
#ifndef sensor_data_h
#define sensor_data_h

#include "BUTTON.h"
#include "HDC2010.h"
#include "REED_SWITCH.h"
//#include "digital_io.h"
#include "env_sensors.h"
#include "sys/misc/flash_fifo.h"
#include "sys/semaphore.h"

namespace sensor {
namespace data {



typedef struct _hx711
{
	GPIO_TypeDef* gpioSck;
	GPIO_TypeDef* gpioData;
	uint16_t pinSck;
	uint16_t pinData;
	int offset;
	int gain; 
	// 1: channel A, gain factor 128
	// 2: channel B, gain factor 32
    // 3: channel A, gain factor 64
} HX711;


void HX711_Init(HX711 data);

HX711 HX711_Tare(HX711 data, uint8_t times);
int HX711_Value(HX711 data);
int HX711_Average_Value(HX711 data, uint8_t times);



struct TempHumData2 {
    hdc2010::TempHumData temp_hum;
};
enum SensorTrigger : uint8_t {
    DIO_TRIG,
    ENV_TRIG,
    FB_TRIG
};

struct SensorStateReport {
    union TrigData {
       // sensor::digio::StateReport dig_ios;
        sensor::env::StateReport env_data;
    }trig_data;
    SensorTrigger trigger;
    hdc2010::TempHumData temp_hum2;
};

extern FlashFifo<SensorStateReport, SENSOR_LOG_SIZE, false> report_log;
//extern uint8_t presses;
void startSensorMonitoring();
void trigSensorEvent(SensorTrigger trigger);

void ADS1231_init(void);
int32_t ADS1231_getData(void);  // value in range -8388608 <= x <= 8388607
void ADS1231_initBus(void);
void ADS1231_enterStandByMode(void);
void ADS1231_leaveStandByMode(void);
uint32_t ADS1231_getBits(void);
int32_t convertTwosToOnesComplement(uint32_t data);
void ADS1231_delay_ms(uint32_t ms);
//void ADS1231_init(void);
//int32_t ADS1231_getData(void);
/**
 * @return number of samples in buffer
 */
size_t waitNewReport(uint32_t timeout);


}
}

#endif
