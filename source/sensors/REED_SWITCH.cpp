
#include <debug_config.h>
#include <sys/debug_log.h>
#include <sys/sys_time.h>
#include <stm32wbxx_ll_gpio.h>
#include <config/dev_config.h>
#include <sys_config.h>
#include <sys/syscalls.h>
#include "i2c_bus.h"
#include "REED_SWITCH.h"
#include "sensor_data.h"
#include "app.h"

// ATTENTION!
// OPT3001 uses MSB first byte order, so order is reversed in I2C transfers

#define SAMPLE_INT              10000u
#define TRIG_HYST               10

#define OPT3001_ADDR            0x45
#define DEV_ID                  0x0130

#define RESULT_REG              0x00
#define CONFIG_REG              0x01
#define LOW_LIMIT_REG           0x02
#define HIGH_LIMIT_REG          0x03
#define DEV_ID_REG              0x7F

#define CONFIG                  (0xC000 | 1 << 4)
#define START                   (1 << 9)


extern uint8_t reed_switch;
extern unsigned button_flag;
extern unsigned reed_switch_flag;
extern unsigned pina_flag;
extern unsigned pinb_flag;
uint8_t reed_switch = 0;
uint8_t led_map[8] = {1,2,3,4,5,6,7,8};
extern uint8_t alarm_state;
extern uint8_t alarm;
extern uint8_t led3_state;

extern uint8_t bee_power;
#define OPT3001                 OPT3001_ADDR

using namespace sensor::env;

namespace sensor {
namespace opt3001 {

OpticalData last_smple;
bool data_ready;
uint16_t dev_config_reg;
sys_time::TimerElement sample_timer;

void sampleEvent(size_t param);
void startMeasure();
void checkTriggers();
uint16_t revBytes(uint16_t value);
uint16_t luxToRegValue(uint32_t lux);
uint32_t regToLux(uint16_t reg_value);

Hysteresis opt_trig_hyst(TRIG_HYST);

void init() {
    if (!(user_conf.en_triggers_bits &
          1u << EnvSensConfig::User::ILLUM_TRIG_OBIT)) {
        MSG_INFO(OPT, "OPT3001 disabled");
    }

//    auto dev_id = i2c_bus::readReg<uint16_t,OPT3001>(DEV_ID_REG);
  //  ASSERT(dev_id == DEV_ID, "wrong OPT3001 id %d", dev_id);

    // set trig limits
   // i2c_bus::writeReg<OPT3001>(LOW_LIMIT_REG, revBytes(0xC000));

    LL_GPIO_InitTypeDef pin_config;
    LL_GPIO_StructInit(&pin_config);
    pin_config.Pin = REED_SWITCH_IRQ_PIN;
    pin_config.Mode = LL_GPIO_MODE_INPUT;
    pin_config.Pull = LL_GPIO_PULL_UP;
    LL_GPIO_Init(REED_SWITCH_IRQ_PORT, &pin_config);

    LL_EXTI_InitTypeDef exti_config;
    exti_config.Line_0_31 = REED_SWITCH_EXTI_LINE;
    exti_config.Line_32_63 = 0;
    exti_config.LineCommand = ENABLE;
    exti_config.Mode = LL_EXTI_MODE_IT;
    exti_config.Trigger = LL_EXTI_TRIGGER_RISING_FALLING;
    LL_EXTI_Init(&exti_config);
    LL_EXTI_ClearFlag_0_31(REED_SWITCH_EXTI_LINE);

    // set configuration
    dev_config_reg = CONFIG;
    if(config.sensors.opt3001.high_resolution) {
        dev_config_reg |= 1 << 11;
    }
   // i2c_bus::writeReg<OPT3001>(CONFIG_REG, revBytes(dev_config_reg));

    // clear IRQ
   // i2c_bus::readReg<uint16_t,OPT3001>(CONFIG_REG);

    // start timer
    sys_time::startTimer(&sample_timer, SAMPLE_INT, sampleEvent, 0);

    MSG_INFO(OPT, "------------================================OPT init done=========-----------------------");
}

void startMeasure() {
    // start single shot conversion
    data_ready = false;
    // clear conf
//    i2c_bus::readReg<uint16_t,OPT3001>(CONFIG_REG);
  //  i2c_bus::writeReg<OPT3001>(CONFIG_REG, revBytes(dev_config_reg | START));
}

OpticalData getLastSample() {
    MaskIrqForScope irq_guard(IRQ_PRI__SENSORS);
    return last_smple;
}

void sampleEvent(size_t param) {
    startMeasure();
    sys_time::startTimer(&sample_timer, SAMPLE_INT, sampleEvent, 0);
}
/**
 * @brief OPT3001 IRQ pin active - data ready
 */
void irqHandler() {
    MSG_WARN(ENV, "--==REED_SWITCH interupt==-");
    reed_switch ^= 1;
    if (reed_switch == 0) {
      if (alarm_state) {
          MSG_DEBUG(SENS, "Alarm == 1");
          alarm = 1;
      }
      else {
        MSG_DEBUG(SENS, "Alarm == 0");
        alarm = 0;

      }

	    if (led3_state) {
		    LL_GPIO_SetOutputPin(LED3_PORT, LED3_PIN);

	    } else {
	            LL_GPIO_ResetOutputPin(LED3_PORT, LED3_PIN);
	    }
        switch (bee_power)
             {
            case 1: //MSG_WARN(ENV, "-========= sensors interupt 1 ========");
                    set_led(1,HIGH);
                    break;
            case 2: //MSG_WARN(ENV, "-========= sensors interupt 2 ========");
                    set_led(2,HIGH);
                    break;
            case 3: //MSG_WARN(ENV, "-========= sensors interupt 3 ========");
                    set_led(3,HIGH);
                    break;
            case 4: //MSG_WARN(ENV, "-========= sensors interupt 4 ========");
                    set_led(4,HIGH);
                    break;
            case 5: //MSG_WARN(ENV, "-========= sensors interupt 5 ========");
                    set_led(5,HIGH);
                    break;
            case 6: //MSG_WARN(ENV, "-========= sensors interupt 6 ========");
                    set_led(6,HIGH);
                    break;
            case 7: //MSG_WARN(ENV, "-========= sensors interupt 7 ========");
                    set_led(7,HIGH);
                    break;
            case 8: //MSG_WARN(ENV, "-========= sensors interupt 7 ========");
                    set_led(8,HIGH);
                    break;
            default: //MSG_WARN(ENV, "-========= sensors interupt 0 ========");
                    set_led(1,LOW);
                    bee_power = 0;
                    break;
             }
        reed_switch_flag = 0;


    } else if (reed_switch == 1) {
        set_led(1, LOW);
	LL_GPIO_ResetOutputPin(LED3_PORT, LED3_PIN);
    }
    reed_switch_flag = 0;
    alarm = 0;
//    app::sendReports();
}

void checkTriggers() {

    auto status = opt_trig_hyst.testValue(last_smple.illum_lux,
        user_conf.illum_trig_high, user_conf.illum_trig_low);

    if(status != Hysteresis::NO_TRIG) {
	    MSG_DEBUG(OPT, "illum trig=====#################################--------------------------------");
        setEnvTrigger(StateReport::EventTrigBitNum::ILLUM);
    }
}

uint16_t revBytes(uint16_t value) {
    return value >> 8 | value << 8;
}

uint16_t luxToRegValue(uint32_t lux) {
    lux *= 100;
    uint16_t exp = 0;
    while (lux & ~0xFFF) {
        lux >>= 1;
        exp++;
    }
    return revBytes(exp << 12 | (uint16_t)lux);
}

uint32_t regToLux(uint16_t reg_value) {
    reg_value = revBytes(reg_value);
    auto exp = reg_value >> 12;
    uint32_t lux = reg_value & 0xFFFu;
    lux <<= exp;
    lux /= 100;
    return lux;
}


}
}
