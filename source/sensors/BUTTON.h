
#ifndef BUTTON_h
#define BUTTON_h

#include "sys_time.h"
#include "HDC2010.h"

namespace sensor {
namespace ccs811 {


enum Mode : uint8_t {
    OFF = 0,        // idle - no measurements
    ON_1s,          // on, Tmeas = 1 s
    PULSE_10s,      // pulse, Tmeas = 10 s
    PULSE_60s       // low power pulse, Tmeas = 60 s
};

struct __attribute__((aligned(4))) Config {
    bool use_baseline;
};

struct GasData {
    uint16_t eCO2_ppm;
    uint16_t eTVOC_ppb;
    uint8_t is_valid;
    hdc2010::TempHumData temp_hum;
    uint8_t led3;
    uint8_t alarm;
};

void init(Mode init_mode);

/**
 * @brief Switch current operating mode
 * @param ensure_idle_time When switching to slower mode, must disable sens
 * for 10 minimum
 */
void switchMode(Mode new_mode, bool ensure_idle_time);

Mode getCurMode();
/**
 * @brief Update environment data for next calculation (temp, humidity)
 */
void writeEnvData(const sensor::hdc2010::TempHumData& env_data);

bool isDataReady();
GasData getLastSample();

void discardBaseline();
void rotary_read();
void irqHandler();

void rotary_irqHandler(); 
}
}

#endif
