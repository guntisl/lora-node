
#include <cstdint>
#include <stm32wbxx.h>
#include <sys_config.h>
#include <sys/syscalls.h>

#include "sys_defines.h"
#include "config/nv_config.h"
#include "config/dev_config.h"
#include "env_sensors.h"
#include "REED_SWITCH.h"
#include "sensor_data.h"


unsigned button_flag = 0;
unsigned reed_switch_flag = 0;
unsigned pina_flag = 0;
unsigned pinb_flag = 0;


namespace sensor {

namespace env {

EnvSensConfig::User user_conf;
const EnvSensConfig &env_conf = config.sensors.env_sens;
uint32_t sampling_int_ms;
uint8_t latched_triggers;
//uint8_t presses;
sys_time::SysTime last_report_time;

sys_time::TimerElement sampling_timer;

void enableSensorIRQs();
void samplingEvent(size_t param);


void init() {
    MSG_INFO(ENV, "Init");

    // read user config
    bool valid;
    user_conf = nvconf::readBankSafe<EnvSensConfig::User>(NVC_BANK_ENV_SENS, 0,
                                                          valid);
    if(!valid) {
        // init from defaults
        MSG_WARN(ENV, "reseting default config");
        user_conf = env_conf.user;
        nvconf::writeBankSafe(NVC_BANK_ENV_SENS, 0, user_conf);
    }

    if(!env_conf.fixed.en_module) return;

    MSG_WARN(ENV, "--=enabling sensors=--");
    enableSensorIRQs();
    opt3001::init();
    hdc2010::init();
    ccs811::init(ccs811::PULSE_60s);

    if(user_conf.sampling_inerval > 0) {
        MSG_INFO(ENV, "sampling interval T = %d'", user_conf.sampling_inerval);
        sampling_int_ms = SEC_TO_ms(user_conf.sampling_inerval);
        sys_time::startTimer(&sampling_timer, sampling_int_ms, samplingEvent, 0);
    }
}



StateReport getStateReport() {
    MaskIrqForScope irq_guard(IRQ_PRI__SENSORS);

    StateReport cur_state = {
        .timestamp = sys_time::getTime(),
        .gas = ccs811::getLastSample(),
        .illum = opt3001::getLastSample(),
        .event_trig = latched_triggers
    };

    latched_triggers = 0;

    return cur_state;
}

void setEnvTrigger(StateReport::EventTrigBitNum trigger) {
    latched_triggers |= 1 << trigger;
    trigSensorEvent(data::SensorTrigger::ENV_TRIG);
    last_report_time = sys_time::getTime();
}

void samplingEvent(size_t param) {
    auto since_last_report = sys_time::getElapsed(last_report_time);

    auto next_sample = sampling_int_ms;
    if(since_last_report < sampling_int_ms) {
        next_sample += sampling_int_ms - since_last_report;
    }

    setEnvTrigger(StateReport::EventTrigBitNum::TIME);

    sys_time::startTimer(&sampling_timer, next_sample, samplingEvent, 0);
}

void enableSensorIRQs() {
    NVIC_SetPriority(EXTI0_IRQn, ENCODE_PRIORITY(IRQ_PRI__SENSORS));
    NVIC_EnableIRQ(EXTI0_IRQn);

    NVIC_SetPriority(EXTI1_IRQn, ENCODE_PRIORITY(IRQ_PRI__SENSORS));
    NVIC_EnableIRQ(EXTI1_IRQn);

    NVIC_SetPriority(EXTI9_5_IRQn, ENCODE_PRIORITY(IRQ_PRI__SENSORS));
    NVIC_EnableIRQ(EXTI9_5_IRQn);
}





extern "C" void EXTI0_IRQHandler() {
    if (LL_EXTI_IsActiveFlag_0_31(BUTTON_EXTI_LINE)) {
        LL_EXTI_ClearFlag_0_31(BUTTON_EXTI_LINE);
        sensor::ccs811::irqHandler();
  //    MSG_WARN(ENV, "--= BUTTON interupt =--");
  //    button_flag = 1;


    }
}

extern "C" void EXTI1_IRQHandler() {
    if (LL_EXTI_IsActiveFlag_0_31(PINA_EXTI_LINE)) {
        LL_EXTI_ClearFlag_0_31(PINA_EXTI_LINE);
       sensor::ccs811::rotary_irqHandler();
    //  MSG_WARN(ENV, "--= PINB_interupt =--");

    }
}

extern "C" void EXTI9_5_IRQHandler() {
    if (LL_EXTI_IsActiveFlag_0_31(REED_SWITCH_EXTI_LINE)) {
        sensor::opt3001::irqHandler();
        LL_EXTI_ClearFlag_0_31(REED_SWITCH_EXTI_LINE);
    }
    else if (LL_EXTI_IsActiveFlag_0_31(HDC2010_EXTI_LINE)) {
  //      MSG_WARN(ENV, "--== HDC  sensors interupt ==--");
        sensor::hdc2010::irqHandler();
        LL_EXTI_ClearFlag_0_31(HDC2010_EXTI_LINE);
    }
    else if (LL_EXTI_IsActiveFlag_0_31(PINB_EXTI_LINE)) {
        sensor::ccs811::rotary_irqHandler();
        LL_EXTI_ClearFlag_0_31(PINB_EXTI_LINE);
//        MSG_WARN(ENV, "--==PINA_interupt==-");
   }
}
}
}

