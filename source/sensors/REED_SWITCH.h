
#ifndef REED_SWITCH_h
#define REED_SWITCH_h

#include "stdint.h"

namespace sensor {
namespace opt3001 {

struct __attribute__((aligned(4))) Config {
    bool high_resolution;
};

struct OpticalData {
    uint32_t illum_lux;
};

void init();
OpticalData getLastSample();

void irqHandler();

}
}

#endif

