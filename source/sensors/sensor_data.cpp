//#include "HX711.h"
#include <config/dev_config.h>
#include "sys/sys_time.h"
#include "sensor_data.h"
#include "BUTTON.h"
//#include "digital_io.h"
#include "env_sensors.h"
#include "sys_defines.h"
#include "HDC2010.h"

#include <stdint.h>
#include <stdbool.h>
#include "sensor_data.h"
#include "REED_SWITCH.h"

extern uint8_t led3_state;
extern uint8_t bee_power;
extern uint8_t food_level;
uint8_t alarm_state = 1;
uint8_t alarm = 0;
uint32_t weight = 0;
uint32_t tare_weight = 0;
uint8_t presses = 0;
extern uint8_t reed_switch;
extern uint8_t _sensor_log_start;

namespace sensor {
namespace data {

 HX711 hx711_config;
//uint8_t presses;
sys::Semaphore sample_sem(true, sys::SEM_BINARY);

FlashFifo<SensorStateReport, SENSOR_LOG_SIZE, false>
    report_log(&_sensor_log_start);

/**
 * @brief starts timer to generate and store sensor samples
 */
void startSensorMonitoring() {
    set_led(1,LOW);
    MSG_INFO(SENS, "start env sens driver");
    sensor::env::init();
    MSG_INFO(SENS, "start dig IO driver");


    sensor::hdc2010::init();

    hx711_config.gpioSck = GPIOA; 
    hx711_config.gpioData = GPIOA; 
    hx711_config.pinSck = GPIO_PIN_5; 
    hx711_config.pinData = GPIO_PIN_7; 
    hx711_config.gain = 1; 
    hx711_config.offset = -16740545;
    HX711_Init(hx711_config);
   
    if (tare_weight == 0){
        tare_weight = HX711_Average_Value(hx711_config,3);

    }
    
}



void HX711_Init(HX711 data)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = data.pinSck;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(data.gpioSck, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = data.pinData;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(data.gpioData, &GPIO_InitStruct);

	HAL_GPIO_WritePin(data.gpioSck, data.pinSck, GPIO_PIN_SET);
	HAL_Delay(50);
	HAL_GPIO_WritePin(data.gpioData, data.pinSck, GPIO_PIN_RESET);

}

int HX711_Average_Value(HX711 data, uint8_t times)
{
    int sum = 0;
    for (int i = 0; i < times; i++)
    {
        sum += HX711_Value(data);
    }

    return sum / times;
}

int HX711_Value(HX711 data)
{
    int buffer;
    buffer = 0;

    while (HAL_GPIO_ReadPin(data.gpioData, data.pinData)==1)
    ;

    for (uint8_t i = 0; i < 24; i++)
    {
    	HAL_GPIO_WritePin(data.gpioSck, data.pinSck, GPIO_PIN_SET);

        buffer = buffer << 1 ;

        if (HAL_GPIO_ReadPin(data.gpioData, data.pinData))
        {
            buffer ++;
        }

        HAL_GPIO_WritePin(data.gpioSck, data.pinSck, GPIO_PIN_RESET);
    }

    for (int i = 0; i < data.gain; i++)
    {
    	HAL_GPIO_WritePin(data.gpioSck, data.pinSck, GPIO_PIN_SET);
    	HAL_GPIO_WritePin(data.gpioSck, data.pinSck, GPIO_PIN_RESET);
    }

    buffer = buffer ^ 0x800000;

    return buffer;
}

HX711 HX711_Tare(HX711 data, uint8_t times)
{
    int sum = HX711_Average_Value(data, times);
    data.offset = sum;
    return data;
}







size_t waitNewReport(uint32_t timeout) {
    sample_sem.timedWait(timeout);
    return report_log.countStored();
}

void trigSensorEvent(data::SensorTrigger trigger) {
    SensorStateReport report;
    // sample data for report
    if(trigger == data::SensorTrigger::DIO_TRIG) {


    }
    else {
        report.trig_data.env_data = env::getStateReport();

        MSG_DEBUG(SENS, "env report:");
        report.trig_data.env_data.gas.led3 = led3_state;
        report.trig_data.env_data.gas.alarm = alarm_state;
        MSG_DEBUG(SENS, "Led3_state = %d", report.trig_data.env_data.gas.led3);
        report.trig_data.env_data.gas.eTVOC_ppb = bee_power;
        report.trig_data.env_data.gas.eCO2_ppm = food_level;

      //  MSG_DEBUG(SENS, "---====#### = weight =  %6d ####====---", ADS1231_getData());
        weight = HX711_Average_Value(hx711_config, 5) - tare_weight;
//        MSG_DEBUG(SENS, "---====####ADS1231 kg value = %6d ####====---", weight);
        MSG_DEBUG(SENS, "Alarm = %d", alarm_state);

          if (reed_switch) {
             // reed_switch = 0;
              report.trig_data.env_data.gas.is_valid = {1};
              set_led(1,LOW);
          }
          else {
            if (alarm_state) {
                MSG_DEBUG(SENS, "Alarm == 1");
                alarm = 1;
            }
            else {
              MSG_DEBUG(SENS, "Alarm == 0");
              alarm = 0;

            }
            report.trig_data.env_data.gas.is_valid = {0};
            reed_switch = 0;
            set_led(1,LOW);
          }




        sensor::hdc2010::startMeasure(config.sensors.hdc2010);
        sensor::hdc2010::irqHandler();
        TempHumData2 t_h2;
        t_h2.temp_hum = hdc2010::getLastSample();
        MSG_DEBUG(SENS, "Temp = %d, %d ",t_h2.temp_hum.temp_whole, t_h2.temp_hum.temp_mili_fraction);
        MSG_DEBUG(SENS, "Humidity = %d, %d ",t_h2.temp_hum.humidity_whole, t_h2.temp_hum.humidity_fraction);
        
        
        report.trig_data.env_data.illum.illum_lux = weight ;
        report.trig_data.env_data.gas.temp_hum.temp_whole = t_h2.temp_hum.temp_whole;
        report.trig_data.env_data.gas.temp_hum.temp_mili_fraction = t_h2.temp_hum.temp_mili_fraction;
        report.trig_data.env_data.gas.temp_hum.humidity_whole = t_h2.temp_hum.humidity_whole;
        report.trig_data.env_data.event_trig = {0};

        MSG_DEBUG(SENS, "CO2 %d (%s), t %d.%03d, H %d, lx %d",
                  report.trig_data.env_data.gas.eCO2_ppm,
                  report.trig_data.env_data.gas.is_valid ? "Y" : "X",
                  report.trig_data.env_data.gas.temp_hum.temp_whole,
                  report.trig_data.env_data.gas.temp_hum.temp_mili_fraction,
                  report.trig_data.env_data.gas.temp_hum.humidity_whole,
                  report.trig_data.env_data.illum.illum_lux);
        MSG_DEBUG(SENS, "trig %X", report.trig_data.env_data.event_trig);
        }
    report.trigger = trigger;

    report_log.insert(report);
    sample_sem.post();
    }
  }
}


