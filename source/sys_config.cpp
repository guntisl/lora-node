
#include <stm32wbxx_ll_usart.h>
#include <stm32wbxx_hal_rcc.h>
#include <stm32wbxx_ll_i2c.h>
#include "stm32wbxx_ll_bus.h"
#include "stm32wbxx_ll_gpio.h"
#include <stm32wbxx_ll_exti.h>
#include "sys_defines.h"
#include "sys_config.h"
#include "debug_log.h"
#include "sys_time.h"


#define EXT_UART_BAUD           1000000
#define EXT_UART_PORT           GPIOB
#define EXT_UART_TX_PIN         LL_GPIO_PIN_6
#define EXT_UART_RX_PIN         LL_GPIO_PIN_7

// approx 400kHz clk freq with 46ns dat setup and 31ns hold
#define I2C_TIMING_REG_VALUE    0x00325050

#define PLL_SOURCE_CRYSTAL      LL_RCC_PLLSOURCE_HSE
#define PLL_SOURCE_MSI          LL_RCC_PLLSOURCE_MSI

#define PLL_SRC                 PLL_SOURCE_CRYSTAL


void configMisc() {
    // configure FPU usage
#if (__FPU_PRESENT == 1) && (__FPU_USED == 1)
    SCB->CPACR |= ((3UL << 10 * 2) | (3UL << 11 * 2));  /* set CP10 and CP11 Full Access */
#endif
    // set voltage scaling
    MODIFY_REG(PWR->CR1, PWR_CR1_VOS, PWR_CR1_VOS_1);
    // disable backup domain write protection
    SET_BIT(PWR->CR1, PWR_CR1_DBP);

    NVIC_SetPriorityGrouping(IRQ_PRIORITY_GROUP);

    // configure SVC exception priority to lowest level
    NVIC_SetPriority(SVCall_IRQn, ENCODE_PRIORITY(IRQ_PRI__SVCALL));
    NVIC_SetPriority(PendSV_IRQn, ENCODE_PRIORITY(IRQ_PRI__PENDSV));

    DBGMCU->APB1FZR1 |= DBGMCU_APB1FZR1_DBG_LPTIM1_STOP;
    // stop TIM1 during debug
    DBGMCU->C2APB2FZR |= DBGMCU_APB2FZR_DBG_TIM1_STOP;

    // enable faults
    SCB->SHCSR |= SCB_SHCSR_USGFAULTENA_Msk | SCB_SHCSR_BUSFAULTENA_Msk
                  | SCB_SHCSR_MEMFAULTENA_Msk;

    // enable div by 0 fault
    SCB->CCR |= SCB_CCR_DIV_0_TRP_Msk;

    // set wait states for memory
    LL_FLASH_SetLatency(LL_FLASH_LATENCY_3);
    while (LL_FLASH_GetLatency() != LL_FLASH_LATENCY_3);

    LL_FLASH_EnablePrefetch();
    LL_FLASH_EnableInstCache();
    LL_FLASH_EnableDataCache();
}

void configPins() {
    LL_GPIO_InitTypeDef pin_config;
    LL_EXTI_InitTypeDef exti_config;

    // config LED pin
    LL_GPIO_StructInit(&pin_config);
    pin_config.Pin = LED1_PIN;
    pin_config.Mode = LL_GPIO_MODE_OUTPUT;
    LL_GPIO_Init(LED1_PORT, &pin_config);

    LL_GPIO_StructInit(&pin_config);
    pin_config.Pin = LED2_PIN;
    pin_config.Mode = LL_GPIO_MODE_OUTPUT;
    LL_GPIO_Init(LED2_PORT, &pin_config);

    LL_GPIO_StructInit(&pin_config);
    pin_config.Pin = LED3_PIN;
    pin_config.Mode = LL_GPIO_MODE_OUTPUT;
    LL_GPIO_Init(LED3_PORT, &pin_config);

    LL_GPIO_StructInit(&pin_config);
    pin_config.Pin = REG_CLK_PIN;
    pin_config.Mode = LL_GPIO_MODE_OUTPUT;
    LL_GPIO_Init(REG_CLK_PORT, &pin_config);

    LL_GPIO_StructInit(&pin_config);
    pin_config.Pin = REG_LATCH_PIN;
    pin_config.Mode = LL_GPIO_MODE_OUTPUT;
    LL_GPIO_Init(REG_LATCH_PORT, &pin_config);

    LL_GPIO_StructInit(&pin_config);
    pin_config.Pin = REG_DATA_IN_PIN;
    pin_config.Mode = LL_GPIO_MODE_OUTPUT;
    LL_GPIO_Init(REG_DATA_IN_PORT, &pin_config);

    LL_GPIO_StructInit(&pin_config);
    pin_config.Pin = DOUT_PIN;
    pin_config.Mode = LL_GPIO_MODE_INPUT;
    LL_GPIO_Init(DOUT_PORT, &pin_config);

    LL_GPIO_StructInit(&pin_config);
    pin_config.Pin = SCK_PIN;
    pin_config.Mode = LL_GPIO_MODE_OUTPUT;
    LL_GPIO_Init(SCK_PORT, &pin_config);

    LL_GPIO_StructInit(&pin_config);
    pin_config.Pin = REED_SWITCH_IRQ_PIN;
    pin_config.Mode = LL_GPIO_MODE_INPUT;
    LL_GPIO_Init(REED_SWITCH_IRQ_PORT, &pin_config);

    LL_GPIO_StructInit(&pin_config);
    pin_config.Pin = PINA_Pin;
    pin_config.Mode = LL_GPIO_MODE_INPUT;
    pin_config.Pull = LL_GPIO_PULL_UP;
    LL_GPIO_Init(PINA_Pin_PORT, &pin_config);

    exti_config.Line_0_31 = PINA_EXTI_LINE;
    exti_config.Line_32_63 = 0;
    exti_config.LineCommand = ENABLE;
    exti_config.Mode = LL_EXTI_MODE_IT;
    exti_config.Trigger = LL_EXTI_TRIGGER_RISING_FALLING;
    LL_EXTI_Init(&exti_config);
    LL_EXTI_ClearFlag_0_31(PINA_EXTI_LINE);

    LL_GPIO_StructInit(&pin_config);
    pin_config.Pin = PINB_Pin;
    pin_config.Mode = LL_GPIO_MODE_INPUT;
    pin_config.Pull = LL_GPIO_PULL_UP;
    LL_GPIO_Init(PINB_Pin_PORT, &pin_config);

    exti_config.Line_0_31 = PINB_EXTI_LINE;
    exti_config.Line_32_63 = 0;
    exti_config.LineCommand = ENABLE;
    exti_config.Mode = LL_EXTI_MODE_IT;
    exti_config.Trigger = LL_EXTI_TRIGGER_RISING_FALLING;
    LL_EXTI_Init(&exti_config);
    LL_EXTI_ClearFlag_0_31(PINB_EXTI_LINE);

    // configure BUTTON pin
    LL_GPIO_StructInit(&pin_config);
    pin_config.Pin = BUTTON_PIN;
    pin_config.Mode = LL_GPIO_MODE_INPUT;
    LL_GPIO_Init(BUTTON_PORT, &pin_config);

    // configure HDC2010 IRQ pin
    LL_GPIO_StructInit(&pin_config);
    pin_config.Pin = HDC2010_IRQ_PIN;
    pin_config.Mode = LL_GPIO_MODE_INPUT;
    LL_GPIO_Init(HDC2010_IRQ_PORT, &pin_config);

    // config UART pins
    pin_config.Pin = EXT_UART_TX_PIN | EXT_UART_RX_PIN;
    pin_config.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    pin_config.Mode = LL_GPIO_MODE_ALTERNATE;
    pin_config.Pull = LL_GPIO_PULL_UP;
    pin_config.Speed = LL_GPIO_SPEED_FREQ_HIGH;
    pin_config.Alternate = LL_GPIO_AF_7;
    LL_GPIO_Init(EXT_UART_PORT, &pin_config);
}

void configClocks() {
    // enable external crystals
    LL_RCC_HSE_Enable();    // 32M
    LL_RCC_LSE_SetDriveCapability(LL_RCC_LSEDRIVE_LOW);
    LL_RCC_LSE_Enable();    // 32k

    // configure PLL
    LL_RCC_PLL_Disable();

    LL_RCC_PLL_DisableDomain_SYS();
    LL_RCC_PLL_DisableDomain_48M();
    LL_RCC_PLL_DisableDomain_SAI();
    LL_RCC_PLL_DisableDomain_ADC();

    LL_RCC_MSI_SetRange(LL_RCC_MSIRANGE_10);

    // configure SYSCLK domain = 64 M
    LL_RCC_PLL_ConfigDomain_SYS(PLL_SRC, LL_RCC_PLLM_DIV_4,
                                16, LL_RCC_PLLR_DIV_2);

    // USB/RNG domain = 48 M
    LL_RCC_PLL_ConfigDomain_48M(PLL_SRC, LL_RCC_PLLM_DIV_4,
                                12, LL_RCC_PLLQ_DIV_2);

    // SAI domain = 8 M
    LL_RCC_PLL_ConfigDomain_SAI(PLL_SRC, LL_RCC_PLLM_DIV_4,
                                8, LL_RCC_PLLP_DIV_8);

    // ADC domain = 64 M
    LL_RCC_PLL_ConfigDomain_ADC(PLL_SRC, LL_RCC_PLLM_DIV_4,
                                16, LL_RCC_PLLP_DIV_2);

    LL_RCC_PLL_EnableDomain_SYS();
    LL_RCC_PLL_EnableDomain_48M();
    // don't need SAI so not enabled
    // use SYSCLK for ADC so don't enable

#if PLL_SRC == PLL_SOURCE_CRYSTAL
    // wait HSE ready
    while (!LL_RCC_HSE_IsReady());
#endif
    // enable
    LL_RCC_PLL_Enable();

    // wait PLL ready
    while (!LL_RCC_PLL_IsReady());

    // wait LSE ready
    while (!LL_RCC_LSE_IsReady());

    // configure clock tree dividers while using MSI clock
    LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);    // AHB = 64 MHz
    LL_C2_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1); // AHB2 = 64 Mhz
    LL_RCC_SetAHB4Prescaler(LL_RCC_SYSCLK_DIV_1);   // AHB4 = 64 MHz
    LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_1);     // APB1 = 64 MHz
    LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1);     // APB1 = 64 MHz

    // switch to PLL as clock source
    LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);

    // set peripheral clock source
    LL_RCC_ConfigRNGClockSource(LL_RCC_RNG_CLKSOURCE_CLK48,
                                LL_RCC_CLK48_CLKSOURCE_PLL);

    LL_RCC_SetLPTIMClockSource(LL_RCC_LPTIM1_CLKSOURCE_LSE);
    LL_RCC_SetI2CClockSource(LL_RCC_I2C1_CLKSOURCE_SYSCLK);
    LL_RCC_SetUSARTClockSource(LL_RCC_USART1_CLKSOURCE_SYSCLK);
    LL_RCC_SetADCClockSource(LL_RCC_ADC_CLKSOURCE_SYSCLK);

    LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOA);
    LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOB);
    LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOE);
    LL_AHB3_GRP1_EnableClock(LL_AHB3_GRP1_PERIPH_RNG);
    LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_I2C1);
    LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_LPTIM1);
    LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_USART1);

    SystemCoreClockUpdate();
}

/**
 * @brief Configure UART for logging/control
 */
void configUART() {
    LL_USART_DeInit(EXT_UART);
    LL_USART_Disable(EXT_UART);

    LL_USART_InitTypeDef uart_config;
    uart_config.BaudRate = EXT_UART_BAUD;
    uart_config.DataWidth = LL_USART_DATAWIDTH_8B;
    uart_config.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
    uart_config.OverSampling = LL_USART_OVERSAMPLING_8;
    uart_config.Parity = LL_USART_PARITY_NONE;
    uart_config.StopBits = LL_USART_STOPBITS_1;
    uart_config.PrescalerValue = LL_USART_PRESCALER_DIV1;
    uart_config.TransferDirection = LL_USART_DIRECTION_TX_RX;

    LL_USART_Init(EXT_UART, &uart_config);
    LL_USART_EnableRxTimeout(EXT_UART);

    // enable FIFO treshold IRQ and RX no activity timeout
    LL_USART_EnableFIFO(EXT_UART);
    LL_USART_SetRXFIFOThreshold(EXT_UART, LL_USART_FIFOTHRESHOLD_7_8);
    LL_USART_EnableIT_RXFT(EXT_UART);
    LL_USART_EnableIT_RTO(EXT_UART);
    LL_USART_SetRxTimeout(EXT_UART, 1000);

    NVIC_SetPriority(USART1_IRQn, ENCODE_PRIORITY(IRQ_PRI__USART1));
    NVIC_EnableIRQ(USART1_IRQn);

    LL_USART_Enable(EXT_UART);
}

/**
 * @brief Sometimes slave device can be left in bad state driving lines
 * to recover issue 9 clock pulses + till SDA is released
 */
void i2cBusErrorUnlock() {
    LL_GPIO_InitTypeDef pin_config;
    pin_config.Pin = SCL_PIN | SDA_PIN;
    pin_config.OutputType = LL_GPIO_OUTPUT_OPENDRAIN;
    pin_config.Mode = LL_GPIO_MODE_OUTPUT;
    pin_config.Alternate = LL_GPIO_AF_0;
    pin_config.Pull = LL_GPIO_PULL_NO;
    pin_config.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
    LL_GPIO_Init(I2C_GPIO, &pin_config);

    LL_GPIO_SetOutputPin(I2C_GPIO, SDA_PIN);
    for(size_t i = 0; i < 10; i++) {
        LL_GPIO_SetOutputPin(I2C_GPIO, SCL_PIN);
        sys_time::delay_us(10);
        LL_GPIO_ResetOutputPin(I2C_GPIO, SCL_PIN);
        sys_time::delay_us(10);
    }
    if(!LL_GPIO_IsInputPinSet(I2C_GPIO, SDA_PIN)) {
        MSG_ERROR(SYS, "I2C SDA pin not released");
    }
}

void configI2C() {
    i2cBusErrorUnlock();

    LL_GPIO_InitTypeDef pin_config;
    pin_config.Pin = SCL_PIN | SDA_PIN;
    pin_config.OutputType = LL_GPIO_OUTPUT_OPENDRAIN;
    pin_config.Mode = LL_GPIO_MODE_ALTERNATE;
    pin_config.Alternate = LL_GPIO_AF_4;
    pin_config.Pull = LL_GPIO_PULL_NO;
    pin_config.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
    LL_GPIO_Init(I2C_GPIO, &pin_config);

    SET_BIT(RCC->APB1ENR1, RCC_APB1ENR1_I2C1EN);
    LL_I2C_Disable(I2C_BUS);
    LL_I2C_EnableClockStretching(I2C_BUS);
    LL_I2C_SetMasterAddressingMode(I2C_BUS, LL_I2C_ADDRESSING_MODE_7BIT);
    LL_I2C_DisableOwnAddress1(I2C_BUS);
    LL_I2C_DisableOwnAddress2(I2C_BUS);
    LL_I2C_SetTiming(I2C_BUS, I2C_TIMING_REG_VALUE);
    LL_I2C_SetMode(I2C_BUS, LL_I2C_MODE_I2C);
    LL_I2C_EnableClockStretching(I2C_BUS);
    LL_I2C_Enable(I2C_BUS);
    MSG_INFO(SYS, "init I2C");
}
