
#include "stm32wbxx.h"
#include "version_str.h"
#include "version.h"

const VersionInfo version __attribute__((used, section(".rodata.version"))) = {
    .git_sha = GIT_REV
};


ChipID getChipUID() {
    ChipID uid = {
        .ID96_1 = *(uint32_t *) UID_BASE,
        .ID96_2 = *(uint32_t *) UID_BASE + 4,
        .ID96_3 = *(uint32_t *) UID_BASE + 8,
        .FLASH_UID64 = *(uint64_t *) UID64_BASE,
    };
    return uid;
}