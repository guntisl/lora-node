
#ifndef serial_port_h
#define serial_port_h

#include <cstdint>
#include <cstdlib>

namespace serial {

int write(const uint8_t *ptr, uint16_t len);
int print_str(const char* str);

}

#endif
