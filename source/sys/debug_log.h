
#ifndef debug_log_h
#define debug_log_h

#ifdef __cplusplus
extern "C" {
#endif

#include <stdarg.h>
#include <assert.h>
#include <stdlib.h>
#include <stdint.h>

#include "debug_config.h"
#include "sys/syscalls.h"
#include "sys/exceptions.h"

typedef enum {
    LEV_OFF = 0,
    LEV_ERROR,
    LEV_WARN,
    LEV_INFO,
    LEV_DEBUG,
    LEV_DUMP,
    LEV_COUNT
} Level;


void debug_log(const char *fmt, ...);
void debug_msg(Level level, Scope scope, const char *fmt, ...);
void hex_dump(Level level, Scope scope, const void *bin_data, size_t count);

void mute(uint32_t timeout);
void setLogLevel(Scope scope, Level level);

#if DEBUG_PRINT_EN
#define LOG(fmt, ...)               debug_log(fmt, ##__VA_ARGS__)
#define MSG(level, scope, fmt, ...) debug_msg(level, scope, fmt, ##__VA_ARGS__)
#define MUTE(timeout)               mute(timeout)
#define SET_LEVEL(scope, level)     setLogLevel(scope, level)
#else
#define LOG(fmt, ...)               ((void)0)
#define MSG(level, scope, fmt, ...) ((void)0)
#define MUTE(t)                     ((void)0)
#define SET_LEVEL(scope, level)     ((void)0)
#endif

#if HEX_MSG_EN && DEBUG_PRINT_EN
#define HEX_MSG(level, scope, bin, len)     hex_dump(level, scope, bin, len)
#else
#define HEX_MSG(level, scope, bin, len)     ((void)0)
#endif

#if ERROR_MSG_EN
#define MSG_ERROR(scope, fmt, ...)  MSG(LEV_ERROR, scope, fmt, ##__VA_ARGS__)
#else
#define MSG_ERROR(scope, fmt, ...)  ((void)0)
#endif

#if WARN_MSG_EN
#define MSG_WARN(scope, fmt, ...)   MSG(LEV_WARN, scope, fmt, ##__VA_ARGS__)
#else
#define MSG_WARN(scope, fmt, ...)   ((void)0)
#endif

#if INFO_MSG_EN
#define MSG_INFO(scope, fmt, ...)   MSG(LEV_INFO, scope, fmt, ##__VA_ARGS__)
#else
#define MSG_INFO(scope, fmt, ...)   ((void)0)
#endif

#if DEBUG_MSG_EN
#define MSG_DEBUG(scope, fmt, ...)  MSG(LEV_DEBUG, scope, fmt, ##__VA_ARGS__)
#else
#define MSG_DEBUG(scope, fmt, ...)  ((void)0)
#endif

#if DUMP_MSG_EN
#define MSG_DUMP(scope, fmt, ...)   MSG(LEV_DUMP, scope, fmt, ##__VA_ARGS__)
#else
#define MSG_DUMP(scope, fmt, ...)   ((void)0)
#endif

#if EN_EXC_LOG
#define FATAL_ERROR(msg)    fatalErrorFunc(__FUNCTION__, msg)
#else
#define FATAL_ERROR(msg)    systemReset()
#endif

#if EN_EXC_LOG
#define STRONG_ASSERT(__e, msg, ...)                            \
    ((__e)                                                      \
    ? ((void)0)                                                 \
    : (LOG("ASSERT: " msg NEWLINE, ##__VA_ARGS__),              \
       __assert_func("", 0, __FUNCTION__, #__e)))
#else
#define STRONG_ASSERT(__e, msg, ...)    ((__e) ? ((void)0) : systemReset())
#endif

#ifdef NDEBUG
#define ASSERT(__e, msg, ...)       ((void)0)
#else
#define ASSERT(__e, msg, ...)                                   \
    ((__e)                                                      \
    ? ((void)0)                                                 \
    : (LOG("ASSERT: " msg NEWLINE, ##__VA_ARGS__),              \
       __assert_func("", 0, __FUNCTION__, #__e)))
#endif


#ifdef __cplusplus
}
#endif

#endif
