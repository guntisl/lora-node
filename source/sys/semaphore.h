
#ifndef semaphore_h
#define semaphore_h

#include <limits>
#include "misc/linked_list.h"
#include "scheduler.h"

// set to IRQ priority that is highest level in which semaphores are used
#define HIGHEST_IRQ_PRI_USED            1

namespace sys {

enum SemaphoreType : uint8_t {
    SEM_BINARY = 0,
    SEM_COUNTING
};

class Semaphore {
    class WaitingThread {
    public:
        sch::ThreadHandle handle;
        uint8_t priority;

        bool operator<(WaitingThread & other) {
            return priority < other.priority;
        }
    };

    uint32_t value;
    bool allow_stop;
    bool binary;
    SemaphoreType type;
    SinglyLinkedList<WaitingThread> waiting_threads;
public:
    static constexpr uint32_t WAIT_FOREVER =
        std::numeric_limits<uint32_t>::max();

    explicit Semaphore(bool can_stop, SemaphoreType sem_type) : value(0),
        allow_stop(can_stop), type(sem_type) {}

    void post();
    bool tryWait();
    bool timedWait(uint32_t timeout);
};

}

#endif
