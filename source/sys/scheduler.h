
#ifndef scheduler_h
#define scheduler_h

#include <cstdlib>
#include <cstdint>
#include "sys_time.h"
#include "sleep.h"

#ifndef STACK_CHECK
#define STACK_CHECK             1
#endif
// to disable set 0
#ifndef THREAD_NAME_LEN
#define THREAD_NAME_LEN         8
#endif

#define STACK_ALIGN             __attribute__((aligned(8)))

namespace sys {
namespace sch {

enum ThreadSignalIds : uint32_t {
    SIG_SEMAPHORE = 1
};

typedef void *ThreadHandle;

void threadClearSignal();
void threadSetSignal(ThreadHandle thread, uint32_t sig);
uint32_t threadGetSignal(ThreadSignalIds sig);
void threadSleep(uint32_t time, sys::PowerSaveMode ps_state);
void threadSuspend(sys::PowerSaveMode ps_state);
void threadYield();

void wakeThread(ThreadHandle thread);
ThreadHandle myHandle();
uint8_t myPriority();

void
startScheduler(const char *name, void *stack, size_t stack_size, uint8_t priority, void (*entry)());
ThreadHandle createNewThread(const char *name, void *stack, size_t stack_size,
                             uint8_t priority, void (*entry)());


}
}

#endif
