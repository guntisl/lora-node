
#include <sys/misc/macros.h>
#include <sys/stat.h>
#include <stdint.h>
#include <sched.h>
#include "debug_log.h"
#include "serial_port.h"
#include "lora.h"
#include "syscalls.h"
#include "led.h"

// Declare heap start address from linker script
extern uint32_t _sheap;
extern uint32_t _eheap;

struct ServiceCallDefs {
    void (*callback)();
};

constexpr ServiceCallDefs svc_list[] = {
    [SVC_LORA] = { lora::procTasks },
    [SVC_LPTIM] = { sys_time::timerService }
};

bool svc_pending[] = {
    [SVC_LORA] = false,
    [SVC_LPTIM] = false
};

constexpr size_t svc_cb_list_size = sizeof(svc_list) / sizeof(ServiceCallDefs);
constexpr size_t svc_pend_list_size = sizeof(svc_pending) / sizeof(bool);

extern "C" {

NORET void systemReset() {
    __DSB();

    SCB->AIRCR = (uint32_t) ((0x5FAUL << SCB_AIRCR_VECTKEY_Pos) |
                             (SCB->AIRCR & SCB_AIRCR_PRIGROUP_Msk) |
                             SCB_AIRCR_SYSRESETREQ_Msk);
    __DSB();

    for (;;) {
        __NOP();
    }
}

void requestService(uint32_t code) {
    svc_pending[code] = true;
    SCB->ICSR |= SCB_ICSR_PENDSVSET_Msk;
    __DSB();
}

USED void processServices() {
    for (size_t i = 0; i < svc_pend_list_size; i++) {
        if (svc_pending[i]) {
            svc_pending[i] = false;
            svc_list[i].callback();
        }
    }
}

void disableIRQ() {
    __disable_irq();
}

void enableIRQ() {
    __enable_irq();
}

/*
 * @brief Writes up to count bytes from the buffer starting at buf to
 *        the file referred to by the file descriptor fd.
 * @return Return -1 on error, otherwise bytes written.
 */
USED int _write(int fd, const void *buf, size_t count) {
    return serial::write((const uint8_t *) buf, count);
}

/*
 * @brief Attempts to read up to count bytes from file descriptor fd
 *        into the buffer starting at buf.
 * @return Returns the number of bytes that were read.
 *         If value is negative, then the system call returned an error.
 */
USED int _read(int fd, void *buf, size_t count) {
    // not implemented
    return -1;
}

/*
 * @brief Repositions the file offset of the open file description
 *     associated with the file descriptor fd to the argument offset
 *     according to the directive whence
 * @return Returns the offset of the pointer (in bytes) from the beginning of
 *         the file. If the return value is -1, then there was an error moving
 *         the pointer.
 */
USED int _lseek(int fd, off_t offset, int whence) {
    // not implemented
    return -1;
}

/*
 * @brief Closes a file descriptor, so that it no longer refers to any
          file and may be reused.
 * @return If success returns 0, else -1
 */
USED int _close(int fd) {
    // not implemented
    return -1;
}

/*
 * @brief System call opens the file specified by pathname
 * @return File ID, or -1 if failed
 */
USED int _open(const char *pathname, int flags, int mode) {
    // not implemented
    return -1;
}

/*
 * @brief Change the location of the program break, which defines the end of
 *        the process's data segment.
 * @param incr Amount by which to increment/decrement memory space.
 * @return On success, sbrk() returns the previous program break.
 *         On error, (void *) -1 is returned.
 * @details This is syscall that gives out regions of heap for usage by malloc
*/
USED void *_sbrk(int incr) {
    static uint32_t cur_prog_brake = (uint32_t) &_sheap;

    void *address = (void *) cur_prog_brake;

    if (incr >= 0) {
        // Check if enough free space for block allocation
        if (cur_prog_brake + incr < (uint32_t) &_eheap) {
            // Can increase memory
            cur_prog_brake += incr;
        }
        else {
            // No more free space to allocate - abort
            address = (void *) -1;
            FATAL_ERROR("out of heap");
        }
    }
    else {
        // Decrement of used memory
        if (cur_prog_brake + incr > (uint32_t) &_sheap) {
            cur_prog_brake += incr;
        }
        else {
            // Bad release of memory - more than start of heap
            address = (void *) -1;
            FATAL_ERROR("bad heap release");
        }
    }
    //Return current program data break
    return address;
}

/*
 * @brief Returns the process ID (PID) of the calling process.
 */
USED int _getpid(void) {
    // not implemented
    return 1;
}

/*
 * @brief The kill() system call can be used to send any signal to any process
 *        group or process
 * @return On success (at least one signal was sent), zero is returned.  On
 *         error, -1 is returned.
 */
USED int _kill(int pid, int sig) {
    // not implemented
    return -1;
}

/**
 * @brief The fstat() function shall obtain information about an open file
 *        associated with the file descriptor fildes, and shall write it to the
 *        area pointed to by buf.
 * @return Upon successful completion, 0 shall be returned. Otherwise, −1 shall
 *         be returned
 */
USED int _fstat(int file, struct stat *st) {
    // not implemented
    return -1;
}

/**
 * @brief The isatty() function tests whether fd is an open file descriptor
 *        referring to a terminal.
 * @return Returns 1 if fd is an open file descriptor referring to aterminal;
 *         otherwise 0 is returned
 */
USED int _isatty(int file) {
    // not implemented
    return 1;
}

/*
 * @brief Lock memory pool for safe multi thread use of malloc
 * @details If multiple threads of execution can call <<malloc>>, or if
 *          <<malloc>> can be called reentrantly, then you need to define your
 *          own versions of these functions in order to safely lock the memory
 *          pool during a call.
 */
USED void __malloc_lock(struct _reent *reent) {
    // not implemented
}

/*
 * @brief Lock memory pool for safe multi thread use of malloc
 * @details If multiple threads of execution can call <<malloc>>, or if
 *          <<malloc>> can be called reentrantly, then you need to define your
 *          own versions of these functions in order to safely lock the memory
 *          pool during a call.
 */
USED void __malloc_unlock(struct _reent *reent) {
    // not implemented
}

/*
 * @brief Use <<exit>> to return control from a program to the host operating
 *        environment.
 */
USED void _exit(int) {
    // not implemented
}



}
