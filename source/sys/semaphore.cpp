
#include "semaphore.h"
#include "syscalls.h"

using namespace sys::sch;

namespace sys {

OPTIM_SPEED void Semaphore::post() {
    MaskIrqForScope irq_off(HIGHEST_IRQ_PRI_USED);

    if(type == SEM_COUNTING) {
        value++;
    }
    else if(type == SEM_BINARY) {
        value = 1;
    }

    // go trough thread list and set signals value times
    for(auto &it : waiting_threads) {
        if(value == 0) break;
        value--;
        threadSetSignal(it.data.handle, ThreadSignalIds::SIG_SEMAPHORE);
        wakeThread(it.data.handle);
    }
}

OPTIM_SPEED bool Semaphore::tryWait() {
    MaskIrqForScope irq_off(HIGHEST_IRQ_PRI_USED);
    if(value > 0) {
        value--;
        return true;
    }
    return false;
}

OPTIM_SPEED bool Semaphore::timedWait(uint32_t timeout) {
    decltype(waiting_threads)::Container hndl_cont;

    {
        MaskIrqForScope irq_off(HIGHEST_IRQ_PRI_USED);

        if(value > 0) {
            value--;
            return true;
        }

        // add me to waiting list
        hndl_cont.data.handle = myHandle();
        hndl_cont.data.priority = myPriority();
        waiting_threads.pushSorted(hndl_cont);
        threadClearSignal();

        // set wait mode
        auto sleep_mode = allow_stop ? sys::PS_MODE_STOP2 : sys::PS_MODE_SLEEP;
        if(timeout == WAIT_FOREVER) {
            threadSuspend(sleep_mode);
        }
        else {
            threadSleep(timeout, sleep_mode);
        }
    }
    // start waiting
    // ---
    // wake from signal or from timeout
    {
        MaskIrqForScope irq_off(HIGHEST_IRQ_PRI_USED);
        waiting_threads.remove(hndl_cont);
    }
    return threadGetSignal(ThreadSignalIds::SIG_SEMAPHORE) != 0;
}

}