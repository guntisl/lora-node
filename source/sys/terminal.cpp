
#include <cstring>
#include <cctype>
#include <cstdio>
#include "sys_defines.h"
#include "debug_log.h"
#include "terminal.h"


namespace term {

Terminal::Terminal(ReplyStr reply_sender, const CmdList cmd_list[], size_t cmd_count)
        : lb_idx(0), reply_func(reply_sender), commands(cmd_list),
        cmd_count(cmd_count) {
    // called pre main
    char_mute_time = TERMINAL_LOG_MUTE_TIMEOUT;
    cmd_done_mute_timeout = CMD_DONE_MUTE_TIMEOUT;
}

void Terminal::searchCmd() {
    for(size_t i = 0; i < cmd_count; i++) {
        // compare if command is received
        auto cmp = strncmp((char*)line_buffer, commands[i].cmd_str,
                            commands[i].cmd_len);

        // check that not part of longer command name
        auto cmd_delimiter = *(line_buffer + commands[i].cmd_len);
        auto delimiter_check = isspace(cmd_delimiter) ||  iscntrl(cmd_delimiter);

        if(cmp == 0 && delimiter_check) {
            auto args_ptr = (char*)line_buffer + commands[i].cmd_len;
            if(commands[i].cmd_func(*this, args_ptr)) {
                reply_func("cmd_ok\r\n");
            }
            else {
                reply_func("cmd_error\r\n");
            }

            // no need to check for other commands
            return;
        }
    }
    reply_func("unknown command\r\n");
}

void Terminal::inputText(uint8_t *data, size_t len) {
#if TERMINAL_ECHO
    sendEcho(data, len);
#endif

    MUTE(char_mute_time);

    for(size_t i = 0; i < len; i++) {
        // check for command delimiters
        if(iscntrl(data[i]) || data[i] == ';') {
            // check that there is line to process
            if(lb_idx > 0) {
                // terminate so can process as string
                line_buffer[lb_idx] = 0;
                searchCmd();
                // reset even if no valid command is found
                lb_idx = 0;
            }
            MUTE(cmd_done_mute_timeout);
        }
        else {
            line_buffer[lb_idx] = data[i];
            lb_idx++;
            checkOverflow();
        }
    }
}

void Terminal::fmtSend(const char* fmt, ...) {
    va_list arg_list;
    va_start(arg_list, fmt);
    auto status = vsnprintf(print_buffer, sizeof(print_buffer), fmt, arg_list);
    va_end(arg_list);
    ASSERT(status >= 0, "status %d", status);

    reply_func(print_buffer);
}

void Terminal::setMuteTimeout(uint32_t on_char, uint32_t on_cmd_done) {
    char_mute_time = on_char;
    cmd_done_mute_timeout = on_cmd_done;
}

void Terminal::checkOverflow() {
    if(lb_idx >= TERM_MAX_LINE_LEN) {
        lb_idx = 0;
        // send warning message
        reply_func("buffer overflow! data flushed!\r\n");
    }
}

void Terminal::sendEcho(uint8_t *data, size_t len) {
#if TERMINAL_ECHO
    char str[2] = { 0, 0 };

    for(size_t i = 0; i < len; i++) {
        if(data[i] == '\r') {
            str[0] = '\n';
            reply_func(str);
        }
        str[0] = data[i];
        reply_func(str);
    }
#endif
}

}
