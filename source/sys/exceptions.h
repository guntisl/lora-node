
#ifndef exceptions_h
#define exceptions_h


__attribute__((noreturn)) void __assert_func(const char *file, int lineNr,
    const char *function, const char *expression);

__attribute__((noreturn)) void fatalErrorFunc(const char *function,
                                              const char *message);


#endif
