
#include <limits>
#include <stdint.h>

#include "stm32wbxx_ll_lptim.h"
#include "stm32wbxx_hal_cortex.h"
#include "stm32wbxx.h"
#include "sys_defines.h"
#include "sys_config.h"
#include "sys_time.h"
#include "debug_log.h"
#include "syscalls.h"

// LPTIM1 + LSE
#define SLEEP_TIM                           LPTIM1

#define LSE_CLK_PERIOD                      (1000000/LSE_VALUE)
#define LETIM_ENABLE_DELAY                  (LSE_CLK_PERIOD * 25 / 10)
#define HALF_LSE_FREQ                       (LSE_VALUE / 2)
#define LSE_FREQ                            LSE_VALUE
#define LPTIM_CLK_DIV                       32
#define MS_MULT                             1000
#define MIN_DELAY_TICKS                     4
#define ARR_UPDATE_TICKS                    2
#define MAX_SLEEP_TIME                      60000
#define MAX_ARR_VAL                         0xFFF0

// systick
#define TICK_RATE                           1000U
#define TICK_PERIOD_ms                      1000U / TICK_RATE

#define USE_LPTIM_AS_SYS_CLK                1

#define SYS_TIME_OPT                        OPTIM_SPEED

static_assert(MIN_DELAY_TICKS > ARR_UPDATE_TICKS,
    "This is critical for function");
static_assert(MIN_DELAY_TICKS - ARR_UPDATE_TICKS >= 2,
    "This is needed so after IRQ have time to still change ARR,"
    "otherwise will get stuck on MIN_DELAY_TICKS value forever");

namespace sys_time {

/**
 * @brief Convert [ms] units to LPTIM clock ticks
 * @details basic equation "ticks = ms * clk / prescaler"
 *          clk = 32768 Hz
 *          prescaler = 32
 */
SYS_TIME_OPT constexpr uint32_t miliSecToTicks(uint32_t ms) {
    return (uint32_t) (((uint64_t) ms
                        * LSE_FREQ + (LPTIM_CLK_DIV * MS_MULT / 2))
                       / (LPTIM_CLK_DIV * MS_MULT));
}

/**
 * @brief Convert LPTIM clock ticks to [ms]
 * @details basic equation "ms = ticks * prescaler / clk"
 *          clk = 32768 Hz
 *          prescaler = 32
 */
SYS_TIME_OPT constexpr uint32_t tickToMiliSec(uint32_t tick) {
    return (uint32_t) (((uint64_t) tick
                        * (LPTIM_CLK_DIV * MS_MULT) + HALF_LSE_FREQ)
                       / LSE_FREQ);
}

volatile SysTime sys_time_ms;
volatile SysTime rtc_tbase_tick;
volatile uint32_t last_counter_val;



SinglyLinkedList<LPSW_Timer> active_timers;
SinglyLinkedList<LPSW_Timer> expired_timers;

extern "C" {

void configLPTIM();
void updateTimers(uint16_t elapsed_time);
uint16_t getWakeupTimerVal();
uint16_t getWakeupTimerElapsed(bool arr_event);
uint16_t getNextWakeup();
void setNextWakeup(uint16_t time);


void startSysTimer() {
    LL_RCC_ClocksTypeDef clocks;
    LL_RCC_GetSystemClocksFreq(&clocks);
    SysTick->LOAD = clocks.HCLK1_Frequency / TICK_RATE;
    SysTick->VAL = 0UL;
    SysTick->CTRL = SysTick_CTRL_CLKSOURCE_Msk
                    | SysTick_CTRL_TICKINT_Msk
                    | SysTick_CTRL_ENABLE_Msk;

    NVIC_SetPriority(SysTick_IRQn, ENCODE_PRIORITY(IRQ_PRI__SYS_TICK));

    configLPTIM();
}

#if CPU_FREQ != 48000000
#error "Adjust loop count multiplier for new CPU frequency"
#endif
NAKED void delay_us(size_t delay) {
    // adjust delay for loop cycles
    // 3x instructions for each delay count
    // CPU @ 48MHz = 16 loops for 1 us
    asm("lsls r0, #4");
    asm("loop_start:");
    // compare with 0
    asm("cbz r0, exit");
    // decrement
    asm("subs r0, #1");
    asm("b loop_start");
    asm("exit:");
    asm("bx lr");
}

SYS_TIME_OPT void delay(size_t delay) {
    auto start_time = getTime();
    while (getTime() - start_time < delay);
}

#if USE_LPTIM_AS_SYS_CLK
SYS_TIME_OPT SysTime getTime() {
    // need to to get consistent state across all variables and RTC timer
    SysTime snapshot2;
    SysTime snapshot1;
    do {
        snapshot1 = rtc_tbase_tick
                    + (getWakeupTimerVal() - last_counter_val);

        snapshot2 = rtc_tbase_tick
                    + (getWakeupTimerVal() - last_counter_val);
    } while (snapshot1 != snapshot2);

    return tickToMiliSec(snapshot1);
}
#else
SYS_TIME_OPT SysTime getTime() {
    return sys_time_ms;
}
#endif

size_t getElapsed(SysTime start_time) {
    return getTime() - start_time;
}

void SysTick_Handler() {
    sys_time_ms += TICK_PERIOD_ms;
    HAL_IncTick();
}

void configLPTIM() {
    LL_LPTIM_Disable(SLEEP_TIM);

    LL_LPTIM_SetPrescaler(SLEEP_TIM, LL_LPTIM_PRESCALER_DIV32);
    LL_LPTIM_TrigSw(SLEEP_TIM);
    LL_LPTIM_SetCounterMode(SLEEP_TIM, LL_LPTIM_COUNTER_MODE_INTERNAL);

    LL_LPTIM_Enable(SLEEP_TIM);
    sys_time::delay_us(LETIM_ENABLE_DELAY);

    LL_LPTIM_SetAutoReload(SLEEP_TIM, 0xFFFF);
    LL_LPTIM_SetCompare(SLEEP_TIM, 0);
    LL_LPTIM_ResetCounter(SLEEP_TIM);
    LL_LPTIM_EnableIT_ARRM(SLEEP_TIM);

    NVIC_SetPriority(LPTIM1_IRQn, ENCODE_PRIORITY(IRQ_PRI__WKUP));
    NVIC_EnableIRQ(LPTIM1_IRQn);

    setNextWakeup(100);

    LL_LPTIM_StartCounter(SLEEP_TIM, LL_LPTIM_OPERATING_MODE_CONTINUOUS);
}

SYS_TIME_OPT void LPTIM1_IRQHandler() {
    uint16_t sleep_time = getWakeupTimerElapsed(true);
    updateTimers(sleep_time);
    sleep_time = getNextWakeup();
    setNextWakeup(sleep_time);
}


SYS_TIME_OPT void updateTimers(uint16_t elapsed_time) {
    rtc_tbase_tick += elapsed_time;

    bool call_service = false;
    for(auto it = active_timers.begin(); it != active_timers.end();) {
        auto& tim = it->data;
        // elapsed time can be larger, because of minimum delay constraint
        // if time left is less than minimum allowed time delay, timer
        // will have larger elapsed time
        if(elapsed_time > tim.time_left) {
            tim.time_left = 0;
        }
        else {
            tim.time_left -= elapsed_time;
        }

        auto it_copy = it;
        ++it;

        // remove expired and stopped timers
        if((tim.time_left == 0) | !tim.active) {
            active_timers.remove(*it_copy);

            // schedule callbacks for active timers with cb function
            if(tim.active && tim.callback) {
                expired_timers.pushBack(*it_copy);
                call_service = true;
            }
            else {
                // flag stopped here
                tim.active = false;
            }
        }
    }

    if(call_service) {
        requestService(SVC_LPTIM);
    }
}

/**
 * @brief executed from PendSV interrupt
 */
SYS_TIME_OPT void timerService() {
    LPSW_Timer *timer;
    do {
        // take first timer from expired list till all are removed
        timer = nullptr;
        {
            MaskIrqForScope irq_off(IRQ_PRI__WKUP);
            if (expired_timers.size()) {
                auto first_tim = expired_timers.begin();
                expired_timers.remove(*first_tim);
                timer = &first_tim->data;
            }
        }

        if (timer != nullptr && timer->active && timer->callback) {
            timer->active = false;
            timer->callback(timer->param);
        }
    }while (timer != nullptr);
}

SYS_TIME_OPT void startTimer(TimerHandle handle, uint32_t time,
                            TimerCallback cb, size_t param) {
    time = miliSecToTicks(time);
    if(time < MIN_DELAY_TICKS) {
        time = MIN_DELAY_TICKS;
    }
    auto tim = (TimerElement *) handle;

    {
        MaskIrqForScope irq_off(IRQ_PRI__WKUP);

        // make sure this timer is removed from active timer list
        // also update active timers because need to insert new
        // timer in sorted position in active list
        tim->data.active = false;
        tim->data.callback = nullptr;
        updateTimers(getWakeupTimerElapsed(false));

        // make sure this timer is not in expired list
        expired_timers.remove(*tim);

        // at this point guaranteed that timer is not in any list
        // setup new settings for timer
        tim->data.time_left = time;
        tim->data.param = param;
        tim->data.callback = cb;
        tim->data.active = true;

        // insert in sorted position with other active timers
        active_timers.pushSorted(*tim);

        // get first timers sleep time (first has shortest sleep time)
        auto sleep_time = getNextWakeup();

        // set auto-reload value to sleep at most 'sleep_time', could be less,
        // if near maximum of 16bit value, then will recalculate on IRQ
        setNextWakeup(sleep_time);
    }
}

void stopTimer(TimerHandle timer) {
    auto timer_el = (TimerElement*)timer;
    timer_el->data.active = false;
}

bool isRunning(TimerHandle timer) {
    auto tim = (TimerElement *) timer;
    return tim->data.active;
}

TimerHandle createTimer() {
    // TODO: heap allocation may be not the best solution
    //  but lora driver constrains to this
    auto handle = new TimerElement();
    STRONG_ASSERT(handle != nullptr, "out of heap memory");
    return handle;
}

void destroyTimer(TimerHandle handle) {
    auto tim = (TimerElement *) handle;
    delete tim;
}

SYS_TIME_OPT void setNextWakeup(uint16_t time) {
    // enforce minimum delay, so can do all processing between interrupts
    if(time < MIN_DELAY_TICKS) {
        time = MIN_DELAY_TICKS;
    }

    // wait till previous ARR write completed
    while (!LL_LPTIM_IsActiveFlag_ARROK(SLEEP_TIM));

    // take current value as reference
    last_counter_val = getWakeupTimerVal();

    // check how long till current AR value
    uint32_t till_cur_ar = SLEEP_TIM->ARR - last_counter_val;

    // this protects against AR event happening while setting new larger value
    if (till_cur_ar < ARR_UPDATE_TICKS) {
        // very close to AR event, can't set new value,
        // risk of setting new while event resets counter
        // to 0. Just leave as is and let IRQ recalculate
        // sleep time left.
        return;
    }

    // safe to set new value
    uint32_t wakeup_time = last_counter_val + time;
    // check to ensure timer maximum limit
    if (wakeup_time > MAX_ARR_VAL) {
        // set maximal wakeup period
        wakeup_time = MAX_ARR_VAL;
    }

    // clear flag for next write
    LL_LPTIM_ClearFlag_ARROK(SLEEP_TIM);

    LL_LPTIM_SetAutoReload(SLEEP_TIM, wakeup_time);
}

SYS_TIME_OPT uint16_t getWakeupTimerVal() {
    uint16_t cnt;
    do {
        cnt = SLEEP_TIM->CNT;
    }while(cnt != SLEEP_TIM->CNT);
    return cnt;
}

SYS_TIME_OPT uint16_t getWakeupTimerElapsed(bool arr_event) {
    uint32_t elapsed;

    ASSERT(SLEEP_TIM->ARR >= last_counter_val, "last cntr val larger than ARR");

    // get correct state of timer value and flag state
    uint16_t cur_val;
    uint32_t irq_flag = arr_event ? 1 : 0;

    if(arr_event) {
        cur_val = getWakeupTimerVal();
    }
    else {
        do {
            cur_val = getWakeupTimerVal();
            irq_flag = LL_LPTIM_IsActiveFlag_ARRM(SLEEP_TIM);
        } while (cur_val != getWakeupTimerVal()
                 || irq_flag != LL_LPTIM_IsActiveFlag_ARRM(SLEEP_TIM));
    }

    LL_LPTIM_ClearFLAG_ARRM(SLEEP_TIM);

    if(irq_flag) {
        ASSERT(SLEEP_TIM->ARR >= last_counter_val, "invaid timer state");
        // counter has reached ARR and started from 0
        elapsed = SLEEP_TIM->ARR - last_counter_val + cur_val;
    }
    else {
        ASSERT(cur_val >= last_counter_val, "invaid timer state");
        // before AR event
        elapsed = cur_val - last_counter_val;
    }

    return (uint16_t)elapsed;
}

SYS_TIME_OPT uint16_t getNextWakeup() {
    uint16_t sleep_time = miliSecToTicks(MAX_SLEEP_TIME);

    if(active_timers.size()) {
        auto first = active_timers.begin();
        if(first != active_timers.end()) {
            if(first->data.time_left < sleep_time) {
                sleep_time = (decltype(sleep_time))first->data.time_left;
            }
        }
    }
    // if no timers sleep maximum
    // will be interrupted if timer gets started
    return sleep_time;
}

}
}

