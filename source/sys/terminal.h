
#ifndef terminal_h
#define terminal_h

#include <cstdint>
#include <cstdlib>

#define TERM_MAX_LINE_LEN               128
#define PRINT_BUF_SIZE                  128
#define TERMINAL_LOG_MUTE_TIMEOUT       5000
#define CMD_DONE_MUTE_TIMEOUT           1000

namespace term {

class Terminal;

typedef int32_t (*ReplyStr)(const char *str);

struct CmdList {
    const char *cmd_str;
    size_t cmd_len;
    const char *usage;
    bool (*cmd_func)(Terminal& terminal, char *arg_str);
};

/**
 * @brief Text terminal for humans
 */
class Terminal {
    uint8_t line_buffer[TERM_MAX_LINE_LEN];
    char print_buffer[PRINT_BUF_SIZE];
    size_t lb_idx;
    ReplyStr reply_func;
    const CmdList *commands;
    size_t cmd_count;
    uint32_t char_mute_time;
    uint32_t cmd_done_mute_timeout;

    void checkOverflow();
    void searchCmd();
    void sendEcho(uint8_t *data, size_t len);
public:
    /**
     * @brief Create terminal
     * @param reply Pipe to use for replies to commands, if null no reply
     * is sent. Still terminal can be used if needed.
     */
    Terminal(ReplyStr reply_sender, const CmdList cmd_list[], size_t cmd_count);
    /**
     * @brief Send raw text to process by terminal
     * @param data Pointer to string with text
     * @param len Maximal length to process
     * @details String is processed up to null byte or len whichever first
     */
    void inputText(uint8_t *data, size_t len);

    /**
     * @brief Send formatted reply to terminal
     */
    void fmtSend(const char* fmt, ...);

    void setMuteTimeout(uint32_t on_char, uint32_t on_cmd_done);
};

}

#endif
