
#include <string.h>
#include "stm32wbxx.h"
#include "internal_flash.h"
#include "debug_log.h"
#include "sys_time.h"

#define ERASE_TIMEOUT       1024u
#define WRITE_TIMEOUT       1024u

// all FLASH_SR error flags that are related to prog/erase
#define PROG_ERRORS         (FLASH_SR_PROGERR | FLASH_SR_WRPERR      \
                             | FLASH_SR_PGAERR | FLASH_SR_SIZERR     \
                             | FLASH_SR_PGSERR | FLASH_SR_MISERR     \
                             | FLASH_SR_FASTERR | FLASH_SR_RDERR)

extern uint32_t _nv_conf_start;
extern uint32_t _nv_conf_end;

namespace flash {

void unlock();
void lock();
void clearAllProgModes();
uint32_t checkClearErrors();
bool isBusy();
void waitReady(size_t timeout);


size_t eeprom_size() {
    return (uint32_t)(&_nv_conf_end - &_nv_conf_start);
}

uint8_t* eeprom_start() {
    return (uint8_t*) &_nv_conf_start;
}


void erasePage(uint32_t address) {
    waitReady(0);
    unlock();
    clearAllProgModes();
    checkClearErrors();

    if (address >= 0x08000000 && address < 0x08100000) {
        address -= 0x08000000;
    }

    auto page = (uint8_t)(address / MCU_PAGE_SIZE);

    // set page and erase mode
    MODIFY_REG(FLASH->CR, (FLASH_CR_PNB | FLASH_CR_PER),
               ((page << FLASH_CR_PNB_Pos) | FLASH_CR_PER));

    SET_BIT(FLASH->CR, FLASH_CR_STRT);

    waitReady(ERASE_TIMEOUT);

    lock();
}

void write(uint32_t address, uint8_t *data, size_t len) {
    // check alignment and length
    STRONG_ASSERT((address & 7) == 0, "");
    STRONG_ASSERT((len & 7) == 0, "");

    auto wr_ptr = (volatile uint64_t*)address;
    uint64_t data_word;

    waitReady(0);
    unlock();
    clearAllProgModes();
    checkClearErrors();

    SET_BIT(FLASH->CR, FLASH_CR_PG);
    while(len > 0) {
        memcpy(&data_word, data, sizeof(data_word));

        *wr_ptr = data_word;

        wr_ptr++;
        data += 8;
        len -= 8;

        waitReady(WRITE_TIMEOUT);
        auto status = checkClearErrors();
        if(status != 0) {
            MSG_ERROR(SYS, "FLASH write error, status %08X", status);
            FATAL_ERROR("FLASH write failed");
        }
    }
    CLEAR_BIT(FLASH->CR, FLASH_CR_PG);

    lock();
}

void clearAllProgModes() {
    CLEAR_BIT(FLASH->CR, FLASH_CR_PG | FLASH_CR_PER | FLASH_CR_MER
                         | FLASH_CR_FSTPG);
}

void unlock() {
    if (READ_BIT(FLASH->CR, FLASH_CR_LOCK) != RESET) {
        WRITE_REG(FLASH->KEYR, FLASH_KEY1);
        WRITE_REG(FLASH->KEYR, FLASH_KEY2);

        if (READ_BIT(FLASH->CR, FLASH_CR_LOCK) != RESET) {
            FATAL_ERROR("flash unlock failed");
        }
    }
}

void lock() {
    SET_BIT(FLASH->CR, FLASH_CR_LOCK);

    if (READ_BIT(FLASH->CR, FLASH_CR_LOCK) == 0x00u) {
        FATAL_ERROR("flash lock failed");
    }
}

bool isBusy() {
    return READ_BIT(FLASH->SR, FLASH_SR_BSY | FLASH_SR_CFGBSY) != 0;
}

void waitReady(size_t timeout) {
    auto start_time = sys_time::getTime();
    while (isBusy()) {
        if(sys_time::getTime() - start_time > timeout) {
            FATAL_ERROR("flash operation timeout");
        }
    }
}

uint32_t checkClearErrors() {
    auto status_reg = FLASH->SR;

    // clear all prog errors
    WRITE_REG(FLASH->SR, PROG_ERRORS);

    return status_reg & PROG_ERRORS;
}

}

