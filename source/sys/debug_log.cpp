
#include <stdarg.h>
#include <stdio.h>
#include <stdbool.h>
#include "misc/mutex.h"
#include "debug_log.h"
#include "sys_time.h"
#include "stm32wbxx.h"

#define OPTIM_DEBUG         OPTIM_SPEED

mutex log_mutex;
bool log_skipped;
const char *scope_names[] = SCOPE_PRINT_NAMES;
Level scope_dbg_levels[] = DEF_SCOPE_LOG_LEVEL;     //!< runtime adjustable
constexpr size_t scopeCount = sizeof(scope_dbg_levels) / sizeof(Level);
sys_time::SysTime mute_time;
uint32_t mute_timeout;
bool mute_log = false;

const char *level_names[] = {
    [LEV_OFF] = "",
    [LEV_ERROR] = "E",
    [LEV_WARN] = "W",
    [LEV_INFO] = "I",
    [LEV_DEBUG] = "D",
    [LEV_DUMP] = "M",
};

// store last message send time
sys_time::SysTime last_msg_time;

OPTIM_DEBUG void setLogLevel(Scope scope, Level level) {
    ASSERT(scope < scopeCount, "scope id overflow %d", scope);
    scope_dbg_levels[scope] = level;
}

/**
 * @brief If more than TSHORT elapsed since last message insert line break
 * if more than TLONG elapsed insert long break
 */
OPTIM_DEBUG static void printTimeDelay() {
    auto cur_time = sys_time::getTime();
    auto elapsed_time = cur_time - last_msg_time;

    if (elapsed_time > LOG_LONG_DELAY) {
        printf(LOG_LONG_DELAY_STR);
    }
    else if (elapsed_time > LOG_SHORT_DELAY) {
        printf(LOG_SHORT_DELAY_STR);
    }

    last_msg_time = cur_time;
}

OPTIM_DEBUG static bool print_lock() {
    bool status = log_mutex.try_lock();
    if(!status) {
        // didn't get mutex lock
        log_skipped = true;
    }
    else {
        if(log_skipped) {
            printf("msg skipped" NEWLINE);
            log_skipped = false;
        }
    }
    return status;
}

OPTIM_DEBUG static void print_unlock() {
    log_mutex.unlock();
}

OPTIM_DEBUG static bool isLevelEnabled(Level level, Scope scope) {
    return scope_dbg_levels[scope] >= level;
}

OPTIM_DEBUG static void printMsgHeader(Level level, Scope scope) {
    uint32_t ms_time = sys_time::getTime();
    printf("%lu.%03lu %s/%s: ", ms_time / 1000, ms_time % 1000,
           scope_names[scope], level_names[level]);
}

OPTIM_DEBUG void debug_log(const char *fmt, ...) {
    if (mute_log && sys_time::getTime() - mute_time < mute_timeout) return;
    if (!print_lock()) return;
#if LOG_PRINT_TIME_DELAY != 0
    printTimeDelay();
#endif
    va_list arg_list;
    va_start(arg_list, fmt);
    vprintf(fmt, arg_list);
    va_end(arg_list);
    print_unlock();
}

OPTIM_DEBUG void debug_msg(Level level, Scope scope, const char *fmt, ...) {
    if (mute_log && sys_time::getTime() - mute_time < mute_timeout) return;
    if (!isLevelEnabled(level, scope)) return;
    if (!print_lock()) return;
#if LOG_PRINT_TIME_DELAY != 0
    printTimeDelay();
#endif
    va_list arg_list;
    va_start(arg_list, fmt);
    printMsgHeader(level, scope);
    vprintf(fmt, arg_list);
    printf(NEWLINE);
    va_end(arg_list);
    print_unlock();
}

OPTIM_DEBUG void hex_dump(Level level, Scope scope, const void *bin_data, size_t count) {
    if (mute_log && sys_time::getTime() - mute_time < mute_timeout) return;
    if (!isLevelEnabled(level, scope) || count == 0) return;
    if (!print_lock()) return;
#if LOG_PRINT_TIME_DELAY != 0
    printTimeDelay();
#endif
    printMsgHeader(level, scope);
    uint8_t *byte_ptr = (uint8_t *) bin_data;
    size_t n = 0;
    do {
        printf("%02X ", *byte_ptr);
        byte_ptr++;
        n++;
        if (n % 16 == 0 && n < count) {
            printf(NEWLINE);
            printMsgHeader(level, scope);
        }
    } while (n < count);
    printf(NEWLINE);
    print_unlock();
}

OPTIM_DEBUG void mute(uint32_t timeout) {
    if(timeout == 0) {
        mute_log = false;
        return;
    }
    mute_timeout = timeout;
    mute_time = sys_time::getTime();
    mute_log = true;
}
