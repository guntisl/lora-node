
#ifndef sys_time_h
#define sys_time_h

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#define SEC_TO_ms(s)                        (s * 1000u)
#define MIN_TO_ms(min)                      (SEC_TO_ms(min * 60u))

#ifdef __cplusplus
#include <misc/linked_list.h>
namespace sys_time {
extern "C" {
#endif

typedef uint32_t SysTime;

#ifdef __cplusplus

class LPSW_Timer {
public:
    SysTime time_left;
    bool active;
    size_t param;
    void (*callback)(size_t param);

    LPSW_Timer() : time_left(0), active(false), param(0),
                   callback(nullptr) {}

    bool operator<(LPSW_Timer& other) {
        return time_left < other.time_left;
    }
};

typedef SinglyLinkedList<LPSW_Timer>::Container TimerElement;

#endif

void startSysTimer();
void delay_us(size_t delay);
void delay(size_t time);
SysTime getTime();
size_t getElapsed(SysTime start_time);

typedef void *TimerHandle;
typedef void (*TimerCallback)(size_t param);

TimerHandle createTimer();
void destroyTimer(TimerHandle handle);

void startTimer(TimerHandle handle, uint32_t time, TimerCallback cb, size_t param);
void stopTimer(TimerHandle timer);
bool isRunning(TimerHandle timer);

void timerService();

#ifdef __cplusplus
}
}
#endif

#endif

