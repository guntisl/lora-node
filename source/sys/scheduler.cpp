
#include <new>
#include <limits>
#include <cstring>
#include "stm32wbxx.h"
#include "scheduler.h"
#include "misc/linked_list.h"
#include "sys_time.h"
#include "syscalls.h"
#include "led.h"
#include "sleep.h"

#define SCHED_OPTIM                     OPTIM_SPEED

#define PRINT_SCHEDULER_LOG             0

#define MAX_PRIORITY                    2
#define PRIORITY_COUNT                  (MAX_PRIORITY + 1)

#define IDLE_STACK_SIZE_WORDS           22
#define IDLE_PRIOR                      -1

#define TIME_SLICE_ms                   30

#define STACK_WATERMARK                 0xABC00CBA
#define STACK_MIN_LEVEL                 16

#define SERVICE_CALL(code)              __asm("svc %0" :: "i" (code));

namespace sys {
namespace sch {

enum ThreadState : uint8_t {
    ACTIVE = 0,
    SLEEP,
    SUSPEND
};

class ThreadControl {
public:
    ThreadControl() : stack_ptr(nullptr), priority(0), state(SUSPEND),
                      ps_state(sys::PS_MODE_STOP2), stack_start(nullptr),
                      cpu_share(0), signal(0) {}

    uint32_t *stack_ptr;
    int8_t priority;
    ThreadState state;
    sys::PowerSaveMode ps_state;
    uint32_t *stack_start;
    size_t stack_size_words;
    int32_t cpu_share;
    sys_time::TimerElement sleep_timer;
    uint32_t signal;
#if THREAD_NAME_LEN > 0
    char name[THREAD_NAME_LEN];
#endif
};

typedef SinglyLinkedList<ThreadControl>::Container TCContainer;

extern "C" {
ThreadControl *cur_thread;
ThreadControl *next_thread;
}
ThreadControl *idle_thread;
bool switch_context;
SinglyLinkedList<ThreadControl> thread_pri_list[PRIORITY_COUNT];
sys_time::TimerElement os_timer;
sys_time::SysTime last_schedule;
STACK_ALIGN uint32_t idle_thread_stack[IDLE_STACK_SIZE_WORDS];

NORET NAKED static void idleThread();
void threadReturn();

static void timeSliceTick(size_t ptr);
TCContainer* makeTCB(void *stack, size_t stack_size, uint8_t priority);
void initThreadStack(TCContainer* tcb, void (*entry)());
void handlePowerSaving(sys::PowerSaveMode ps_mode);
void createIdleThread();

template <typename AccessFunc>
void accessStackWatermarks(ThreadControl* thread, AccessFunc access);

extern "C" {
NAKED void dbgPrintCoreRegs();
void printfReg(uint32_t id, uint32_t reg);
void printfNewline();
void printfHeader();
NAKED void dbgPrintCoreRegs();
}


__attribute__((noreturn)) void
startScheduler(const char *name, void *stack, size_t stack_size,
    uint8_t priority, void (*entry)()) {
    {
        MaskIrqForScope mask_irq(IRQ_PRI__PENDSV);

        createIdleThread();

        auto tcb = (ThreadControl *) createNewThread(name, stack,
                stack_size / 4, priority, entry);

        // set as current tcb
        cur_thread = tcb;
        next_thread = cur_thread;
    }
    SERVICE_CALL(0);
    // never here
    FATAL_ERROR("scheduler startup failed");
}

extern "C" USED SCHED_OPTIM void startupTasks() {
    // start scheduler timer
    last_schedule = sys_time::getTime();
    sys_time::startTimer(&os_timer, TIME_SLICE_ms, timeSliceTick, 0);
}

extern "C" NAKED void SVC_Handler() {
    // start scheduler
    asm("push { lr }");
    asm("bl startupTasks");
    asm("pop { lr }");

    // pop registers from stack
    // read thread stack pointer
    asm("ldr r1, cur_thread_addr_c2");
    asm("ldr r1, [r1]");
    asm("ldr r0, [r1]");

    // pop non auto registers from stack
    asm("ldmia r0!, {r4 - r11, lr}");

    // set new process stack
    asm("msr psp, r0");

    // set nPRIV=0, SPSEL=1, FPCA=0
    asm("ldr r0, =2");
    asm("msr control, r0");
    asm("isb");

    // reset back initial stack so not to waste space
    asm("ldr r1, VTOR_c1");
    asm("ldr r1, [r1]");
    asm("ldr r0, [r1]");
    asm("msr msp, r0");
    asm("isb");

    // return to new thread
    asm("bx lr");

    asm(".align 4");
    asm("cur_thread_addr_c2: .word cur_thread");
    asm("VTOR_c1: .word 0xE000ED08");
}

extern "C" NAKED void PendSV_Handler() {
    asm("push { lr }");
    // process all pending service requests made by requestService()
    asm("bl processServices");
    // get next thread
    asm("bl getThreadToRun");
    asm("pop { lr }");

    asm("cmp r0, #0");
    asm("beq skipContextSwitch");
    // context switch
    asm("b contextSwitch");

    asm("skipContextSwitch:");
    asm("bx lr");
}

extern "C" USED SCHED_OPTIM uint32_t getThreadToRun() {
    // check if need to make context switch
    if(!switch_context && os_timer.data.active) {
        // skip context switch
        return 0;
    }
    switch_context = false;

#if PRINT_SCHEDULER_LOG
    LOG("switch context:" NEWLINE);
    LOG("-------------------------" NEWLINE);
#endif

#if STACK_CHECK
    if(cur_thread != idle_thread) {
        size_t watermark_ok = 0;
        accessStackWatermarks(cur_thread, [&](size_t idx, uint32_t &word) {
            if (word == STACK_WATERMARK) {
                watermark_ok = idx;
                return true;
            }
            else {
                return false;
            }
        });
#if THREAD_NAME_LEN > 0
        ASSERT(watermark_ok >= STACK_MIN_LEVEL, "low free stack space %d, "
                "thread %s", watermark_ok, cur_thread->name);
#else
        ASSERT(watermark_ok >= STACK_MIN_LEVEL, "low free stack space %d, "
                "thread %08X", watermark_ok, cur_thread);
#endif
    }
#endif

    int32_t time_used = sys_time::getElapsed(last_schedule);
    last_schedule = sys_time::getTime();

    // set deepest power saving mode, but if any thread has lower
    // state use that instead
    sys::PowerSaveMode used_ps_mode = sys::PowerSaveMode::PS_MODE_STOP2;

    ThreadControl *prospect = nullptr;
    // select next thread to run
    for(int32_t i = 0; i < PRIORITY_COUNT; i++) {
        auto priority = MAX_PRIORITY - i;
        size_t ready_count = 0;
        int32_t lowest_share = std::numeric_limits<int32_t>::max();
        for(auto& it : thread_pri_list[priority]) {
            if(priority == cur_thread->priority) {
                // update shares
                int32_t thread_usage;
                if(&it.data == cur_thread) {
                    thread_usage = time_used * 100;
                }
                else {
                    thread_usage = 0;
                }
                it.data.cpu_share += (thread_usage - it.data.cpu_share) / 16;
#if PRINT_SCHEDULER_LOG
                LOG("share %08X %d (%08X)" NEWLINE, &it.data, it.data.cpu_share,
                    it.data.stack_ptr);
#endif
            }
            if(it.data.state == ACTIVE) {
                ready_count++;
                if(it.data.cpu_share < lowest_share) {
                    lowest_share = it.data.cpu_share;
                    prospect = &it.data;
                }
            }
            else {
                if(it.data.ps_state == sys::PowerSaveMode::PS_MODE_SLEEP) {
                    // downgrade ps mode for whole system
                    used_ps_mode = sys::PowerSaveMode::PS_MODE_SLEEP;
                }
            }
        }
        if(prospect) {
#if PRINT_SCHEDULER_LOG
            LOG("using %08X, ready %d" NEWLINE, prospect, ready_count);
#endif
            // found next thread
            next_thread = prospect;
            if(ready_count > 1) {
                // need time share - start scheduler timer
                sys_time::startTimer(&os_timer, TIME_SLICE_ms, timeSliceTick, 0);
            }
#if PRINT_SCHEDULER_LOG
            LOG("switch cur: %08X, new %08X" NEWLINE, cur_thread->stack_ptr,
                next_thread->stack_ptr);
#endif
            handlePowerSaving(used_ps_mode);

            return next_thread != cur_thread ? 1 : 0;
        }
    }

    // no active threads found - schedule idle thread
    next_thread = idle_thread;
    return next_thread != cur_thread ? 1 : 0;
}

SCHED_OPTIM void handlePowerSaving(sys::PowerSaveMode ps_mode) {
    if(next_thread == idle_thread && cur_thread != idle_thread) {
        // can enter power saving mode
        prepForPowSave(ps_mode);
        enPowSaveOnExcRet();
    }
    else if(cur_thread == idle_thread && next_thread != idle_thread) {
        // exit power saving
        disPowSaveOnExcRet();
        exitFromPowSave();
    }
}

extern "C" NAKED USED void contextSwitch() {
    // exception has pushed regs: r0, r1, r2, r3, r12, lr(r14), return, xPSR
    // need to save r4, r5, r6, r7, r8, r9, r10, r11

#if PRINT_SCHEDULER_LOG
    // === DEBUG CORE REG PRINTOUT ===
    asm("push { lr }");
    asm("bl dbgPrintCoreRegs");
    asm("pop { lr }");
    // ===============================
#endif

    asm("mrs r0, psp");
    asm("stmdb r0!, {r4 - r11, lr}");

    // read current thread control block
    asm("ldr r2, cur_thread_addr_c1");
    asm("ldr r1, [r2]");
    // save stack pointer in 'cur_thread->stack_ptr'
    asm("str r0, [r1]");

    // read next thread control block
    asm("ldr r2, next_thread_addr_c1");
    asm("ldr r1, [r2]");
    // read new stack pointer
    asm("ldr r0, [r1]");

    // pop registers from stack
    asm("ldmia r0!, {r4 - r11, lr}");
    asm("msr psp, r0");

    // set cur_thread = next_thread
    // r1 = &next_thread
    asm("ldr r2, cur_thread_addr_c1");
    asm("str r1, [r2]");

#if PRINT_SCHEDULER_LOG
    // === DEBUG CORE REG PRINTOUT ===
    asm("push { lr }");
    asm("bl dbgPrintCoreRegs");
    asm("pop { lr }");
    // ===============================
#endif

    // return from exception
    asm("bx lr");

    asm(".align 4");
    asm("cur_thread_addr_c1: .word cur_thread");
    asm("next_thread_addr_c1: .word next_thread");
}

constexpr uint32_t initial_stack[] {
    0x1004,     // register initial values - last two is register ID in decimal
    0x1005,
    0x1006,
    0x1007,
    0x1008,
    0x1009,
    0x1010,
    0x1011,
    0xFFFFFFFD, // lr return from exception
    0x1000,
    0x1001,
    0x1002,
    0x1003,
    0x1012,
    0xAA0000AA, // debug symbol that no return possible
    0xBB0000BB, // placeholder for thread function
    0x01000000  // xPSR
};

SCHED_OPTIM void initThreadStack(TCContainer* tcb, void (*entry)()) {
    // set stack initial state
    ThreadControl* thread = &tcb->data;

    thread->stack_ptr = thread->stack_start + thread->stack_size_words;
    thread->stack_ptr -= sizeof(initial_stack) / sizeof(uint32_t);
    memcpy(thread->stack_ptr, initial_stack, sizeof(initial_stack));
    thread->stack_ptr[15] = ((uint32_t)entry) & 0xFFFFFFFE;

    // set stack watermark detection symbols
    accessStackWatermarks(thread, [](size_t idx, uint32_t& word) {
        word = STACK_WATERMARK;
        return true;
    });
}

SCHED_OPTIM void createIdleThread() {
    MaskIrqForScope mask_irq(1);
    auto tcb = makeTCB(idle_thread_stack, sizeof(idle_thread_stack) / 4,
                       IDLE_PRIOR);
    initThreadStack(tcb, idleThread);
    tcb->data.state = ACTIVE;
    idle_thread = &(tcb->data);
#if THREAD_NAME_LEN > 0
    strncpy(tcb->data.name, "idle", sizeof(tcb->data.name));
#endif
}

SCHED_OPTIM ThreadHandle createNewThread(const char *name, void *stack,
    size_t stack_size, uint8_t priority, void (*entry)()) {
    MaskIrqForScope mask_irq(1);

    auto tcb = makeTCB(stack, stack_size, priority);
    initThreadStack(tcb, entry);
    thread_pri_list[tcb->data.priority].pushFront(*tcb);
    tcb->data.state = ACTIVE;
#if THREAD_NAME_LEN > 0
    strncpy(tcb->data.name, name, sizeof(tcb->data.name));
#endif
    return &tcb->data;
}

SCHED_OPTIM TCContainer* makeTCB(void *stack, size_t stack_size,
        uint8_t priority) {
    auto tc_container = new (std::nothrow) TCContainer;
    ASSERT(tc_container, "out of heap");

    STRONG_ASSERT(((uint32_t)stack & 7) == 0, "stack needs 8 B allignment");
    STRONG_ASSERT(stack_size % 2 == 0, "stack len must be multiple of 8 B");

    tc_container->data.stack_start = (uint32_t *) stack;
    tc_container->data.stack_size_words = stack_size;
    tc_container->data.priority = priority;
    tc_container->data.stack_ptr = tc_container->data.stack_start;

    return tc_container;
}

SCHED_OPTIM void threadClearSignal() {
    cur_thread->signal = 0;
}

SCHED_OPTIM void threadSetSignal(ThreadHandle thread, uint32_t sig) {
    auto *tcb = (ThreadControl*)thread;
    tcb->signal |= sig;
}

SCHED_OPTIM uint32_t threadGetSignal(ThreadSignalIds sig) {
    return cur_thread->signal & sig;
}

SCHED_OPTIM void threadSleep(uint32_t time, sys::PowerSaveMode ps_state) {
    cur_thread->state = SLEEP;
    cur_thread->ps_state = ps_state;
    sys_time::startTimer(&cur_thread->sleep_timer, time,
                         (sys_time::TimerCallback) wakeThread,
                         (size_t) cur_thread);
    threadYield();
}

SCHED_OPTIM void threadSuspend(sys::PowerSaveMode ps_state) {
    cur_thread->state = SUSPEND;
    cur_thread->ps_state = ps_state;
    threadYield();
}

SCHED_OPTIM void threadYield() {
    switch_context = true;
    SCB->ICSR |= SCB_ICSR_PENDSVSET_Msk;
    __DSB();
}

SCHED_OPTIM void wakeThread(ThreadHandle thread) {
    auto *tcb = (ThreadControl*)thread;
    tcb->state = ACTIVE;
    sys_time::stopTimer(&tcb->sleep_timer);
    threadYield();
}

SCHED_OPTIM ThreadHandle myHandle() {
    return cur_thread;
}

SCHED_OPTIM uint8_t myPriority() {
    return cur_thread->priority;
}

/**
 * @brief This will practically never run, because of sleep mode entry
 * @details If it returns it could be due to debug activity
 */
NORET NAKED static void idleThread() {
    // WARNING - do not add code here, that uses stack, unless you adjust
    // stack size. Stack is just enough to start this thread, but will
    // likely overflow if something is pushed to it
    asm("idle_thread:");
    asm("nop");
    asm("b idle_thread");
}

// os timer event
SCHED_OPTIM static void timeSliceTick(size_t ptr) {
#if PRINT_SCHEDULER_LOG
    LOG("time slice" NEWLINE);
#endif
}



// === Functions for debug
extern "C" {

SCHED_OPTIM void printfReg(uint32_t id, uint32_t reg) {
    LOG("r%02d %08X ", id, reg);
}
SCHED_OPTIM void printfNewline() {
    LOG(NEWLINE);
}

SCHED_OPTIM void printfHeader() {
    LOG("Regs" NEWLINE);
}

#define PRINT_REG(id)               asm("mov r1, r"#id);                \
                                    asm("mov.w r0, #"#id);              \
                                    asm("bl printfReg");

#define PRINT_STACKED_REG(off,id)   asm("mrs r3, psp");                 \
                                    asm("ldr r1, [r3, #" #off "*4]");   \
                                    asm("mov.w r0, #"#id);              \
                                    asm("bl printfReg");

#define PRINT_SP                    asm("mrs r1, psp");                 \
                                    asm("mov.w r0, #13");               \
                                    asm("bl printfReg");

/**
 * @brief Print all register state for current thread
 */
NAKED void dbgPrintCoreRegs() {
    asm("push { r0, r1, r3, lr }");
    asm("bl printfHeader");
    PRINT_STACKED_REG(0, 0);
    PRINT_STACKED_REG(1, 1);
    PRINT_STACKED_REG(2, 2);
    PRINT_STACKED_REG(3, 3);
    asm("bl printfNewline");
    PRINT_REG(4);
    PRINT_REG(5);
    PRINT_REG(6);
    PRINT_REG(7);
    asm("bl printfNewline");
    PRINT_REG(8);
    PRINT_REG(9);
    PRINT_REG(10);
    PRINT_REG(11);
    asm("bl printfNewline");
    PRINT_STACKED_REG(4, 12);
    PRINT_SP;
    PRINT_STACKED_REG(5, 14);
    PRINT_STACKED_REG(6, 15);
    asm("bl printfNewline");
    asm("pop { r0, r1, r3, lr }");
    asm("bx lr");
}

}

template <typename AccessFunc>
SCHED_OPTIM void accessStackWatermarks(ThreadControl* thread,
        AccessFunc access) {
    ASSERT(thread->stack_ptr > thread->stack_start, "");
    size_t mark_idx = 0;
    while (thread->stack_start + mark_idx < thread->stack_ptr) {
        if(!access(mark_idx, thread->stack_start[mark_idx])) {
            return;
        }
        if(mark_idx == 0) {
            mark_idx++;
        }
        else {
            mark_idx *= 2;
        }
    }
}

}
}
