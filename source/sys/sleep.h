 
#ifndef sleep_h
#define sleep_h

#include <cstdint>
#include <cstdlib>

#ifndef ENTER_PS_MODE
#define ENTER_PS_MODE           0
#endif

namespace sys {

enum PowerSaveMode : size_t {
    PS_MODE_SLEEP,
    PS_MODE_STOP2
};

void prepForPowSave(PowerSaveMode mode);
void enPowSaveOnExcRet();
void disPowSaveOnExcRet();
void exitFromPowSave();

}

#endif
