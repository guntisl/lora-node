#include <stm32wbxx.h>
#include "sleep.h"
#include "debug_log.h"

#define LOG_PS_STATE            0
#define ALLOW_STOP_MODE         1

namespace sys {

PowerSaveMode ps_mode;

void prepForPowSave(PowerSaveMode mode) {
    static bool init_done;
#if LOG_PS_STATE
    LOG("prepare sys for low power (%d)" NEWLINE, mode);
#endif
    ps_mode = mode;

    if(!init_done) {
     //   HAL_SuspendTick();
  //      LL_C2_PWR_SetPowerMode(LL_PWR_MODE_STOP2);
    //    LL_PWR_SetPowerMode(LL_PWR_MODE_STOP2);
        /*
        // set STOP2 mode for CPU1 and CPU2
        MODIFY_REG(PWR->CR1, 0x7, 0x2);
        MODIFY_REG(PWR->C2CR1, 0x7, 0x2);
        */
        init_done = true;
    }

#if ALLOW_STOP_MODE
    if(ps_mode == PS_MODE_STOP2) {
        //SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;
      //  HAL_PWREx_EnterSTOP2Mode(PWR_STOPENTRY_WFE);
    }
    else {
        //SCB->SCR &= ~SCB_SCR_SLEEPDEEP_Msk;
    }
#else
   // SCB->SCR &= ~SCB_SCR_SLEEPDEEP_Msk;
#endif

    // add calls to periphery drivers if needed
}

void enPowSaveOnExcRet() {
#if LOG_PS_STATE
    LOG("enter low pow" NEWLINE);
#endif

    // enable sleep on return
   // SCB->SCR |= SCB_SCR_SLEEPONEXIT_Msk;
//	HAL_PWREx_EnterSTOP2Mode(PWR_STOPENTRY_WFE);
}

void disPowSaveOnExcRet() {
#if LOG_PS_STATE
    LOG("exit low pow" NEWLINE);
#endif

//	HAL_PWREx_EnterSTOP2Mode(PWR_STOPENTRY_WFE);
    // disable sleep on return
   // SCB->SCR &= ~SCB_SCR_SLEEPONEXIT_Msk;
 //   HAL_ResumeTick();
}

void exitFromPowSave() {
#if LOG_PS_STATE
//    LOG("config sys for normal mode" NEWLINE);
#endif

    //    HAL_RCC_DeInit();
    // add calls to periphery drivers if needed
//	HAL_PWREx_EnterSTOP2Mode(PWR_STOPENTRY_WFE);
}

}
