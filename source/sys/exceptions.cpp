
#include <stm32wbxx.h>
#include <cstring>
#include "crypto/misc_crypto.h"
#include "sys/misc/flash_fifo.h"
#include "sys/misc/fifo_buffer.h"
#include "exceptions.h"
#include "sys_time.h"

#if EN_EXC_LOG

class CoreRegs {
public:
    uint32_t r0;
    uint32_t r1;
    uint32_t r2;
    uint32_t r3;
    uint32_t r4;
    uint32_t r5;
    uint32_t r6;
    uint32_t r7;
    uint32_t r8;
    uint32_t r9;
    uint32_t r10;
    uint32_t r11;
    uint32_t r12;
    uint32_t sp;
    uint32_t lr;
    uint32_t pc;
};

CoreRegs core_regs;

class ExceptionReport {
public:
    enum Type : uint8_t {
        INVALID = 0,
        ASSERT,
        FATAL_ERROR,
        ARM_HARD_FAULT
    };

    static void logTime(sys_time::SysTime time) {
        LOG(LF("%d.%03d"), time / 1000, time % 1000);
    }

    class AssertReport {
    public:
        sys_time::SysTime timestamp;
        const char *function;
        const char *expression;

        void printReport() const {
            LOG(LF("type: ASSERT"));
            LOG("time: ");
            logTime(timestamp);
            LOG(LF("expr: '%s'"), expression);
            LOG(LF("func: '%s'"), function);
        }
    };
    class FatalErrorReport {
    public:
        sys_time::SysTime timestamp;
        const char *function;
        const char *message;

        void printReport() const {
            LOG(LF("type: FATAL ERROR"));
            LOG("time: ");
            logTime(timestamp);
            LOG(LF("expr: '%s'"), message);
            LOG(LF("func: '%s'"), function);
        }
    };
    class ArmFaultReport {
    public:
        enum FaultType : uint8_t {
            NMI_FAULT,
            HARD_FAULT,
            BUS_FAULT,
            USAGE_FAULT,
            MEM_FAULT
        };
        sys_time::SysTime timestamp;
        FaultType type;
        uint32_t UFSR;
        uint32_t BFSR;
        uint32_t BFAR;
        uint32_t MMFSR;
        uint32_t MMFAR;
        uint32_t HFSR;
        CoreRegs core_regs;

        void printReport() const {
            LOG(LF("type: ARM FAULT"));
            LOG("time: ");
            logTime(timestamp);
            LOG(LF("type %d"), type);
            LOG(LF("UFSR  %08X BFSR  %08X BFAR %08X"), UFSR, BFSR, BFAR);
            LOG(LF("MMFSR %08X MMFAR %08X HFSR %08X"), MMFSR, MMFAR, HFSR);

            LOG(LF("%08X %08X %08X %08X"), core_regs.r0, core_regs.r1,
                core_regs.r2, core_regs.r3);
            LOG(LF("%08X %08X %08X %08X"), core_regs.r4, core_regs.r5,
                core_regs.r6, core_regs.r7);
            LOG(LF("%08X %08X %08X %08X"), core_regs.r8, core_regs.r9,
                core_regs.r10, core_regs.r11);
            LOG(LF("%08X %08X %08X %08X"), core_regs.r12, core_regs.sp,
                core_regs.lr, core_regs.pc);
        }
    };

    union Data {
        uint8_t byte[0];
        AssertReport assert;
        FatalErrorReport fatal_error;
        ArmFaultReport arm_fault;
    };

    // type for flash log storage
    static constexpr uint8_t rec_type = 1;
    Type type;
    uint16_t crc;
    Data data;
};

__attribute__((used, section(".noinit"))) ExceptionReport last_report;
extern uint8_t _exc_log_start;

class ExceptionMonitor {
    FlashFifo<ExceptionReport, EXC_LOG_SIZE, true> except_log;
public:
    ExceptionMonitor() : except_log(&_exc_log_start) {
        // check exception report for possible exception info
        auto crc = crc::crc16_CCITT(&last_report.data, sizeof(last_report.data));
        crc = crc::crc16_CCITT(&last_report.type, sizeof(last_report.type), crc);
        if(crc == last_report.crc) {
            // valid report found - store in log
            except_log.insert(last_report);
        }
        last_report.crc = 0;
        last_report.type = ExceptionReport::INVALID;

        if(except_log.countStored()) {
            // read exception log - print out information
            LOG(LF("Exception log printout:"));
            while (except_log.countStored()) {
                if(except_log.isNextValid()) {
                    auto next = except_log.accessNext();
                    LOG(LF(LINE_BREAK));
                    switch (next.type) {
                    case ExceptionReport::Type::ASSERT:
                        next.data.assert.printReport();
                        break;
                    case ExceptionReport::Type::FATAL_ERROR:
                        next.data.fatal_error.printReport();
                        break;
                    case ExceptionReport::Type::ARM_HARD_FAULT:
                        next.data.arm_fault.printReport();
                        break;
                    default:
                        LOG(LF("unknown error type"));
                        break;
                    }
                }
                except_log.remove();
            }
            LOG(LF(LINE_BREAK));
            LOG(LF("END"));
        }
    }
};

ExceptionMonitor monitor;

uint16_t calcReportCRC();
static void recoverSystem();
bool isDebugAttached();

template <typename SetReportFunc>
void setReport(SetReportFunc setReport) {
    memset(&last_report.data, 0x00, sizeof(last_report.data));

    setReport();

    last_report.crc = calcReportCRC();
}

__attribute__((noreturn)) void __assert_func(const char *file, int lineNr,
        const char *function, const char *expression) {

    setReport([&]() {
        last_report.type = ExceptionReport::ASSERT;
        last_report.data.assert.timestamp = sys_time::getTime();
        last_report.data.assert.function = function;
        last_report.data.assert.expression = expression;
        if(isDebugAttached()) last_report.data.assert.printReport();
    });

    recoverSystem();
}

__attribute__((noreturn)) void fatalErrorFunc(const char *function,
                                              const char *message) {

    setReport([&]() {
        last_report.type = ExceptionReport::FATAL_ERROR;
        last_report.data.fatal_error.timestamp = sys_time::getTime();
        last_report.data.fatal_error.function = function;
        last_report.data.fatal_error.message = message;
        if(isDebugAttached()) last_report.data.assert.printReport();
    });

    recoverSystem();
}

__attribute__((noreturn)) static void recoverSystem() {
    if(isDebugAttached()) {
        // debugger connected - wait forever
        // if debugger connected - don't save log on restart
        last_report.crc = 0;
        last_report.type = ExceptionReport::INVALID;
        while (1);
    }
    else {
        sys_time::delay(3000);
    }
    NVIC_SystemReset();
    while (1);
}

bool isDebugAttached() {
    return (CoreDebug->DHCSR & CoreDebug_DHCSR_C_DEBUGEN_Msk) != 0;
}

void setCoreRegs(ExceptionReport::ArmFaultReport &report) {
    report.core_regs = core_regs;
}



void NMI_HandlerProc() {
    setReport([&]() {
        last_report.type = ExceptionReport::ARM_HARD_FAULT;
        last_report.data.arm_fault.timestamp = sys_time::getTime();
        last_report.data.arm_fault.type =
            ExceptionReport::ArmFaultReport::NMI_FAULT;
        setCoreRegs(last_report.data.arm_fault);

        if(isDebugAttached()) last_report.data.arm_fault.printReport();
    });
    recoverSystem();
}

void HardFault_HandlerProc() {
    setReport([&]() {
        last_report.type = ExceptionReport::ARM_HARD_FAULT;
        last_report.data.arm_fault.timestamp = sys_time::getTime();
        last_report.data.arm_fault.type =
            ExceptionReport::ArmFaultReport::HARD_FAULT;
        last_report.data.arm_fault.HFSR = SCB->HFSR;
        setCoreRegs(last_report.data.arm_fault);

        if(isDebugAttached()) last_report.data.arm_fault.printReport();
    });
    recoverSystem();
}

void MemManage_HandlerProc() {
    uint32_t MMFSR = (SCB->CFSR & SCB_CFSR_MEMFAULTSR_Msk)
        >> SCB_CFSR_MEMFAULTSR_Pos;

    setReport([&]() {
        last_report.type = ExceptionReport::ARM_HARD_FAULT;
        last_report.data.arm_fault.timestamp = sys_time::getTime();
        last_report.data.arm_fault.type =
            ExceptionReport::ArmFaultReport::MEM_FAULT;
        last_report.data.arm_fault.MMFSR = MMFSR;
        last_report.data.arm_fault.MMFAR = SCB->MMFAR;
        setCoreRegs(last_report.data.arm_fault);

        if(isDebugAttached()) last_report.data.arm_fault.printReport();
    });
    recoverSystem();
}

void BusFault_HandlerProc() {
    uint32_t BFSR = (SCB->CFSR & SCB_CFSR_BUSFAULTSR_Msk)
        >> SCB_CFSR_BUSFAULTSR_Pos;

    setReport([&]() {
        last_report.type = ExceptionReport::ARM_HARD_FAULT;
        last_report.data.arm_fault.timestamp = sys_time::getTime();
        last_report.data.arm_fault.type =
            ExceptionReport::ArmFaultReport::BUS_FAULT;
        last_report.data.arm_fault.BFSR = BFSR;
        last_report.data.arm_fault.BFAR = SCB->BFAR;
        setCoreRegs(last_report.data.arm_fault);

        if(isDebugAttached()) last_report.data.arm_fault.printReport();
    });
    recoverSystem();
}

void UsageFault_HandlerProc() {
    uint32_t UFSR = (SCB->CFSR & SCB_CFSR_USGFAULTSR_Msk)
            >> SCB_CFSR_USGFAULTSR_Pos;

    setReport([&]() {
        last_report.type = ExceptionReport::ARM_HARD_FAULT;
        last_report.data.arm_fault.timestamp = sys_time::getTime();
        last_report.data.arm_fault.type =
            ExceptionReport::ArmFaultReport::USAGE_FAULT;
        last_report.data.arm_fault.UFSR = UFSR;
        setCoreRegs(last_report.data.arm_fault);

        if(isDebugAttached()) last_report.data.arm_fault.printReport();
    });
    recoverSystem();
}

uint16_t calcReportCRC() {
    uint16_t crc;
    crc = crc::crc16_CCITT(&last_report.data, sizeof(last_report.data));
    crc = crc::crc16_CCITT(&last_report.type, sizeof(last_report.type), crc);
    return crc;
}

extern "C" NAKED void saveCoreRegs() {
    // read global object core_regs address
    asm("ldr r0, core_regs_c1");
    asm("add r0, 16");
    asm("stm r0, { r4 - r11 }");

    asm("ldr r0, core_regs_c1");

    asm("ldr r2, [r1, 0]");     // r0
    asm("str r2, [r0, 0]");

    asm("ldr r2, [r1, 4]");     // r1
    asm("str r2, [r0, 4]");

    asm("ldr r2, [r1, 8]");     // r2
    asm("str r2, [r0, 8]");

    asm("ldr r2, [r1, 12]");    // r3
    asm("str r2, [r0, 12]");

    asm("ldr r2, [r1, 16]");    // r12
    asm("str r2, [r0, 48]");

    asm("str r1, [r0, 52]");    // sp

    asm("ldr r2, [r1, 20]");    // lr
    asm("str r2, [r0, 56]");

    asm("ldr r2, [r1, 24]");    // pc
    asm("str r2, [r0, 60]");

    asm("bx lr");

    asm(".align 4");
    asm("core_regs_c1: .word core_regs");
}

extern "C" NAKED void NMI_Handler() {
    asm("tst lr, #4");
    asm("ite eq");
    asm("mrseq r1, msp");
    asm("mrsne r1, psp");
    asm("bl %0" :: "i"(saveCoreRegs));
    asm("bl %0" :: "i"(NMI_HandlerProc));
}

extern "C" NAKED void HardFault_Handler() {
    asm("tst lr, #4");
    asm("ite eq");
    asm("mrseq r1, msp");
    asm("mrsne r1, psp");
    asm("bl %0" :: "i"(saveCoreRegs));
    asm("bl %0" :: "i"(HardFault_HandlerProc));
}

extern "C" NAKED void MemManage_Handler() {
    asm("tst lr, #4");
    asm("ite eq");
    asm("mrseq r1, msp");
    asm("mrsne r1, psp");
    asm("bl %0" :: "i"(saveCoreRegs));
    asm("bl %0" :: "i"(MemManage_HandlerProc));
}

extern "C" NAKED void BusFault_Handler() {
    asm("tst lr, #4");
    asm("ite eq");
    asm("mrseq r1, msp");
    asm("mrsne r1, psp");
    asm("bl %0" :: "i"(saveCoreRegs));
    asm("bl %0" :: "i"(BusFault_HandlerProc));
}

extern "C" NAKED void UsageFault_Handler() {
    asm("tst lr, #4");
    asm("ite eq");
    asm("mrseq r1, msp");
    asm("mrsne r1, psp");
    asm("bl %0" :: "i"(saveCoreRegs));
    asm("bl %0" :: "i"(UsageFault_HandlerProc));
}

#endif
