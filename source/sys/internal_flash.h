
#ifndef internal_flash_h
#define internal_flash_h

#include <stdint.h>
#include <stdlib.h>

namespace flash {

/**
 * @brief erase single flash page
 * @param address address inside page to erase
 */
void erasePage(uint32_t address);

/**
 * @brief program double word to flash
 * @param address double word aligned address
 * @param dword data to program
 */
void write(uint32_t address, uint8_t *data, size_t len);

/**
 * @brief eeprom emulation region size
 * @return bytes
 */
size_t eeprom_size();

/**
 * @brief eeprom emulation region start address
 * @return
 */
uint8_t* eeprom_start();

}

#endif
