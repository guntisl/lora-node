
#ifndef syscalls_h
#define syscalls_h

#include "sys/misc/macros.h"
#include "sys_config.h"

// service call list
enum ServiceCallId {
    SVC_LORA,
    SVC_LPTIM
};

#ifdef __cplusplus
extern "C" {
#endif

void requestService(uint32_t code);

void disableIRQ();
void enableIRQ();
void processServices();
NORET void systemReset();

#ifdef __cplusplus

class DisIrqForScope {
public:
    DisIrqForScope() {
        disableIRQ();
    }

    ~DisIrqForScope() {
        enableIRQ();
    }
};

class MaskIrqForScope {
    uint32_t previous;
public:
    explicit MaskIrqForScope(uint8_t mask_level) {
        previous = __get_BASEPRI();
        __set_BASEPRI_MAX(ENCODE_PRIORITY(mask_level) << (8U - __NVIC_PRIO_BITS));
    }

    ~MaskIrqForScope() {
        __set_BASEPRI(previous);
    }
};


}
#endif

#endif
