
#ifndef macros_h
#define macros_h


#define CONCAT_EXECUTE(a, b)            a # b
#define CONCAT(a, b)                    CONCAT_EXECUTE(a,b)

/**
 * @brief Return number of bytes to add to 'size' to get multiple of 'multiple'
 */
#define PAD_TO(size, multiple)         (multiple - (size % multiple))


#define OPTIM_SPEED                     __attribute__((optimize("O3")))
#define OPTIM_SIZE                      __attribute__((optimize("Os")))
#define USED                            __attribute__((used))
#define NAKED                           __attribute__((naked))
#define NORET                           __attribute__((noreturn))






#endif
