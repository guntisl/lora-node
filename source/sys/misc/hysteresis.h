
#ifndef hysteresis_h
#define hysteresis_h

#include "stdlib.h"
#include "stdint.h"

class Hysteresis {
    bool trig_low;
    bool trig_high;
    size_t counter_low;
    size_t counter_high;
    size_t hyst;
public:
    explicit Hysteresis(size_t hysteresis = 10) : trig_low(false),
        trig_high(false), counter_low(0), counter_high(0), hyst(hysteresis) {}

    enum Status{
        NO_TRIG,
        TRIG_HIGH,
        TRIG_LOW
    };

    template <typename T>
    Status testValue(const T &value, const T &high_level, const T &low_level) {
        if(value > high_level) {
            counter_high = 0;
            if(!trig_high) {
                trig_high = true;
                return TRIG_HIGH;
            }
        }
        else {
            if(counter_high == hyst) {
                trig_high = false;
            }
            else counter_high++;
        }
        if(value < low_level) {
            counter_low = 0;
            if(!trig_low) {
                trig_low = true;
                return TRIG_LOW;
            }
        }
        else {
            if(counter_low == hyst) {
                trig_low = false;
            }
            else counter_low++;
        }
        return NO_TRIG;
    }
};

#endif
