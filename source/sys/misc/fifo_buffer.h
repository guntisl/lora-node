
#ifndef fifo_buffer_h
#define fifo_buffer_h

template <typename T, size_t SIZE>
class FifoBuffer {
    size_t write_idx;
    size_t read_idx;
    bool full;
    T memory[SIZE];

    void incIndex(size_t &index, size_t inc = 1) {
        auto copy = index;
        copy += inc;
        if(copy >= SIZE) {
            copy -= SIZE;
        }
        index = copy;
    }

public:
    FifoBuffer() : write_idx(0), read_idx(0), full(false) {}

    bool insert(const T& item) {
        if(!full) {
            memory[write_idx] = item;
            incIndex(write_idx);
            if(write_idx == read_idx) full = true;
            return true;
        }
        return false;
    }

    const T& accessNext(size_t num_from_end = 0) {
        auto idx = read_idx;
        incIndex(idx, num_from_end);
        T& ref = memory[idx];
        return ref;
    }

    T readNext() {
        T copy = accessNext();
        remove();
        return copy;
    }

    void remove(size_t count) {
        ASSERT(size() >= count, "remove more than have %d", size());
        incIndex(read_idx, count);
        full = false;
    }

    size_t size() const {
        if(full) return SIZE;
        if(write_idx >= read_idx) {
            return write_idx - read_idx;
        }
        return SIZE - read_idx + write_idx;
    }
};












#endif
