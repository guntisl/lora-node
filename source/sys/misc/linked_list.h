
#ifndef linked_list_h
#define linked_list_h

#include "debug_log.h"


template <typename T>
class SinglyLinkedList {
public:
    class Container;
    class iterator;
private:

    Container* head;
    Container* tail;
    iterator end_it;
    size_t el_count;
public:
    class Container {
        friend SinglyLinkedList;
        friend iterator;

        Container* next;
    public:
        Container() : next(nullptr) {}
        explicit Container(const T& init) : next(nullptr), data(init) {}
        T data;
    };

    class iterator {
        Container* element;
    public:
        explicit iterator(Container* el) : element(el) {}
        void operator++() {
            ASSERT(element != nullptr, "iter advance past end");
            if(element != nullptr) {
                element = element->next;
            }
        }

        Container& operator*() {
            ASSERT(element != nullptr, "dereference list end");
            return *element;
        }

        Container* operator->() {
            return element;
        }

        explicit operator Container*() {
            return element;
        }

        bool operator==(const iterator& other) {
            return other.element == element;
        }

        bool operator!=(const iterator& other) {
            return !(*this == other);
        }
    };

    SinglyLinkedList() : head(nullptr), tail(nullptr), end_it(nullptr), el_count(0) {}

    iterator begin() {
        return iterator(head);
    }

    iterator end() {
        return end_it;
    }

    /**
     * @brief Push element in front of the list
     */
    void pushFront(Container& element) {
        if (head != nullptr) {
            // something in the list
            element.next = head;
        }
        else {
            tail = &element;
        }
        head = &element;

        el_count++;
    }

    void pushBack(Container& element) {
        if(el_count && tail != nullptr) {
            // have last element
            ASSERT(tail->next == nullptr, "bad tail");
            element.next = nullptr;
            tail->next = &element;
            tail = &element;
            el_count++;
        }
        else {
            // list is empty - use front push
            pushFront(element);
        }
    }

    /**
     * @brief Insert elements with sorting (smallest first)
     */
    void pushSorted(Container& element) {
        Container** cur = &head;
        while (*cur != nullptr) {
            if(element.data < (*cur)->data) {
                // insert before this item
                element.next = *cur;
                break;
            }
            cur = &(*cur)->next;
        }
        if(*cur == nullptr) {
            tail = &element;
        }
        *cur = &element;

        el_count++;
    }

    /**
     * @brief remove existing element from list
     */
    void remove(Container& element) {
        Container** cur = &head;
        Container* prev_el = nullptr;   // need to relink tail

        while(*cur != &element) {
            if(*cur == nullptr) {
                // item was not found in list
                return;
            }
            prev_el = *cur;
            cur = &(*cur)->next;
        }
        *cur = element.next;

        if(tail == &element) {
            tail = prev_el;
        }

        // unlink removed element
        element.next = nullptr;

        el_count--;
    }

    size_t size() {
        return el_count;
    }

    void printState() {
        Container** cur = &head;
        LOG("list: %s" NEWLINE, el_count == 0 ? "empty" : "");
        for(size_t i = 0; i < el_count; i++) {
            LOG("%08X %s%s" NEWLINE, *cur, *cur == head ? "<-head" : "",
                *cur == tail ? "<-tail" : "");
            cur = &(*cur)->next;
        }
    }
};




#endif
