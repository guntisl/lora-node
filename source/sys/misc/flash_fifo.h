
#ifndef flash_log_h
#define flash_log_h

#include <cstdlib>
#include <cstdint>
#include <sys/debug_log.h>
#include "sys/misc/macros.h"
#include "crypto/misc_crypto.h"
#include "sys/internal_flash.h"


template <typename T, size_t PAGE_COUNT, bool OTP>
class FlashFifo {
    size_t write_idx;
    size_t read_idx;
    size_t erase_idx;
    bool erased_mem_avail;
    uint8_t *mem_address;

    static constexpr uint32_t HEADER_ID = 0xAA11BB22;
    static constexpr uint8_t VALID_ITEM = 0xDD;

    union Header {
        uint32_t id;
        uint8_t bytes[MCU_PROG_WORD_SIZE];
    };
    static_assert(sizeof(Header) == MCU_PROG_WORD_SIZE, "bad header");

    static constexpr Header VALID_HEADER = {
        .id = HEADER_ID
    };

    struct ItemStruct {
        T data;
        uint16_t crc;
        uint8_t valid_item_flag;
    };

    struct WriteStruct {
        ItemStruct element;
        uint8_t padding[PAD_TO(sizeof(ItemStruct), MCU_PROG_WORD_SIZE)];
    }__attribute__((packed));

    static constexpr size_t HEAD_PAGE_CAPACITY =
        (MCU_PAGE_SIZE - sizeof(Header)) / sizeof(WriteStruct);
    static constexpr size_t PAGE_CAPACITY = MCU_PAGE_SIZE / sizeof(WriteStruct);
    static constexpr size_t CAPACITY = PAGE_COUNT == 1 ? HEAD_PAGE_CAPACITY :
                                       HEAD_PAGE_CAPACITY
                                       + (PAGE_COUNT - 1) * PAGE_CAPACITY;

    /**
     * @brief Get actual memory address from index
     */
    uint8_t *getAddress(size_t index) {
        uint8_t *address;
        if (index < HEAD_PAGE_CAPACITY) {
            address = mem_address + sizeof(Header)
                      + index * sizeof(WriteStruct);
        }
        else {
            address = mem_address + MCU_PAGE_SIZE
                      + index * sizeof(WriteStruct);
        }
        return address;
    }

    uint32_t getPageNum(size_t index) {
        if (index < HEAD_PAGE_CAPACITY) {
            return 0;
        }
        else {
            return (index - HEAD_PAGE_CAPACITY) / PAGE_CAPACITY;
        }
    }

    size_t getPageCapacity(size_t page_idx) {
        return page_idx < HEAD_PAGE_CAPACITY ? HEAD_PAGE_CAPACITY
                                             : PAGE_CAPACITY;
    }

    void incIndex(size_t &index, size_t count = 1) {
        auto copy = index;
        copy += count;
        if(copy >= CAPACITY) {
            copy -= CAPACITY;
        }
        index = copy;
    }

    bool isErased(size_t &index) {
        auto *data = (uint8_t *) getAddress(index);

        for(size_t i = 0; i < sizeof(WriteStruct); i++) {
            if(data[i] != 0xFF) {
                return false;
            }
        }
        return true;
    }

    void writeHeader() {
        flash::write((uint32_t) mem_address, (uint8_t *) &VALID_HEADER,
                     sizeof(VALID_HEADER));
    }

    void removeLast() {
        if (countStored() == 0) return;
        auto prev_page = getPageNum(read_idx);
        incIndex(read_idx);
        auto cur_page = getPageNum(read_idx);
        // if index past page - erase page
        if (!OTP && (cur_page != prev_page || read_idx == 0)) {
            // erase previous page
            auto er_page_addr = mem_address + prev_page * MCU_PAGE_SIZE;
            flash::erasePage((uint32_t)er_page_addr);
            if (er_page_addr == mem_address) {
                writeHeader();
            }
            erase_idx = read_idx;
            erased_mem_avail = true;
        }
        MSG_DUMP(SYS, "remove, wr %u, rd %u, er %u", write_idx, read_idx,
                 erase_idx);
    }

public:
    explicit FlashFifo(uint8_t *address) : write_idx(0), read_idx(0),
            erase_idx(0), erased_mem_avail(true), mem_address(address) {
        // check if valid log block
        MSG_WARN(SYS, "F-FIFO %08X; cap %d, pc %d", this, CAPACITY, PAGE_COUNT);

        auto *actual_header = (Header *) mem_address;
        if (actual_header->id != HEADER_ID) {
            MSG_WARN(SYS, "F-FIFO wrong header id %08x", actual_header->id);
            resetLog();
            return;
        }

        // get possible state of read/write/erase indices
        size_t index = 0;
        bool prev_erased = true;
        bool have_data = false;
        do {
            auto is_erased = isErased(index);

            if(!is_erased && prev_erased) {
                // start of data block
                have_data = true;
                if(read_idx == 0) {
                    read_idx = index;
                }
            }

            if(is_erased && !prev_erased) {
                // end of data block
                if(write_idx == 0) {
                    // keep first end as write index
                    write_idx = index;
                }
            }

            prev_erased = is_erased;
            incIndex(index);
        } while (index != 0 && (write_idx == 0 || read_idx == 0));

        MSG_INFO(SYS, "F-FIFO rd %d; wr %d; e %d", read_idx, write_idx,
                 erase_idx);

        if(write_idx == read_idx && have_data) {
            // bad state - same index value allowed only if empty
            MSG_ERROR(SYS, "F-FIFO bad state");
            resetLog();
            return;
        }

        if(have_data) {
            // verify that write has erased memory till end of page
            erase_idx = write_idx;
            size_t till_end;
            if (erase_idx < HEAD_PAGE_CAPACITY) {
                till_end = HEAD_PAGE_CAPACITY - erase_idx;
            }
            else {
                till_end = erase_idx - HEAD_PAGE_CAPACITY % PAGE_CAPACITY;
            }

            while (till_end-- > 0) {
                if (!isErased(erase_idx)) {
                    MSG_ERROR(SYS, "F-FIFO bad state, write idx has data ahead");
                    resetLog();
                    return;
                }
                incIndex(erase_idx);
            }
        }

        if(!OTP && PAGE_COUNT > 1) {
            size_t erase_idx_page = getPageNum(erase_idx);
            size_t read_idx_page = getPageNum(read_idx);

            while (erase_idx_page != read_idx_page) {
                MSG_INFO(SYS, "F-FIFO erase page %d", erase_idx_page);
                flash::erasePage((uint32_t)getAddress(erase_idx));
                incIndex(erase_idx, getPageCapacity(erase_idx));
                erase_idx_page = getPageNum(erase_idx);
            }
        }
        MSG_INFO(SYS, "F-FIFO init complete");
    }

    bool insert(const T& element) {
        if(countStored() + 1 < CAPACITY && erased_mem_avail) {
            WriteStruct new_item = {
                .element = {
                    .data = element,
                    .crc = crc::crc16_CCITT(&element, sizeof(element)),
                    .valid_item_flag = VALID_ITEM,
                }
            };

            flash::write((uint32_t)getAddress(write_idx), (uint8_t *) &new_item,
                         sizeof(new_item));

            incIndex(write_idx);
            if(write_idx == erase_idx) {
                erased_mem_avail = false;
            }
            MSG_DUMP(SYS, "insert, wr %u, rd %u, er %u", write_idx, read_idx,
                     erase_idx);
            return true;
        }
        MSG_DUMP(SYS, "insert full, wr %u, rd %u, er %u", write_idx, read_idx,
                 erase_idx);
        return false;
    }

    bool isNextValid() {
        auto item_offset = offsetof(WriteStruct, element);
        auto &item = *(const ItemStruct *) (getAddress(read_idx) + item_offset);

        auto crc = crc::crc16_CCITT(&item.data, sizeof(T));

        return item.valid_item_flag == VALID_ITEM && item.crc == crc;
    }

    const T& accessNext(size_t num_from_end = 0) {
        auto idx = read_idx;
        incIndex(idx, num_from_end);

        auto item_offset = offsetof(WriteStruct, element) +
                          offsetof(ItemStruct, data);

        const T& item = *(T*)(getAddress(idx) + item_offset);

        return item;
    }

    T readNext() {
        T copy = accessNext();
        remove();
        return copy;
    }

    void remove(size_t count = 1) {
        ASSERT(countStored() >= count, "remove more than have %d",
               countStored());
        while (count-- > 0) {
            removeLast();
        }
    }

    size_t countStored() const {
        if(write_idx >= read_idx) {
            return write_idx - read_idx;
        }
        return CAPACITY - read_idx + write_idx;
    }

    void resetLog() {
        for(size_t i = 0; i < PAGE_COUNT; i++) {
            flash::erasePage((uint32_t)(mem_address + i * MCU_PAGE_SIZE));
        }

        writeHeader();

        read_idx = 0;
        write_idx = 0;
        erase_idx = 0;
        erased_mem_avail = true;
        MSG_WARN(SYS, "F-FIFO reset");
    }

    static constexpr size_t getCapacity() {
        return CAPACITY;
    }
};

template <typename T, size_t PAGE_COUNT, bool OTP>
constexpr uint32_t FlashFifo<T, PAGE_COUNT, OTP>::HEADER_ID;

template <typename T, size_t PAGE_COUNT, bool OTP>
constexpr typename FlashFifo<T, PAGE_COUNT, OTP>::Header
FlashFifo<T, PAGE_COUNT, OTP>::VALID_HEADER;

#endif
