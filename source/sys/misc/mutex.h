
#ifndef mutex_h
#define mutex_h

#include <mutex>

class mutex {
protected:
    uint32_t mutex_lock;
public:
    typedef uint32_t* native_handle_type;

    mutex() noexcept : mutex_lock(0) {}
    ~mutex() = default;

    mutex(const mutex&) = delete;
    mutex& operator=(const mutex&) = delete;

    void lock() {
        while (!try_lock()) {
            // never call from ISR if lower level ISR or main code has lock
            // wait for lock forever
        }
    }

    bool try_lock() {
        uint32_t unlocked_state = 0;
        return __atomic_compare_exchange_n(&mutex_lock, &unlocked_state,
                                           1, false, __ATOMIC_RELAXED,
                                           __ATOMIC_RELAXED);
    }

    void unlock() {
        uint32_t unlocked_state = 0;
        __atomic_store (&mutex_lock, &unlocked_state, __ATOMIC_RELAXED);
    }

    native_handle_type native_handle() {
        return &mutex_lock;
    }
};

#endif
