#include <sensors/sensor_data.h>
#include <lora/lora.h>
#include <cstring>
#include <config/nv_config.h>
#include <config/dev_config.h>
#include <sys/sleep.h>
#include "app.h"
#include "led.h"

#define SCALE_DEVICE   1
#define WATCH_DEVICE   2
#define IN_HIVE_DEVICE 3
uint8_t device_type = SCALE_DEVICE;
extern uint8_t alarm;

void EnterStop2Mode(void);

namespace app {

#define APP_DATA_PORT               1




void EnterStop2Mode(void)
{


}


struct GreetingMsg {
    uint32_t uptime;

    // number of restarts
    // version...
};

STACK_ALIGN uint32_t app_stack[200];

Config::User user_conf;
sys_time::SysTime last_send;


void application() {
    SET_LEVEL(APP, LEV_DEBUG);
    SET_LEVEL(LORA, LEV_DEBUG);

    MSG_INFO(APP, "APP start");

    MSG_INFO(APP, "read user conf");
    bool valid;
    user_conf = nvconf::readBankSafe<app::Config::User>(NVC_BANK_APP, 0, valid);
    if(!valid) {
        // init from defaults
        MSG_WARN(APP, "reseting to def conf");
        user_conf = config.app.user;
        nvconf::writeBankSafe(NVC_BANK_APP, 0, user_conf);
    }

    // purge unsent records on restart
    while (sensor::data::report_log.countStored() > 0) {
        sensor::data::report_log.remove(1);
    }

    MSG_INFO(APP, "start sensor monitoring");
    sensor::data::startSensorMonitoring();
    lora::init();

    MSG_INFO(APP, "==Init lora done==");

    LL_C2_PWR_SetPowerMode(LL_PWR_MODE_SHUTDOWN);

    DBGMCU->CR = 0;
    while (1) {
      MSG_INFO(APP, "wait data ");

        // wait for network state to be joined
        if (lora::hasJoinedNetw()) {
            // network state is joined
              MSG_INFO(APP, "network joined ");
            if(lora::checkJoinEvent()) {
                sendGreeting();
            }
            else {
                // send report
                MSG_INFO(APP, "send report");
                auto report_count = sensor::data::waitNewReport(5000);
                if (report_count >= user_conf.wait_n_reports) {
                    sendReports();
                }
            }
        }
        // waiting data
        MSG_INFO(APP, "wait data ");
        sys::sch::threadSleep(1000, sys::PS_MODE_STOP2);
        LL_PWR_SetPowerMode(LL_PWR_MODE_STOP2);
	HAL_PWREx_EnterSTOP2Mode(PWR_STOPENTRY_WFE);
    }
}







void sendGreeting() {
    bool status;
    do {
        // send greeting to server
        MSG_INFO(APP, "send greeting msg");
        GreetingMsg greeting = {
            .uptime = sys_time::getTime()
        };
        status = lora::sendData(false, 1, (uint8_t *) &greeting,
                                sizeof(greeting));
    } while (!status);
}

void sendReports() {
    using namespace sensor;
    static uint8_t report_pack_buf[LORA_MAX_SEND_LEN];

    while (data::report_log.countStored() > 0) {
        MSG_INFO(APP, "send report, %d left", data::report_log.countStored() - 1);

        // get report
        auto &report = sensor::data::report_log.accessNext(0);

        size_t len;
        uint8_t *data;
        if (report.trigger == sensor::data::DIO_TRIG) {
//            len = sizeof(report.trig_data.dig_ios);
  //          data = (uint8_t *) &report.trig_data.dig_ios;
        }
        else {
            len = sizeof(report.trig_data.env_data);
           data = (uint8_t *) &report.trig_data.env_data;
        }

        report_pack_buf[0] = alarm;
 //       report_pack_buf[25] = led3_state;

        ASSERT(len + 1 <= sizeof(report_pack_buf), "overflow");
        memcpy(&report_pack_buf[1], data, len);

        if (lora::sendData(false, APP_DATA_PORT, report_pack_buf, len + 1)) {
            sensor::data::report_log.remove(1);
            MSG_INFO(APP, "ok");
        }
        else {
            MSG_INFO(APP, "failed");
        }
    }
}

}

