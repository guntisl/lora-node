
#ifndef led_h
#define led_h

#include "stm32wbxx_ll_gpio.h"
#include "sys_defines.h"
#include "sys_time.h"

inline void setLED1(bool state) {
    if (state) {
//        LL_GPIO_SetOutputPin(LED1_PORT, LED1_PIN);
    }
    else {
  //      LL_GPIO_ResetOutputPin(LED1_PORT, LED1_PIN);
    }
}


inline void setLED2(bool state) {
    if (state) {
    //    LL_GPIO_SetOutputPin(LED2_PORT, LED2_PIN);
    }
    else {
      //  LL_GPIO_ResetOutputPin(LED2_PORT, LED2_PIN);
    }
}

inline void blinkLED(uint32_t time) {
    setLED1(true);
    sys_time::delay(time);
    setLED1(false);
}

inline void blinkLED(uint32_t time, size_t count) {
    while (count-- > 0) {
        blinkLED(time);
        sys_time::delay(time);
    }
}


#endif
