
#include <cstring>
#include "lora/lora.h"
#include "sensors/sensor_data.h"
#include "sys_config.h"
#include "led.h"
#include "sys_time.h"
#include "debug_log.h"
#include "app.h"

#include "sys/sleep.h"
#include "scheduler.h"
#include "misc/linked_list.h"
#include "sys_time.h"
#include "syscalls.h"

int main() {

   // random::init();
    // set log levels for application
    SET_LEVEL(SENS, LEV_DEBUG);

    LL_C2_PWR_SetPowerMode(LL_PWR_MODE_SHUTDOWN);
    MSG_INFO(TST, "Delay 2s to unbrick MCU");
    sys_time::delay(2000);

//    LL_GPIO_SetOutputPin(LED3_PORT, LED3_PIN);     //clear STCP
    //sys_time::delay(2000);
    // --- start application ---
    sys::sch::startScheduler("tst-app", app::app_stack, sizeof(app::app_stack),
                             1, app::application);

   // sys::sch::threadSleep(10000, sys::PS_MODE_STOP2);
}
