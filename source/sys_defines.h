
#ifndef sys_defines_h
#define sys_defines_h

#define LED1_PORT                GPIOA
#define LED1_PIN                 LL_GPIO_PIN_4

#define LED2_PORT                GPIOA
#define LED2_PIN                 LL_GPIO_PIN_13

#define LED3_PORT                GPIOA
#define LED3_PIN                 LL_GPIO_PIN_14

#define REG_CLK_PORT             GPIOA
#define REG_CLK_PIN              LL_GPIO_PIN_11

#define REG_LATCH_PORT           GPIOA
#define REG_LATCH_PIN            LL_GPIO_PIN_12

#define REG_DATA_IN_PORT         GPIOB
#define REG_DATA_IN_PIN          LL_GPIO_PIN_2

#define PINA_Pin_PORT            GPIOA
#define PINA_Pin                 LL_GPIO_PIN_6
#define PINA_EXTI_LINE           LL_EXTI_LINE_6

#define PINB_Pin_PORT            GPIOA
#define PINB_Pin                 LL_GPIO_PIN_1
#define PINB_EXTI_LINE           LL_EXTI_LINE_1

#define DOUT_PORT                GPIOA
#define DOUT_PIN                 LL_GPIO_PIN_7

#define SCK_PORT                 GPIOA
#define SCK_PIN                  LL_GPIO_PIN_5

#define BUTTON_PORT              GPIOA
#define BUTTON_PIN               LL_GPIO_PIN_0
#define BUTTON_EXTI_LINE         LL_EXTI_LINE_0

#define HDC2010_IRQ_PORT         GPIOA
#define HDC2010_IRQ_PIN          LL_GPIO_PIN_9
#define HDC2010_EXTI_LINE        LL_EXTI_LINE_9

#define REED_SWITCH_IRQ_PORT     GPIOA
#define REED_SWITCH_IRQ_PIN      LL_GPIO_PIN_8
#define REED_SWITCH_EXTI_LINE    LL_EXTI_LINE_8

#define EXT_UART                 USART1

#define I2C_BUS                  I2C1
#define SCL_PIN                  LL_GPIO_PIN_8
#define SDA_PIN                  LL_GPIO_PIN_9
#define I2C_GPIO                 GPIOB

#define TERMINAL_ECHO            1


#endif
