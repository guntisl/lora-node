
#ifndef sys_config_h
#define sys_config_h

#include "stm32wbxx.h"
#include "stm32wbxx_ll_gpio.h"

#define CPU_FREQ                    48000000

#define IRQ_PRIORITY_GROUP      NVIC_PRIORITYGROUP_4
#define LOWEST_IRQ_PRIORITY     15
#define ENCODE_PRIORITY(pri)    NVIC_EncodePriority(IRQ_PRIORITY_GROUP, pri, 0)

// --- IRQ Priority table for system ---
#define IRQ_PRI__SYS_TICK           2
#define IRQ_PRI__WKUP               2
#define IRQ_PRI__LORA_GPIO          IRQ_LOW_PRIORITY   /* 3 */
#define IRQ_PRI__SENSORS            4
#define IRQ_PRI__DIGIO              4
#define IRQ_PRI__USART1             6
#define IRQ_PRI__SVCALL             14
#define IRQ_PRI__PENDSV             15

#ifdef __cplusplus
extern "C" {
#endif

void configPins();
void configClocks();
void configUART();
void configI2C();

/**
 * @brief Miscellaneous system configurations
 */
void configMisc();

#ifdef __cplusplus
}
#endif

#endif
