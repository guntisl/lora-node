
#include "stm32wbxx.h"
#include "stm32wbxx_ll_i2c.h"
#include "stm32wbxx_ll_gpio.h"
#include "i2c_bus.h"
#include "debug_log.h"
#include "sys_time.h"

#define I2C_TIMEOUT                     1024u

// TIMEOUT; OVR; ARLO; BERR; NACKF;
#define BUS_ERRORS                      (1 << 12) | (1 << 10) | (1 << 9)    \
                                        | (1 << 8) | (1 << 4)

namespace i2c_bus {

template<typename transfConfigFunc, typename isReadyFunc, typename procNextByteFunc>
void i2c_transfer(transfConfigFunc configTransf, isReadyFunc isReady, procNextByteFunc procByte) {

    auto start_time = sys_time::getTime();
    while (LL_I2C_IsActiveFlag_BUSY(I2C_BUS)) {
        STRONG_ASSERT(sys_time::getTime() - start_time < I2C_TIMEOUT, "BUSY");
    }

    ASSERT(I2C_BUS->ISR & BUS_ERRORS, "uncleared bus errors %08X", I2C_BUS->ISR);

    configTransf();

    do {
        start_time = sys_time::getTime();
        while (!isReady()) {
            ASSERT(I2C_BUS->ISR & BUS_ERRORS, "uncleared bus errors %08X",
                   I2C_BUS->ISR);
            STRONG_ASSERT(sys_time::getTime() - start_time < I2C_TIMEOUT, "");
        }
    }while (procByte());

    while (!LL_I2C_IsActiveFlag_STOP(I2C_BUS)) {
        start_time = sys_time::getTime();
        STRONG_ASSERT(sys_time::getTime() - start_time < I2C_TIMEOUT, "");
    }
    LL_I2C_ClearFlag_STOP(I2C_BUS);
    sys_time::delay_us(1);
}

void master_tx(uint8_t address, uint8_t *data, size_t len) {

    const auto isActive_TXIS = []() {
        return LL_I2C_IsActiveFlag_TXIS(I2C_BUS);
    };

    const auto sendByte = [&]() {
        LL_I2C_TransmitData8(I2C_BUS, *data++);
        return --len;
    };

    const auto confTransfer = [&]() {
        LL_I2C_HandleTransfer(I2C_BUS, address << 1, LL_I2C_ADDRSLAVE_7BIT, len,
                              LL_I2C_MODE_AUTOEND, LL_I2C_GENERATE_START_WRITE);
    };

    i2c_transfer(confTransfer, isActive_TXIS, sendByte);
}

void master_rx(uint8_t address, uint8_t* data, size_t len) {

    const auto isActive_RXNE = []() {
        return LL_I2C_IsActiveFlag_RXNE(I2C_BUS);
    };

    const auto receiveByte = [&]() {
        *data++ = LL_I2C_ReceiveData8(I2C_BUS);
        return --len;
    };

    const auto confTransfer = [&]() {
        LL_I2C_HandleTransfer(I2C_BUS, address << 1, LL_I2C_ADDRSLAVE_7BIT, len,
                              LL_I2C_MODE_AUTOEND, LL_I2C_GENERATE_START_READ);
    };

    i2c_transfer(confTransfer, isActive_RXNE, receiveByte);
}

}
