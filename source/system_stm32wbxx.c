
#include "stm32wbxx.h"

#if !defined  (HSE_VALUE)
  #define HSE_VALUE    ((uint32_t)32000000) /*!< Value of the External oscillator in Hz */
#endif /* HSE_VALUE */

#if !defined  (MSI_VALUE)
   #define MSI_VALUE    ((uint32_t)4000000) /*!< Value of the Internal oscillator in Hz*/
#endif /* MSI_VALUE */

#if !defined  (HSI_VALUE)
  #define HSI_VALUE    ((uint32_t)16000000) /*!< Value of the Internal oscillator in Hz*/
#endif /* HSI_VALUE */

#if !defined  (LSI_VALUE) 
 #define LSI_VALUE  ((uint32_t)32000)       /*!< Value of LSI in Hz*/
#endif /* LSI_VALUE */ 

#if !defined  (LSE_VALUE)
  #define LSE_VALUE    ((uint32_t)32768)    /*!< Value of LSE in Hz*/
#endif /* LSE_VALUE */

/* The SystemCoreClock variable is updated in three ways:
    1) by calling CMSIS function SystemCoreClockUpdate()
    2) by calling HAL API function HAL_RCC_GetHCLKFreq()
    3) each time HAL_RCC_ClockConfig() is called to configure the system clock frequency
       Note: If you use this function to configure the system clock; then there
             is no need to call the 2 first functions listed above, since SystemCoreClock
             variable is updated automatically.
*/
uint32_t SystemCoreClock = 4000000UL; /*CPU1: M4 on MSI clock after startup (4MHz)*/

const uint32_t AHBPrescTable[16UL] = {
    1UL, 3UL, 5UL, 1UL, 1UL, 6UL, 10UL, 32UL, 2UL, 4UL, 8UL, 16UL,
    64UL, 128UL, 256UL, 512UL
};

const uint32_t APBPrescTable[8UL] = { 0UL, 0UL, 0UL, 0UL, 1UL, 2UL, 3UL, 4UL };

/* 0UL values are incorrect cases */
const uint32_t MSIRangeTable[16UL] = {
    100000UL, 200000UL, 400000UL, 800000UL, 1000000UL, 2000000UL, 4000000UL,
    8000000UL, 16000000UL, 24000000UL, 32000000UL, 48000000UL, 0UL, 0UL, 0UL,
    0UL
};

const uint32_t SmpsPrescalerTable[4UL][6UL] = {
    { 1UL, 3UL, 2UL, 2UL, 1UL, 2UL }, \
    { 2UL, 6UL, 4UL, 3UL, 2UL, 4UL }, \
    { 4UL, 12UL, 8UL, 6UL, 4UL, 8UL }, \
    { 4UL, 12UL, 8UL, 6UL, 4UL, 8UL }
};

void resetSysClocks(void) {
    /* Reset the RCC clock configuration to the default reset state ------------*/
    /* Set MSION bit */
    RCC->CR |= RCC_CR_MSION;

    /* Reset CFGR register */
    RCC->CFGR = 0x00070000U;

    /* Reset PLLSAI1ON, PLLON, HSECSSON, HSEON, HSION, and MSIPLLON bits */
    RCC->CR &= (uint32_t) 0xFAF6FEFBU;

    /*!< Reset LSI1 and LSI2 bits */
    RCC->CSR &= (uint32_t) 0xFFFFFFFAU;

    /*!< Reset HSI48ON  bit */
    RCC->CRRCR &= (uint32_t) 0xFFFFFFFEU;

    /* Reset PLLCFGR register */
    RCC->PLLCFGR = 0x22041000U;

    /* Reset PLLSAI1CFGR register */
    RCC->PLLSAI1CFGR = 0x22041000U;

    /* Reset HSEBYP bit */
    RCC->CR &= (uint32_t) 0xFFFBFFFF;

    /* Disable all interrupts */
    RCC->CIER = 0x00000000;
}

/**
  * @brief  Update SystemCoreClock variable according to Clock Register Values.
  *         The SystemCoreClock variable contains the core clock (HCLK), it can
  *         be used by the user application to setup the SysTick timer or configure
  *         other parameters.
  *
  * @note   Each time the core clock (HCLK) changes, this function must be called
  *         to update SystemCoreClock variable value. Otherwise, any configuration
  *         based on this variable will be incorrect.
  *
  * @note   - The system frequency computed by this function is not the real
  *           frequency in the chip. It is calculated based on the predefined
  *           constant and the selected clock source:
  *
  *           - If SYSCLK source is MSI, SystemCoreClock will contain the MSI_VALUE(*)
  *
  *           - If SYSCLK source is HSI, SystemCoreClock will contain the HSI_VALUE(**)
  *
  *           - If SYSCLK source is HSE, SystemCoreClock will contain the HSE_VALUE(***)
  *
  *           - If SYSCLK source is PLL, SystemCoreClock will contain the HSE_VALUE(***)
  *             or HSI_VALUE(*) or MSI_VALUE(*) multiplied/divided by the PLL factors.
  *
  *         (*) MSI_VALUE is a constant defined in stm32wbxx_hal.h file (default value
  *             4 MHz) but the real value may vary depending on the variations
  *             in voltage and temperature.
  *
  *         (**) HSI_VALUE is a constant defined in stm32wbxx_hal_conf.h file (default value
  *              16 MHz) but the real value may vary depending on the variations
  *              in voltage and temperature.
  *
  *         (***) HSE_VALUE is a constant defined in stm32wbxx_hal_conf.h file (default value
  *              32 MHz), user has to ensure that HSE_VALUE is same as the real
  *              frequency of the crystal used. Otherwise, this function may
  *              have wrong result.
  *
  *         - The result of this function could be not correct when using fractional
  *           value for HSE crystal.
  *
  * @param  None
  * @retval None
  */
void SystemCoreClockUpdate(void) {
    uint32_t tmp = 0, msirange = 0, pllvco = 0, pllr = 2, pllsource = 0, pllm = 2;

    /* Get MSI Range frequency--------------------------------------------------*/

    /*MSI frequency range in Hz*/
    msirange = MSIRangeTable[(RCC->CR & RCC_CR_MSIRANGE) >> RCC_CR_MSIRANGE_Pos];

    /*SystemCoreClock=HAL_RCC_GetSysClockFreq();*/
    /* Get SYSCLK source -------------------------------------------------------*/
    switch (RCC->CFGR & RCC_CFGR_SWS) {
    case 0x00:   /* MSI used as system clock source */
        SystemCoreClock = msirange;
        break;

    case 0x04:  /* HSI used as system clock source */
        /* HSI used as system clock source */
        SystemCoreClock = HSI_VALUE;
        break;

    case 0x08:  /* HSE used as system clock source */
        SystemCoreClock = HSE_VALUE;
        break;

    case 0x0C: /* PLL used as system clock  source */
        /* PLL_VCO = (HSE_VALUE or HSI_VALUE or MSI_VALUE/ PLLM) * PLLN
           SYSCLK = PLL_VCO / PLLR
           */
        pllsource = (RCC->PLLCFGR & RCC_PLLCFGR_PLLSRC);
        pllm = ((RCC->PLLCFGR & RCC_PLLCFGR_PLLM) >> RCC_PLLCFGR_PLLM_Pos) + 1;

        switch (pllsource) {
        case 0x02:  /* HSI used as PLL clock source */
            pllvco = (HSI_VALUE / pllm);
            break;

        case 0x03:  /* HSE used as PLL clock source */
            pllvco = (HSE_VALUE / pllm);
            break;

        default:    /* MSI used as PLL clock source */
            pllvco = (msirange / pllm);
            break;
        }

        pllvco = pllvco * ((RCC->PLLCFGR & RCC_PLLCFGR_PLLN) >> RCC_PLLCFGR_PLLN_Pos);
        pllr = (((RCC->PLLCFGR & RCC_PLLCFGR_PLLR) >> RCC_PLLCFGR_PLLR_Pos) + 1);

        SystemCoreClock = pllvco / pllr;
        break;

    default:
        SystemCoreClock = msirange;
        break;
    }

    /* Compute HCLK clock frequency --------------------------------------------*/
#ifdef CORE_CM0PLUS
    /* Get HCLK2 prescaler */
  tmp = AHBPrescTable[((RCC->EXTCFGR & RCC_EXTCFGR_C2HPRE) >> RCC_EXTCFGR_C2HPRE_Pos)];

#else
    /* Get HCLK1 prescaler */
    tmp = AHBPrescTable[((RCC->CFGR & RCC_CFGR_HPRE) >> RCC_CFGR_HPRE_Pos)];
#endif
    /* HCLK clock frequency */
    SystemCoreClock = SystemCoreClock / tmp;

}
