
#ifndef debug_config_h
#define debug_config_h

#include "sys/misc/macros.h"

// define message scopes
typedef enum {
    APP = 0,        // application
    CONF,           // NV configuration
    LORA,           // LORA/SX1261
    SCO2,           // CCS811 CO2 sensor
    STH,            // HDC2010 temp, humidity sensor
    SYS,            // SYSTEM messages
    SENS,           // general sensor data messages
    OPT,            // OPT3001 light sensor
    DIO,            // digital IOs
    ENV,            // environmental sensors
    TST             // all tests
} Scope;

// define scope printable names
#define SCOPE_PRINT_NAMES               \
{                                       \
    [APP] = "APP",                      \
    [CONF] = "CONF",                    \
    [LORA] = "LORA",                    \
    [SCO2] = "CCS",                     \
    [STH] = "HDC",                      \
    [SYS] = "SYS",                      \
    [SENS] = "SENS",                    \
    [OPT] = "OPT",                      \
    [DIO] = "DIO",                      \
    [ENV] = "ENV",                      \
    [TST] = "TEST"                      \
}

// set default log level for scopes
#ifndef DEF_SCOPE_LOG_LEVEL
#define DEF_SCOPE_LOG_LEVEL             \
{                                       \
    [APP] = LEV_INFO,                   \
    [CONF] = LEV_INFO,                  \
    [LORA] = LEV_INFO,                  \
    [SCO2] = LEV_INFO,                  \
    [STH] = LEV_INFO,                   \
    [SYS] = LEV_INFO,                   \
    [SENS] = LEV_INFO,                  \
    [OPT] = LEV_INFO,                   \
    [DIO] = LEV_INFO,                   \
    [ENV] = LEV_INFO,                   \
    [TST] = LEV_INFO                    \
}
#endif

// switch messages to enable
#ifndef DEBUG_PRINT_EN
#define DEBUG_PRINT_EN                  1
#endif

#ifndef ERROR_MSG_EN
#define ERROR_MSG_EN                    1
#endif

#ifndef WARN_MSG_EN
#define WARN_MSG_EN                     1
#endif

#ifndef INFO_MSG_EN
#define INFO_MSG_EN                     1
#endif

#ifndef DEBUG_MSG_EN
#define DEBUG_MSG_EN                    1
#endif

#ifndef DUMP_MSG_EN
#define DUMP_MSG_EN                     1
#endif

#define HEX_MSG_EN                      1

#ifndef EN_EXC_LOG
#define EN_EXC_LOG                      1
#endif


#define LOG_PRINT_TIME_DELAY            1
#define LOG_SHORT_DELAY                 100
#define LOG_LONG_DELAY                  800
#define NEWLINE_STR                     \r\n
#define NEWLINE                         "\r\n"
#define LF(str)                         CONCAT(str, NEWLINE_STR)
#define LOG_LONG_DELAY_STR              NEWLINE NEWLINE
#define LOG_SHORT_DELAY_STR             NEWLINE
#define LINE_BREAK                      "--------------------------------------"


#endif
