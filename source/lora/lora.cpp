
#include <stdio.h>
#include <debug_log.h>
#include "dev_console.h"
#include <cstring>
#include <syscalls.h>
#include <sys_time.h>
#include <nv_config.h>
#include <misc_crypto.h>
#include <version.h>
#include "dev_config.h"
#include "sys/semaphore.h"

extern "C" {
#include "utilities.h"
#include "board.h"
#include "gpio.h"
#include "LoRaMac.h"
#include "settings.h"
#include "context.h"
#include "LoRaMacTest.h"
}

#include "lora_mac_msg.h"
#include "lora.h"

#ifndef EN_LORA_COMPLIANCE_TST
#define EN_LORA_COMPLIANCE_TST                      0
#endif

#define LORAWAN_APP_DATA_MAX_SIZE                   242

#define COMPLIANCE_TEST_PORT                        224
#define COMPLIANCE_PING_INT                         5000
extern uint8_t alarm_state;
extern uint8_t led3_state;
namespace lora {

enum NodeState {
    START,
    JOIN,
    SEND,
    WAIT_NETW,
    WAIT_ACK,
    WAIT_DATA,
    WAIT_TIMEOUT
};

#ifdef REGION_US915
constexpr LoRaMacRegion_t used_region = LORAMAC_REGION_US915;
#else
constexpr LoRaMacRegion_t used_region = LORAMAC_REGION_EU868;
#endif

LoraConf::User user_conf;
static const LoraConf::Fixed& fixed_conf = config.lora.fixed;

static constexpr size_t min_rqst_interval_ms = 1000;
static constexpr size_t rqst_timeout = 100000;

bool started;
bool init_done;
NodeState state;

struct TxTransfer {
    struct Status {
        bool sent;
        bool completed;
        bool ack;
    };

    bool requested;
    bool server_has_data;
    uint8_t buffer[LORA_MAX_SEND_LEN];
    uint8_t length;
    uint8_t port;
    bool need_ack;
    Status status;
    sys_time::SysTime send_time;
    size_t failed_comms;       //!< consecutive failed comms
};

#if EN_LORA_COMPLIANCE_TST == 1
struct ComplianceTest
{
    bool running;
    uint8_t state;
    bool is_tx_confirmed;
    uint16_t downlink_counter;
    uint8_t demod_margin;
    uint8_t num_gateways;
};

ComplianceTest compliance_test = { .running = false };

static void MCPS_ComplianceTest(McpsIndication_t *mcpsIndication);
static void MLME_ComplianceTest(MlmeConfirm_t *mlmeConfirm);

#endif

bool new_join;
TimerEvent_t timer_handle;
LoRaMacPrimitives_t mac_primitives;
LoRaMacCallback_t mac_callbacks;
TxTransfer tx_transfer = { .requested = false };
sys::Semaphore ready_to_send(true, sys::SEM_BINARY);
sys::Semaphore tx_completed(true, sys::SEM_BINARY);

uint32_t DevAddr;

static void MCPS_ConfirmEvent(McpsConfirm_t *mcpsConfirm);
static void MCPS_IndicationEvent(McpsIndication_t *mcpsIndication);
static void MLME_ConfirmEvent(MlmeConfirm_t *mlmeConfirm);
static void MLME_IndicationEvent(MlmeIndication_t *mlmeIndication);
static void timerEvent(void *context);
static void macStatusEvent();

static bool sendFrame();
static void joinNetwork();
static void readUserConfig();
static void restoreSettings();

// --- Lora API ---
bool isBusy();
bool lastSendStatus();
bool scheduleSend(bool wait_ack, uint8_t port, uint8_t *data, uint8_t length);


bool checkJoinEvent() {
    auto last_status = new_join;
    new_join = false;
    return last_status;
}

bool hasJoinedNetw() {
    MibRequestConfirm_t mib_req;
    mib_req.Type = MIB_NETWORK_ACTIVATION;
    auto status = LoRaMacMibGetRequestConfirm(&mib_req);

    return status == LORAMAC_STATUS_OK
           && mib_req.Param.NetworkActivation != ACTIVATION_TYPE_NONE;
}

bool sendData(bool wait_ack, uint8_t port, uint8_t *data, uint8_t length) {
    // wait ready_to_send
    ready_to_send.timedWait(sys::Semaphore::WAIT_FOREVER);

    // clear send completed
    tx_completed.tryWait();

    // schedule send
    while (!scheduleSend(wait_ack, port, data, length)) {
        // wait while compliance test is running
        sys::sch::threadSleep(1000, sys::PS_MODE_STOP2);
    }

    // wait send_completed
    tx_completed.timedWait(sys::Semaphore::WAIT_FOREVER);

    return lastSendStatus();
}

bool isBusy() {
    return tx_transfer.requested;
}

bool lastSendStatus() {
    return tx_transfer.need_ack ? tx_transfer.status.ack
                                  && tx_transfer.status.completed
                                : tx_transfer.status.completed;
}

bool scheduleSend(bool wait_ack, uint8_t port, uint8_t *data, uint8_t length) {
#if EN_LORA_COMPLIANCE_TST == 1
    if(compliance_test.running && port != COMPLIANCE_TEST_PORT) return false;
#endif
    if(isBusy()) return false;

    tx_transfer.requested = false;
    tx_transfer.status.completed = false;
    tx_transfer.status.ack = false;
    tx_transfer.status.sent = false;

    STRONG_ASSERT(length <= sizeof(tx_transfer.buffer), "payload too large");
    memcpy(tx_transfer.buffer, data, length);
    tx_transfer.length = length;
    tx_transfer.need_ack = wait_ack;
    tx_transfer.port = port;

    MSG_INFO(LORA, "schedule uplink: port %d, len %d", port, length);
    tx_transfer.requested = true;

    requestService(SVC_LORA);

    return true;
}

// --- end ---



bool init() {
    // start lora
    started = true;
    requestService(SVC_LORA);
    return true;
}

void procTasks() {
    using sys_time::getElapsed;

    if(!started) return;

    if(!init_done) {
        MSG_INFO(LORA, "init lora MAC");
        BoardInitMcu();

        mac_primitives.MacMcpsConfirm = MCPS_ConfirmEvent;
        mac_primitives.MacMcpsIndication = MCPS_IndicationEvent;
        mac_primitives.MacMlmeConfirm = MLME_ConfirmEvent;
        mac_primitives.MacMlmeIndication = MLME_IndicationEvent;
        mac_callbacks.GetBatteryLevel = nullptr;
        mac_callbacks.GetTemperatureLevel = nullptr;
        mac_callbacks.NvmContextChange = NvmCtxMgmtEvent;
        mac_callbacks.MacProcessNotify = macStatusEvent;

        auto res_code = LoRaMacInitialization(&mac_primitives, &mac_callbacks,
                                              used_region);
        ASSERT(res_code == LORAMAC_STATUS_OK, "LoRa MAC init failed with %d",
               res_code);

        state = START;

        readUserConfig();
        restoreSettings();

        MSG_INFO(LORA, "DevEui:");
        HEX_MSG(LEV_INFO, LORA, fixed_conf.dev_eui,
                 sizeof(fixed_conf.dev_eui));
        MSG_INFO(LORA, "JoinEUI:");
        HEX_MSG(LEV_INFO, LORA, user_conf.join_eui, sizeof(user_conf.join_eui));

        init_done = true;
        ready_to_send.post();
    }

    if (Radio.IrqProcess != NULL) {
        Radio.IrqProcess();
    }

    LoRaMacProcess();

    if (user_conf.drop_server_cnt != 0 &&
            tx_transfer.failed_comms >= user_conf.drop_server_cnt) {
        MSG_ERROR(LORA, "too much failed comms %d - rejoin server",
            tx_transfer.failed_comms);
        state = JOIN;
        tx_transfer.status.sent = false;
        tx_transfer.failed_comms = 0;
        if(!user_conf.otaa_mode) {
            MSG_ERROR(LORA, "ABP mode - restart dev");
        }
    }

    if (state == WAIT_DATA
            && (tx_transfer.requested || tx_transfer.server_has_data)) {
        state = SEND;
    }

#if EN_LORA_COMPLIANCE_TST == 1
    if (compliance_test.running && !isBusy()
        && (getElapsed(tx_transfer.send_time) > COMPLIANCE_PING_INT)) {
        // send compliance test frame
        uint8_t payload[2] = {
            (uint8_t) (compliance_test.downlink_counter >> 8),
            (uint8_t) compliance_test.downlink_counter
        };
        scheduleSend(true, COMPLIANCE_TEST_PORT,
                     payload, sizeof(payload));
    }
#endif

    switch (state) {
    case START: {
        MSG_INFO(LORA, "starting LORA MAC");

        TimerInit(&timer_handle, timerEvent);

        MibRequestConfirm_t mib_req;

        mib_req.Type = MIB_PUBLIC_NETWORK;
        mib_req.Param.EnablePublicNetwork = LORAWAN_PUBLIC_NETWORK;
        MSG_INFO(LORA, "public netw en = %d", mib_req.Param.EnablePublicNetwork);
        LoRaMacMibSetRequestConfirm(&mib_req);

        mib_req.Type = MIB_ADR;
        mib_req.Param.AdrEnable = user_conf.adr_enable;
        MSG_INFO(LORA, "ADR en = %d", user_conf.adr_enable);
        LoRaMacMibSetRequestConfirm(&mib_req);

#if defined( REGION_EU868 ) || defined( REGION_RU864 )  \
            || defined( REGION_CN779 ) \
            || defined( REGION_EU433 )
        LoRaMacTestSetDutyCycleOn(user_conf.duty_limit_enable);
#endif

        mib_req.Type = MIB_SYSTEM_MAX_RX_ERROR;
        mib_req.Param.SystemMaxRxError = fixed_conf.max_rx_error_ms;
        LoRaMacMibSetRequestConfirm(&mib_req);

        mib_req.Type = MIB_DEVICE_CLASS;
        mib_req.Param.Class = (DeviceClass_t)user_conf.dev_class;
        LoRaMacMibSetRequestConfirm(&mib_req);

        LoRaMacStart();

        mib_req.Type = MIB_NETWORK_ACTIVATION;
        auto status = LoRaMacMibGetRequestConfirm(&mib_req);

        assert(status == LORAMAC_STATUS_OK);
        if (mib_req.Param.NetworkActivation == ACTIVATION_TYPE_NONE) {
            state = JOIN;
        }
        else {
            state = SEND;
        }

        // Important to make one recursive call to start chain of events!
        procTasks();
        break;
    }
    case JOIN: {
        if(user_conf.otaa_mode) {
            joinNetwork();
        }
        else {
            MSG_INFO(LORA, "JOINED");
            MSG_INFO(LORA, "ABP");
            MSG_INFO(LORA, "DevAddr %08lX", DevAddr);
            MSG_INFO(LORA, " NwkSKey:");
            HEX_MSG(LEV_INFO, LORA, (uint8_t *) fixed_conf.FNwkSIntKey, 16);
            MSG_INFO(LORA, "AppSKey:");
            HEX_MSG(LEV_INFO, LORA, (uint8_t *) fixed_conf.AppSKey, 16);

            MibRequestConfirm_t mib_req;
            // Tell the MAC layer which network server version are we connecting too.
            mib_req.Type = MIB_ABP_LORAWAN_VERSION;
            mib_req.Param.AbpLrWanVersion.Value = ABP_ACTIVATION_LRWAN_VERSION;
            LoRaMacMibSetRequestConfirm(&mib_req);

            mib_req.Type = MIB_NETWORK_ACTIVATION;
            mib_req.Param.NetworkActivation = ACTIVATION_TYPE_ABP;
            LoRaMacMibSetRequestConfirm(&mib_req);

            new_join = true;

            state = SEND;
        }
        break;
    }
    case WAIT_NETW: {
        MSG_DUMP(LORA, "WAITING NETWORK");
        break;
    }
    case SEND: {
        if(tx_transfer.requested || tx_transfer.server_has_data) {
            if (sendFrame()) {
                // wait request confirmation
                TimerSetValue(&timer_handle, rqst_timeout);
                state = WAIT_ACK;
            }
            else {
                // rqst failed - wait minimal time before making new request
                TimerSetValue(&timer_handle, min_rqst_interval_ms);
                state = WAIT_TIMEOUT;
            }
        }
        else {
            // nothing to do - save context and wait
            MSG_DEBUG(LORA, "nothing to send");
            if (NvmCtxMgmtStore() == NVMCTXMGMT_STATUS_SUCCESS) {
                MSG_DEBUG(LORA, "context saved");
            }
            auto time_var = randr(-fixed_conf.tx_duty_rnd_ms, fixed_conf.tx_duty_rnd_ms);
            auto idle_time = fixed_conf.tx_duty_ms + time_var;
#if EN_LORA_COMPLIANCE_TST == 1
            if(compliance_test.running) {
                // during compliance test period is 5s
                idle_time = COMPLIANCE_PING_INT;
            }
#endif

            TimerSetValue(&timer_handle, idle_time);
            state = WAIT_DATA;
        }
        TimerStart(&timer_handle);
        break;
    }
    case WAIT_ACK: {
        MSG_DUMP(LORA, "WAITING ACK");
        break;
    }
    case WAIT_DATA: {
        MSG_DUMP(LORA, "WAITING DATA");
        break;
    }
    case WAIT_TIMEOUT:
        MSG_DUMP(LORA, "WAITING TIMEOUT");
        break;
    default: {
        state = START;
        break;
    }
    }
}

static void joinNetwork(void) {
    LoRaMacStatus_t status;
    MlmeReq_t mlmeReq;

    MSG_DEBUG(LORA, "MLME-Join-Request");
    mlmeReq.Type = MLME_JOIN;
    mlmeReq.Req.Join.DevEui = (uint8_t *) fixed_conf.dev_eui;
    mlmeReq.Req.Join.JoinEui = user_conf.join_eui;
    mlmeReq.Req.Join.Datarate = fixed_conf.data_rate;
    status = LoRaMacMlmeRequest(&mlmeReq);
    MSG_DEBUG(LORA, "status: %s", MacStatusStrings[status]);

    // will wait join request completion
    state = WAIT_NETW;

    if (status == LORAMAC_STATUS_OK) {
        // wait for MLME confirm event
        MSG_INFO(LORA, "Join request");
        TimerSetValue(&timer_handle, rqst_timeout);
    }
    else {
        // wait minimal time before making new request
        TimerSetValue(&timer_handle, min_rqst_interval_ms);
    }
    TimerStart(&timer_handle);
}

static bool sendFrame() {
    McpsReq_t mcps_req;
    LoRaMacTxInfo_t tx_info;

    // check maximal payload length available
    auto querry_status = LoRaMacQueryTxPossible(tx_transfer.length, &tx_info);

    if (querry_status != LORAMAC_STATUS_OK ||
            (tx_transfer.server_has_data && !tx_transfer.requested)) {
        MSG_DEBUG(LORA, "LoRaMacQueryTxPossible() = %d", querry_status);
        MSG_DEBUG(LORA, "server has data = %u", tx_transfer.server_has_data);
        MSG_DEBUG(LORA, "sending empty frame");
        // Send empty frame in order to flush MAC commands or
        // receive data downlink from server
        mcps_req.Type = MCPS_UNCONFIRMED;
        mcps_req.Req.Unconfirmed.fPort = 0;
        mcps_req.Req.Unconfirmed.Datarate = fixed_conf.data_rate;
        mcps_req.Req.Unconfirmed.fBuffer = nullptr;
        mcps_req.Req.Unconfirmed.fBufferSize = 0;
    }
    else {
        auto &max_pl_len = tx_info.MaxPossibleApplicationDataSize;
        MSG_DEBUG(LORA, "max pl len %d", max_pl_len);

        if (tx_transfer.need_ack) {
            mcps_req.Type = MCPS_CONFIRMED;
            mcps_req.Req.Confirmed.fPort = tx_transfer.port;
            mcps_req.Req.Confirmed.Datarate = fixed_conf.data_rate;
            mcps_req.Req.Confirmed.fBuffer = tx_transfer.buffer;
            mcps_req.Req.Confirmed.fBufferSize = tx_transfer.length;
            mcps_req.Req.Confirmed.NbTrials = user_conf.retransmit_count;
            if(mcps_req.Req.Confirmed.fBufferSize > max_pl_len) {
                MSG_WARN(LORA, "packet to long");
                mcps_req.Req.Confirmed.fBufferSize = max_pl_len;
            }
        }
        else {
            mcps_req.Type = MCPS_UNCONFIRMED;
            mcps_req.Req.Unconfirmed.fPort = tx_transfer.port;
            mcps_req.Req.Unconfirmed.Datarate = fixed_conf.data_rate;
            mcps_req.Req.Unconfirmed.fBuffer = tx_transfer.buffer;
            mcps_req.Req.Unconfirmed.fBufferSize = tx_transfer.length;
            if(mcps_req.Req.Unconfirmed.fBufferSize > max_pl_len) {
                MSG_WARN(LORA, "packet to long");
                mcps_req.Req.Unconfirmed.fBufferSize = max_pl_len;
            }
        }
        tx_transfer.status.sent = true;
    }

    LoRaMacStatus_t status;
    MSG_DEBUG(LORA, "MCPS-Request %d", mcps_req.Type);
    status = LoRaMacMcpsRequest(&mcps_req);
    MSG_DEBUG(LORA, "status: %s", MacStatusStrings[status]);

    return status == LORAMAC_STATUS_OK;
}

/**
 * @brief timer event is called on request timeouts and periodically to
 *        perform actions as sending more data
 */
static void timerEvent(void *context) {
    MSG_DUMP(LORA, "timer IRQ");
    MibRequestConfirm_t mib_req;
    LoRaMacStatus_t status;

    TimerStop(&timer_handle);

    // request network state
    mib_req.Type = MIB_NETWORK_ACTIVATION;
    status = LoRaMacMibGetRequestConfirm(&mib_req);

    if (status == LORAMAC_STATUS_OK) {
        if (mib_req.Param.NetworkActivation != ACTIVATION_TYPE_NONE) {
            // node has joined network
            state = SEND;
        }
        else {
            // node needs to join network
            state = JOIN;
        }
    }
    else {
        // network state request failed
        state = WAIT_NETW;
        // wait minimal time before making new request
        TimerSetValue(&timer_handle, min_rqst_interval_ms);
    }

    requestService(SVC_LORA);
}

/**
 * @brief MAC Common Part Sublayer - request confirmation
 */
static void MCPS_ConfirmEvent(McpsConfirm_t *mcpsConfirm) {
    MSG_DEBUG(LORA, "MCPS-Confirm");
    MSG_DEBUG(LORA, "status: %s", EventInfoStatusStrings[mcpsConfirm->Status]);

    TimerStop(&timer_handle);

    if(mcpsConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK) {

        MibRequestConfirm_t mib_get;
        MibRequestConfirm_t mib_req;

        mib_req.Type = MIB_DEVICE_CLASS;
        LoRaMacMibGetRequestConfirm(&mib_req);

        MSG_INFO(LORA, "UPLINK FRAME %lu:", mcpsConfirm->UpLinkCounter);
        MSG_INFO(LORA, "rqst %d, class %c, port %d, DR %d, tx pwr %d, tx_time %d",
                 mcpsConfirm->McpsRequest, "ABC"[mib_req.Param.Class],
                 tx_transfer.port, mcpsConfirm->Datarate, mcpsConfirm->TxPower,
                 mcpsConfirm->TxTimeOnAir);

        if (tx_transfer.requested && tx_transfer.status.sent &&
            tx_transfer.length != 0) {
            MSG_DEBUG(LORA, "payload:");
            HEX_MSG(LEV_DEBUG, LORA, tx_transfer.buffer, tx_transfer.length);
        }

        mib_get.Type = MIB_CHANNELS;
        if (LoRaMacMibGetRequestConfirm(&mib_get) == LORAMAC_STATUS_OK) {
            uint32_t &freq = mib_get.Param.ChannelList[mcpsConfirm->Channel].Frequency;
            MSG_INFO(LORA, "F %u.%06u MHz", freq / 1000000, freq % 1000000);
        }

        mib_get.Type = MIB_CHANNELS_MASK;
        if (LoRaMacMibGetRequestConfirm(&mib_get) == LORAMAC_STATUS_OK) {
            MSG_INFO(LORA, "chan mask %04X", mib_get.Param.ChannelsMask[0]);
        }
    }

    tx_transfer.server_has_data = false;

    if (tx_transfer.requested && tx_transfer.status.sent) {
        // if waiting for downlink ack set result
        if(mcpsConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK) {
            if (tx_transfer.need_ack) {
                tx_transfer.status.ack = mcpsConfirm->AckReceived;
                MSG_DEBUG(LORA, "server: %sACK", tx_transfer.status.ack
                                                 ? "" : "N");
            }
            tx_transfer.status.completed = true;
            tx_transfer.failed_comms = 0;
        }
        else {
            tx_transfer.status.completed = false;
            tx_transfer.failed_comms++;
        }

        tx_transfer.send_time = sys_time::getTime();
        tx_transfer.requested = false;

        tx_completed.post();
        ready_to_send.post();
    }

    state = SEND;

    requestService(SVC_LORA);
}

/**
 * @brief MAC Common Part Sublayer - indication
 */
static void MCPS_IndicationEvent(McpsIndication_t *mcpsIndication) {
    MSG_DEBUG(LORA, "MCPS-Indication");
    MSG_DEBUG(LORA, "status: %s", EventInfoStatusStrings[mcpsIndication->Status]);
    if (mcpsIndication->Status != LORAMAC_EVENT_INFO_STATUS_OK) {
        return;
    }

    switch (mcpsIndication->McpsIndication) {
    case MCPS_UNCONFIRMED: {
        break;
    }
    case MCPS_CONFIRMED: {
        break;
    }
    case MCPS_PROPRIETARY: {
        break;
    }
    case MCPS_MULTICAST: {
        break;
    }
    default:
        break;
    }

    if (mcpsIndication->FramePending) {
        // The server signals that it has pending data to be sent.
        // We schedule an uplink as soon as possible to flush the server.
        tx_transfer.server_has_data = true;
    }

#if EN_LORA_COMPLIANCE_TST == 1
    if (compliance_test.running) {
        compliance_test.downlink_counter++;
    }
#endif

    const char *slotStrings[] = { "1", "2", "C", "Ping-Slot",
                                  "Multicast Ping-Slot" };

    MSG_INFO(LORA, "DOWNLINK FRAME %lu:", mcpsIndication->DownLinkCounter);
    MSG_INFO(LORA, "rx window %s, port %d, DR %d, RSSI %d, SNR %d",
        slotStrings[mcpsIndication->RxSlot], mcpsIndication->Port,
        mcpsIndication->RxDatarate, mcpsIndication->Rssi, mcpsIndication->Snr);

    if (mcpsIndication->RxData && mcpsIndication->BufferSize != 0) {
        MSG_DEBUG(LORA, "rx data:");
        HEX_MSG(LEV_DEBUG, LORA, mcpsIndication->Buffer,
            mcpsIndication->BufferSize);
        if (*mcpsIndication->Buffer == 0x01) {

            alarm_state = 1;
            MSG_DEBUG(LORA, "rx data: %X", *mcpsIndication->Buffer);
        }

        else if(*mcpsIndication->Buffer == 0x02){
            alarm_state = 0;
            MSG_DEBUG(LORA, "rx data: %X", *mcpsIndication->Buffer);
        }
        else if(*mcpsIndication->Buffer == 0x33){
            led3_state ^= 1;
            MSG_DEBUG(LORA, "led3_state = %d",led3_state);
        }
    else 
    {

        MSG_DEBUG(LORA, "rx data not valid: %X", *mcpsIndication->Buffer);
    }

        // --- add callbacks to process lora rx data ---
        switch (mcpsIndication->Port) {
        case 22: {
#ifdef EN_LORA_CONSOLE
            serial_console.inputText(mcpsIndication->Buffer,
                              mcpsIndication->BufferSize);
#endif
            break;
        }
        case 224:
#if EN_LORA_COMPLIANCE_TST == 1
            MCPS_ComplianceTest(mcpsIndication);
#endif
            break;
        }
        // ------
    }

    requestService(SVC_LORA);
}

/**
 * @brief MAC layer management entity - request confirmation
 */
static void MLME_ConfirmEvent(MlmeConfirm_t *mlmeConfirm) {
    MSG_DEBUG(LORA, "MLME-Confirm");
    MSG_DEBUG(LORA, "status: %s", EventInfoStatusStrings[mlmeConfirm->Status]);

    TimerStop(&timer_handle);

    switch (mlmeConfirm->MlmeRequest) {
    case MLME_JOIN: {
        if (mlmeConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK) {
            MibRequestConfirm_t mibGet;
            MSG_INFO(LORA, "network joined with OTAA");

            mibGet.Type = MIB_DEV_ADDR;
            LoRaMacMibGetRequestConfirm(&mibGet);
            MSG_INFO(LORA, "DevAddr %08lX", mibGet.Param.DevAddr);

            mibGet.Type = MIB_CHANNELS_DATARATE;
            LoRaMacMibGetRequestConfirm(&mibGet);
            MSG_INFO(LORA, "data rate %d", mibGet.Param.ChannelsDatarate);

            new_join = true;

            state = SEND;
        }
        else {
            // Join was not successful. Try to join again
            state = JOIN;
        }
        break;
    }
    case MLME_LINK_CHECK:
#if EN_LORA_COMPLIANCE_TST == 1
        MLME_ComplianceTest(mlmeConfirm);
#endif
        break;
    default:
        MSG_ERROR(LORA, "unused MLME rqst %d return", mlmeConfirm->MlmeRequest);
        state = JOIN;
        break;
    }

    requestService(SVC_LORA);
}

/**
 * @brief MAC layer management entity - indication
 */
static void MLME_IndicationEvent(MlmeIndication_t *mlmeIndication) {
    if (mlmeIndication->Status != LORAMAC_EVENT_INFO_STATUS_BEACON_LOCKED) {
        MSG_DEBUG(LORA, "MLME-Indication");
        MSG_DEBUG(LORA, "status: %s", EventInfoStatusStrings[mlmeIndication->Status]);
    }

    MibRequestConfirm_t mib_req;
    LoRaMacStatus_t status;

    mib_req.Type = MIB_NETWORK_ACTIVATION;
    status = LoRaMacMibGetRequestConfirm(&mib_req);

    if (status == LORAMAC_STATUS_OK
            && mib_req.Param.NetworkActivation != ACTIVATION_TYPE_NONE) {
        switch (mlmeIndication->MlmeIndication) {
        case MLME_SCHEDULE_UPLINK: {
            // The MAC signals that we shall provide an uplink as soon as possible
            tx_transfer.server_has_data = true;
            requestService(SVC_LORA);
            break;
        }
        default:
            MSG_WARN(LORA, "unexpected MLME indication");
            break;
        }

    }
}

static void macStatusEvent() {
//    pendSVC(SVC_LORA);
}

static void readUserConfig() {
    MSG_INFO(LORA, "Reading LORA user settings:");
    bool is_valid;
    user_conf = nvconf::readBankSafe<LoraConf::User>(NVC_BANK_LORA, 0, is_valid);

    if(!is_valid) {
        // wrong crc - reset to defaults
        MSG_ERROR(LORA, "Config CRC wrong - will reset");
        user_conf = config.lora.user;

        // write new config to memory
        nvconf::writeBankSafe(NVC_BANK_LORA, 0, user_conf);
    }
    MSG_INFO(LORA, "OTAA %d", user_conf.otaa_mode);
    MSG_INFO(LORA, "public netw %d", user_conf.public_netw);
    MSG_INFO(LORA, "tx duty %d ms", fixed_conf.tx_duty_ms);
    MSG_INFO(LORA, "tx duty rnd %d ms", fixed_conf.tx_duty_rnd_ms);
    MSG_INFO(LORA, "duty limit enable %d", user_conf.duty_limit_enable);
    MSG_INFO(LORA, "data rate %d", fixed_conf.data_rate);
    MSG_INFO(LORA, "ADR enable %d", user_conf.adr_enable);
    MSG_INFO(LORA, "max rx error %d ms", fixed_conf.max_rx_error_ms);
    MSG_INFO(LORA, "retransmit count %d", user_conf.retransmit_count);
    MSG_INFO(LORA, "dev class %d", user_conf.dev_class);
    MSG_INFO(LORA, "restore mac context %d", fixed_conf.restore_context);
}

static void restoreSettings() {
    MSG_INFO(LORA, "load mac context");

    // its important to call NvmCtxMgmtRestore() always
    if (NvmCtxMgmtRestore() == NVMCTXMGMT_STATUS_SUCCESS) {
        MSG_INFO(LORA, "ok");
        if(fixed_conf.restore_context) return;
    }

    MSG_INFO(LORA, "using new settings:");

    MibRequestConfirm_t mib_req;
    mib_req.Type = MIB_APP_KEY;
    // drop const in assignment - should be ok
    mib_req.Param.AppKey = (uint8_t*)fixed_conf.app_key;
    MSG_INFO(LORA, "AppKey:");
    HEX_MSG(LEV_INFO, LORA, (uint8_t *) fixed_conf.app_key,
             sizeof(fixed_conf.app_key));
    LoRaMacMibSetRequestConfirm(&mib_req);

    mib_req.Type = MIB_NWK_KEY;
    // drop const in assignment - should be ok
    mib_req.Param.NwkKey = (uint8_t*)fixed_conf.app_key;
    LoRaMacMibSetRequestConfirm(&mib_req);

    if(!user_conf.otaa_mode) {
        // Choose a random device address if not already defined in settings.h
        if (DevAddr == 0) {
            // Random seed initialization
            srand1(BoardGetRandomSeed());

            // Choose a random device address
            DevAddr = randr(0, 0x01FFFFFF);
        }

        mib_req.Type = MIB_NET_ID;
        mib_req.Param.NetID = LORAWAN_NETWORK_ID;
        LoRaMacMibSetRequestConfirm(&mib_req);

        mib_req.Type = MIB_DEV_ADDR;
        mib_req.Param.DevAddr = DevAddr;
        LoRaMacMibSetRequestConfirm(&mib_req);

        mib_req.Type = MIB_F_NWK_S_INT_KEY;
        mib_req.Param.FNwkSIntKey = (uint8_t *) fixed_conf.FNwkSIntKey;
        LoRaMacMibSetRequestConfirm(&mib_req);

        mib_req.Type = MIB_S_NWK_S_INT_KEY;
        mib_req.Param.SNwkSIntKey = (uint8_t *) fixed_conf.SNwkSIntKey;
        LoRaMacMibSetRequestConfirm(&mib_req);

        mib_req.Type = MIB_NWK_S_ENC_KEY;
        mib_req.Param.NwkSEncKey = (uint8_t *) fixed_conf.NwkSEncKey;
        LoRaMacMibSetRequestConfirm(&mib_req);

        mib_req.Type = MIB_APP_S_KEY;
        mib_req.Param.AppSKey = (uint8_t *) fixed_conf.AppSKey;
        LoRaMacMibSetRequestConfirm(&mib_req);
    }
}

#if EN_LORA_COMPLIANCE_TST == 1
static void MCPS_ComplianceTest(McpsIndication_t *mcpsIndication) {
    if (!compliance_test.running) {
        // Check compliance test enable command (i)
        if ((mcpsIndication->BufferSize == 4) &&
            (mcpsIndication->Buffer[0] == 0x01) &&
            (mcpsIndication->Buffer[1] == 0x01) &&
            (mcpsIndication->Buffer[2] == 0x01) &&
            (mcpsIndication->Buffer[3] == 0x01)) {

            MSG_WARN(LORA, "enabled compliance test");

            if(tx_transfer.requested) {
                MSG_WARN(LORA, "abort normal send");
                tx_transfer.status.ack = false;
                tx_transfer.status.completed = true;
                tx_transfer.requested = false;
            }

            compliance_test.downlink_counter = 0;
            compliance_test.demod_margin = 0;
            compliance_test.num_gateways = 0;
            compliance_test.running = true;
            compliance_test.state = 1;

            MibRequestConfirm_t mibReq;
            mibReq.Type = MIB_ADR;
            mibReq.Param.AdrEnable = true;
            LoRaMacMibSetRequestConfirm(&mibReq);

#if defined( REGION_EU868 ) || defined( REGION_RU864 ) || \
    defined( REGION_CN779 ) || defined( REGION_EU433 )
            LoRaMacTestSetDutyCycleOn(false);
#endif
        }
    }
    else {
        compliance_test.state = mcpsIndication->Buffer[0];
        MSG_WARN(LORA, "test: state %d", compliance_test.state);

        switch (compliance_test.state) {
        case 0:
            // disable compliance test
            compliance_test.running = false;

            MibRequestConfirm_t mib_req;
            mib_req.Type = MIB_ADR;
            mib_req.Param.AdrEnable = user_conf.adr_enable;
            LoRaMacMibSetRequestConfirm(&mib_req);

            LoRaMacTestSetDutyCycleOn(user_conf.duty_limit_enable);
            break;
        case 1: {
            // (iii, iv)
            // send down link counter
            break;
        }
        case 2:
            // Enable confirmed messages (v)
            compliance_test.is_tx_confirmed = true;
            compliance_test.state = 1;
            break;
        case 3:
            // Disable confirmed messages (vi)
            compliance_test.is_tx_confirmed = false;
            compliance_test.state = 1;
            break;
        case 4: {
            // (vii)
            size_t len = MIN(mcpsIndication->BufferSize, LORAWAN_APP_DATA_MAX_SIZE);
            STRONG_ASSERT(len <= LORAWAN_APP_DATA_MAX_SIZE,
                          "bad buffer size %d", len);
            uint8_t tmp_buffer[len];
            tmp_buffer[0] = compliance_test.state;
            for(size_t i = 1; i < len; i++) {
                tmp_buffer[i] = mcpsIndication->Buffer[i] + 1;
            }
            scheduleSend(compliance_test.is_tx_confirmed, COMPLIANCE_TEST_PORT,
                         tmp_buffer, sizeof(tmp_buffer));
            break;
        }
        case 5: {
            // (viii)
            MlmeReq_t mlmeReq;
            mlmeReq.Type = MLME_LINK_CHECK;
            LoRaMacStatus_t status = LoRaMacMlmeRequest(&mlmeReq);
            MSG_INFO(LORA, "CTST: link check");
            MSG_DEBUG(LORA, "status: %s", MacStatusStrings[status]);
            break;
        }
        case 6: {
            // (ix)
            // Disable TestMode and revert back to normal operation
            compliance_test.running = false;

            MibRequestConfirm_t mibReq;
            mibReq.Type = MIB_ADR;
            mibReq.Param.AdrEnable = user_conf.adr_enable;
            LoRaMacMibSetRequestConfirm(&mibReq);
            LoRaMacTestSetDutyCycleOn(user_conf.duty_limit_enable);

            state = JOIN;
            break;
        }
        case 7: {
            if (mcpsIndication->BufferSize == 3) {
                MlmeReq_t mlmeReq;
                mlmeReq.Type = MLME_TXCW;
                mlmeReq.Req.TxCw.Timeout = (uint16_t) ((mcpsIndication->Buffer[1] << 8) |
                                                       mcpsIndication->Buffer[2]);
                LoRaMacStatus_t status = LoRaMacMlmeRequest(&mlmeReq);
                MSG_INFO(LORA, "CTST: set TXCW");
                MSG_DEBUG(LORA, "status: %s", MacStatusStrings[status]);
            }
            else if (mcpsIndication->BufferSize == 7) {
                MlmeReq_t mlmeReq;
                mlmeReq.Type = MLME_TXCW_1;
                mlmeReq.Req.TxCw.Timeout = (uint16_t) ((mcpsIndication->Buffer[1] << 8) |
                                                       mcpsIndication->Buffer[2]);
                mlmeReq.Req.TxCw.Frequency = (uint32_t) ((mcpsIndication->Buffer[3] << 16) |
                                                         (mcpsIndication->Buffer[4] << 8) |
                                                         mcpsIndication->Buffer[5]) * 100;
                mlmeReq.Req.TxCw.Power = mcpsIndication->Buffer[6];
                LoRaMacStatus_t status = LoRaMacMlmeRequest(&mlmeReq);
                MSG_INFO(LORA, "CTST: set TXCW1");
                MSG_DEBUG(LORA, "status: %s", MacStatusStrings[status]);
            }
            compliance_test.state = 1;
            break;
        }
        default:
            break;
        }
    }
}

static void MLME_ComplianceTest(MlmeConfirm_t *mlmeConfirm) {
    if (mlmeConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK) {

        if (compliance_test.running) {
            compliance_test.demod_margin = mlmeConfirm->DemodMargin;
            compliance_test.num_gateways = mlmeConfirm->NbGateways;
            uint8_t payload[3] = { 5, compliance_test.demod_margin,
                                   compliance_test.num_gateways };

            MSG_INFO(LORA, "CTST: demod-margin %d, %d gateways visible",
                     compliance_test.demod_margin, compliance_test.num_gateways);

            scheduleSend(compliance_test.is_tx_confirmed, COMPLIANCE_TEST_PORT,
                payload, sizeof(payload));
        }
    }
}
#endif

}   // namespace lora




