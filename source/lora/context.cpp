
extern "C" {

#include <stdio.h>
#include "utilities.h"
#include "nvmm.h"
#include "context.h"

#define NVM_CTX_STORAGE_MASK               0x8C



/*!
* LoRaMAC Structure holding contexts changed status
* in case of a \ref MLME_NVM_CTXS_UPDATE indication.
*/
typedef union uLoRaMacCtxsUpdateInfo
{
    /*!
     * Byte-access to the bits
     */
    uint8_t Value;
    /*!
     * The according context bit will be set to one
     * if the context changed or 0 otherwise.
     */
    struct sElements
    {
        uint8_t Mac : 1;
        uint8_t Region : 1;
        uint8_t Crypto : 1;
        uint8_t SecureElement : 1;
        uint8_t Commands : 1;
        uint8_t ClassB : 1;
        uint8_t ConfirmQueue : 1;
        uint8_t FCntHandlerNvmCtx : 1;
    }Elements;
}LoRaMacCtxUpdateStatus_t;

static NvmmDataBlock_t FCntHandlerNvmCtxDataBlock;
static NvmmDataBlock_t SecureElementNvmCtxDataBlock;
static NvmmDataBlock_t CryptoNvmCtxDataBlock;

LoRaMacCtxUpdateStatus_t CtxUpdateStatus = { .Value = 0 };

void NvmCtxMgmtEvent( LoRaMacNvmCtxModule_t module )
{
    switch( module )
    {
        case LORAMAC_NVMCTXMODULE_MAC:
        {
            CtxUpdateStatus.Elements.Mac = 1;
            break;
        }
        case LORAMAC_NVMCTXMODULE_REGION:
        {
            CtxUpdateStatus.Elements.Region = 1;
            break;
        }
        case LORAMAC_NVMCTXMODULE_CRYPTO:
        {
            CtxUpdateStatus.Elements.Crypto = 1;
            break;
        }
        case LORAMAC_NVMCTXMODULE_SECURE_ELEMENT:
        {
            CtxUpdateStatus.Elements.SecureElement = 1;
            break;
        }
        case LORAMAC_NVMCTXMODULE_COMMANDS:
        {
            CtxUpdateStatus.Elements.Commands = 1;
            break;
        }
        case LORAMAC_NVMCTXMODULE_CLASS_B:
        {
            CtxUpdateStatus.Elements.ClassB = 1;
            break;
        }
        case LORAMAC_NVMCTXMODULE_CONFIRM_QUEUE:
        {
            CtxUpdateStatus.Elements.ConfirmQueue = 1;
            break;
        }
        case LORAMAC_NVMCTXMODULE_FCNT_HANDLER:
        {
            CtxUpdateStatus.Elements.FCntHandlerNvmCtx = 1;
            break;
        }
        default:
        {
            break;
        }
    }
}


NvmCtxMgmtStatus_t NvmCtxMgmtStore( void )
{
    // Read out the contexts lengths and pointers
    MibRequestConfirm_t mibReq;
    mibReq.Type = MIB_NVM_CTXS;
    LoRaMacMibGetRequestConfirm( &mibReq );
    LoRaMacCtxs_t* MacContexts = mibReq.Param.Contexts;

    // Input checks
    if( ( CtxUpdateStatus.Value & NVM_CTX_STORAGE_MASK ) == 0 )
    {
        return NVMCTXMGMT_STATUS_FAIL;
    }
    if( LoRaMacStop( ) != LORAMAC_STATUS_OK )
    {
        return NVMCTXMGMT_STATUS_FAIL;
    }

    // Write
    if( CtxUpdateStatus.Elements.Crypto == 1 )
    {
        if( NvmmWrite( &CryptoNvmCtxDataBlock, MacContexts->CryptoNvmCtx, MacContexts->CryptoNvmCtxSize ) != NVMM_SUCCESS )
        {
            return NVMCTXMGMT_STATUS_FAIL;
        }
    }

    if( CtxUpdateStatus.Elements.SecureElement == 1 )
    {
        if( NvmmWrite( &SecureElementNvmCtxDataBlock, MacContexts->SecureElementNvmCtx, MacContexts->SecureElementNvmCtxSize ) != NVMM_SUCCESS )
        {
            return NVMCTXMGMT_STATUS_FAIL;
        }
    }

    if( CtxUpdateStatus.Elements.FCntHandlerNvmCtx == 1 )
    {
        if( NvmmWrite( &FCntHandlerNvmCtxDataBlock, MacContexts->FCntHandlerNvmCtx, MacContexts->FCntHandlerNvmCtxSize ) != NVMM_SUCCESS )
        {
            return NVMCTXMGMT_STATUS_FAIL;
        }
    }

    CtxUpdateStatus.Value = 0x00;

    // Resume LoRaMac
    LoRaMacStart( );

    return NVMCTXMGMT_STATUS_SUCCESS;
}

NvmCtxMgmtStatus_t NvmCtxMgmtRestore( void )
{
    MibRequestConfirm_t mibReq;
    LoRaMacCtxs_t contexts = { 0 };
    NvmCtxMgmtStatus_t status = NVMCTXMGMT_STATUS_SUCCESS;

    // Read out the contexts lengths
    mibReq.Type = MIB_NVM_CTXS;
    LoRaMacMibGetRequestConfirm( &mibReq );


    uint8_t NvmCryptoCtxRestore[mibReq.Param.Contexts->CryptoNvmCtxSize];
    uint8_t NvmSecureElementCtxRestore[mibReq.Param.Contexts->SecureElementNvmCtxSize];
    uint8_t NvmFCntHandlerCtxRestore[mibReq.Param.Contexts->FCntHandlerNvmCtxSize];

    if ( NvmmDeclare( &CryptoNvmCtxDataBlock, mibReq.Param.Contexts->CryptoNvmCtxSize ) == NVMM_SUCCESS )
    {
        NvmmRead( &CryptoNvmCtxDataBlock, NvmCryptoCtxRestore, mibReq.Param.Contexts->CryptoNvmCtxSize );
        contexts.CryptoNvmCtx = &NvmCryptoCtxRestore;
        contexts.CryptoNvmCtxSize = mibReq.Param.Contexts->CryptoNvmCtxSize;
    }
    else
    {
        status = NVMCTXMGMT_STATUS_FAIL;
    }

    if ( NvmmDeclare( &SecureElementNvmCtxDataBlock, mibReq.Param.Contexts->SecureElementNvmCtxSize ) == NVMM_SUCCESS )
    {
        NvmmRead( &SecureElementNvmCtxDataBlock, NvmSecureElementCtxRestore, mibReq.Param.Contexts->SecureElementNvmCtxSize );
        contexts.SecureElementNvmCtx = &NvmSecureElementCtxRestore;
        contexts.SecureElementNvmCtxSize = mibReq.Param.Contexts->SecureElementNvmCtxSize;
    }
    else
    {
        status = NVMCTXMGMT_STATUS_FAIL;
    }

    if ( NvmmDeclare( &FCntHandlerNvmCtxDataBlock, mibReq.Param.Contexts->FCntHandlerNvmCtxSize ) == NVMM_SUCCESS )
    {
        NvmmRead( &FCntHandlerNvmCtxDataBlock, NvmFCntHandlerCtxRestore, mibReq.Param.Contexts->FCntHandlerNvmCtxSize );
        contexts.FCntHandlerNvmCtx = &NvmFCntHandlerCtxRestore;
        contexts.FCntHandlerNvmCtxSize = mibReq.Param.Contexts->FCntHandlerNvmCtxSize;
    }
    else
    {
        status = NVMCTXMGMT_STATUS_FAIL;
    }

    // Enforce storing all contexts
    if( status == NVMCTXMGMT_STATUS_FAIL )
    {
        CtxUpdateStatus.Value = 0xFF;
        NvmCtxMgmtStore( );
    }
    else
    {  // If successful query the mac to restore contexts
        mibReq.Type = MIB_NVM_CTXS;
        mibReq.Param.Contexts = &contexts;
        LoRaMacMibSetRequestConfirm( &mibReq );
    }

    return status;
}


}
