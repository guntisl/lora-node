/*!
 * \file      board.c
 *
 * \brief     Target board general functions implementation
 *
 * \copyright Revised BSD License, see section \ref LICENSE.
 *
 * \code
 *                ______                              _
 *               / _____)             _              | |
 *              ( (____  _____ ____ _| |_ _____  ____| |__
 *               \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 *               _____) ) ____| | | || |_| ____( (___| | | |
 *              (______/|_____)_|_|_| \__)_____)\____)_| |_|
 *              (C)2013-2017 Semtech
 *
 * \endcode
 *
 * \author    Miguel Luis ( Semtech )
 *
 * \author    Gregory Cristian ( Semtech )
 */
#include "stm32wbxx.h"
#include "utilities.h"
#include "gpio.h"
#include "adc.h"
#include "spi.h"
#include "i2c.h"
#include "uart.h"
#include "timer.h"
#include "board-config.h"
#include "lpm-board.h"
#include "rtc-board.h"

#include "sx126x-board.h"
#include "board.h"

/*!
 * Unique Devices IDs register set ( STM32L4xxx )
 */
#define         ID1                                 ( 0x1FFF7590 )
#define         ID2                                 ( 0x1FFF7594 )
#define         ID3                                 ( 0x1FFF7594 )

/*!
 * Used to measure and calibrate the system wake-up time from STOP mode
 */
//static void CalibrateSystemWakeupTime(void);

/*!
 * Timer used at first boot to calibrate the SystemWakeupTime
 */
//static TimerEvent_t CalibrateSystemWakeupTimeTimer;

/*!
 * Flag to indicate if the MCU is Initialized
 */
static bool McuInitialized = false;

/*!
 * Flag to indicate if the SystemWakeupTime is Calibrated
 */
//static volatile bool SystemWakeupTimeCalibrated = false;

/*!
 * Callback indicating the end of the system wake-up time calibration
static void OnCalibrateSystemWakeupTimeTimerEvent(void *context) {
    RtcSetMcuWakeUpTime();
    SystemWakeupTimeCalibrated = true;
}

*/
void BoardCriticalSectionBegin(uint32_t *mask) {
    *mask = __get_PRIMASK();
    __disable_irq();
}

void BoardCriticalSectionEnd(uint32_t *mask) {
    __set_PRIMASK(*mask);
}

void BoardInitMcu(void) {
    SpiInit(&SX126x.Spi, SPI_1, RADIO_MOSI, RADIO_MISO, RADIO_SCLK, NC);
    SX126xIoInit();

    McuInitialized = true;
}

void BoardDeInitMcu(void) {
    SpiDeInit(&SX126x.Spi);
    SX126xIoDeInit();
}

uint32_t BoardGetRandomSeed(void) {
    // TODO: return real random number for seed
    return ((*(uint32_t *) ID1) ^ (*(uint32_t *) ID2) ^ (*(uint32_t *) ID3));
}

void BoardGetUniqueId(uint8_t *id) {
    id[7] = ((*(uint32_t *) ID1) + (*(uint32_t *) ID3)) >> 24;
    id[6] = ((*(uint32_t *) ID1) + (*(uint32_t *) ID3)) >> 16;
    id[5] = ((*(uint32_t *) ID1) + (*(uint32_t *) ID3)) >> 8;
    id[4] = ((*(uint32_t *) ID1) + (*(uint32_t *) ID3));
    id[3] = ((*(uint32_t *) ID2)) >> 24;
    id[2] = ((*(uint32_t *) ID2)) >> 16;
    id[1] = ((*(uint32_t *) ID2)) >> 8;
    id[0] = ((*(uint32_t *) ID2));
}

/*

void CalibrateSystemWakeupTime(void) {

    if (SystemWakeupTimeCalibrated == false) {
        TimerInit(&CalibrateSystemWakeupTimeTimer,
            OnCalibrateSystemWakeupTimeTimerEvent);
        TimerSetValue(&CalibrateSystemWakeupTimeTimer, 1000);
        TimerStart(&CalibrateSystemWakeupTimeTimer);
        while (SystemWakeupTimeCalibrated == false) {

        }
    }
}
*/
