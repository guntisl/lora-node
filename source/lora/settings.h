
#ifndef settings_h
#define settings_h


// --- LORA MAC default settings ---

#define RESTORE_SAVED_CONTEXT                       false

// tx event period in [ms] (if duty cycle is on some events are skipped)
#define APP_TX_DUTYCYCLE                            10000

// random variation in tx duty cycle period
#define APP_TX_DUTYCYCLE_RND                        100

#define LORAWAN_DATARATE                            DR_0

// adaptive data rate (gateway can control node data rate)
#define LORAWAN_ADR_ON                              1

// enforce maximum time on air limiting (for compliance, disable only for tests)
#define LORAWAN_DUTYCYCLE_ON                        false

#define MAX_RX_ERROR_ms                             20

#define RETRANSMIT_CNT                              0

#define LORAWAN_CLASS                               CLASS_A

#define LORA_REJOIN_LIMIT                           10


// --- Node activation settings ---
/*!
 * 1 the application uses the Over-the-Air activation procedure (OTAA)
 * 0 the application uses the Personalization activation procedure (ABP)
 */
#define OVER_THE_AIR_ACTIVATION                     1

// changes phy packet synch word
#define LORAWAN_PUBLIC_NETWORK                      true


// --- Over-the-Air activation procedure settings (OTAA) ---
#define IEEE_OUI                    0xE4, 0xAA, 0xBB
#define LORAWAN_DEVICE_EUI          { IEEE_OUI, 0x00, 0x00, 0x00, 0x0A, 0x04 }

// default App/Join server IEEE EUI (big endian)
#define LORAWAN_JOIN_EUI            { 0xE0, 0xE1, 0xE2, 0x01, 0x02, 0x03, 0x04, 0x04 }

/*!
 * Application root key
 * WARNING: NOT USED FOR 1.0.x DEVICES
 */
#define LORAWAN_APP_KEY             { 0x00, 0xB0, 0x32, 0x09, 0x50, 0x10, 0x03, 0xD0, 0xDA, 0x08, 0xE0, 0xB6, 0x05, 0x40, 0x33, 0x44 }

/*!
 * Network root key
 * WARNING: FOR 1.0.x DEVICES IT IS THE \ref LORAWAN_DEFAULT_APP_KEY
 */
#define LORAWAN_DEFAULT_NWK_KEY     LORAWAN_APP_KEY





// --- Personalization activation procedure settings (ABP) ---
/*!
 * Current network ID
 */
#define LORAWAN_NETWORK_ID          ( uint32_t )0

/*!
 * Device address on the network (big endian)
 *
 * \remark In this application the value is automatically generated using
 *         a pseudo random generator seeded with a value derived from
 *         BoardUniqueId value if LORAWAN_DEVICE_ADDRESS is set to 0
 */
#define LORAWAN_DEVICE_ADDRESS      ( uint32_t )0x26011F11

/*!
 * Forwarding Network session integrity key
 * WARNING: NWK_S_KEY FOR 1.0.x DEVICES
 */
#define LORAWAN_F_NWK_S_INT_KEY     { 0xF0, 0x69, 0xD1, 0x09, 0xE1, 0x80, 0x2B, 0x67, 0xC7, 0x54, 0x34, 0xAF, 0xB7, 0x19, 0xD7, 0x5F }

/*!
 * Serving Network session integrity key
 * WARNING: NOT USED FOR 1.0.x DEVICES. MUST BE THE SAME AS \ref LORAWAN_F_NWK_S_INT_KEY
 */
#define LORAWAN_S_NWK_S_INT_KEY     LORAWAN_F_NWK_S_INT_KEY

/*!
 * Network session encryption key
 * WARNING: NOT USED FOR 1.0.x DEVICES. MUST BE THE SAME AS \ref LORAWAN_F_NWK_S_INT_KEY
 */
#define LORAWAN_NWK_S_ENC_KEY       LORAWAN_F_NWK_S_INT_KEY

/*!
 * Application session key
 */
#define LORAWAN_APP_S_KEY           { 0xF0, 0x84, 0x4C, 0x53, 0x60, 0xDA, 0x35, 0xF9, 0x11, 0xCB, 0xFA, 0xCC, 0x13, 0xC8, 0xD2, 0x45 }

/*!
 * When using ABP activation the MAC layer must know in advance to which server
 * version it will be connected.
 */
#define ABP_ACTIVATION_LRWAN_VERSION_V10x   0x01000300 // 1.0.3.0

#define ABP_ACTIVATION_LRWAN_VERSION        ABP_ACTIVATION_LRWAN_VERSION_V10x

#endif
