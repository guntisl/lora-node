
#ifndef lora_h
#define lora_h


#include "stdint.h"
#include "stdlib.h"
#include "settings.h"

namespace lora {

#define LORA_MAX_SEND_LEN                           51

#define LORA_EUI_LEN                                8
#define LORA_KEY_LEN                                16

struct __attribute__((aligned(4))) LoraConf {
    /// factory fixed configuration
    struct __attribute__((aligned(4))) Fixed {
        // OTAA mode
        uint8_t dev_eui[LORA_EUI_LEN];
        uint8_t app_key[LORA_KEY_LEN];

        // APB mode
        uint8_t FNwkSIntKey[LORA_KEY_LEN];
        uint8_t SNwkSIntKey[LORA_KEY_LEN];
        uint8_t NwkSEncKey[LORA_KEY_LEN];
        uint8_t AppSKey[LORA_KEY_LEN];

        // MAC
        uint32_t tx_duty_ms;
        uint32_t tx_duty_rnd_ms;
        uint8_t data_rate;
        uint16_t max_rx_error_ms;
        bool restore_context;       // if set lora driver reads saved context
    };

    /// user variable configuration
    struct __attribute__((aligned(4))) User {
        bool otaa_mode;
        bool public_netw;
        bool duty_limit_enable;
        bool adr_enable;
        uint8_t retransmit_count;
        uint8_t dev_class;
        uint8_t join_eui[LORA_EUI_LEN];
        size_t drop_server_cnt;
    };

    Fixed fixed;
    User user;
};


extern LoraConf::User user_conf;

bool init();
/**
 * @brief return true once for each network join
 */
bool checkJoinEvent();
bool hasJoinedNetw();
/**
 * @brief send data block to lora server
 * @param length bytes to send, maximum = 51
 */
bool sendData(bool wait_ack, uint8_t port, uint8_t *data, uint8_t length);

/*
 * To process rx data add callback function in MCPS_IndicationEvent()
 */

/**
 * @brief Process all pending tasks
 * @details Don't call directly, but use pendSVC(SVC_LORA)
 */
void procTasks();

}

#endif
