
#ifndef context_h
#define context_h

#include "LoRaMac.h"

extern "C" {

typedef enum NvmCtxMgmtStatus_e {
    NVMCTXMGMT_STATUS_SUCCESS,
    NVMCTXMGMT_STATUS_FAIL
} NvmCtxMgmtStatus_t;


void NvmCtxMgmtEvent(LoRaMacNvmCtxModule_t module);
NvmCtxMgmtStatus_t NvmCtxMgmtStore();
NvmCtxMgmtStatus_t NvmCtxMgmtRestore();

}

#endif
