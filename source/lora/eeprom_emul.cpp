
#include "nv_config.h"

extern "C" {
#include "eeprom.h"
#include "utilities.h"

// this is wrapper for lora mac

uint8_t EepromWriteBuffer(uint16_t addr, uint8_t *buffer, uint16_t size) {
    nvconf::writeBank(NVC_BANK_LORA_MAC, addr, buffer, size);
    return SUCCESS;
}

uint8_t EepromReadBuffer(uint16_t addr, uint8_t *buffer, uint16_t size) {
    nvconf::readBank(NVC_BANK_LORA_MAC, addr, buffer, size);
    return SUCCESS;
}

}