
/**
 * @brief Reimplementation for lora driver defined functions, to
 * integrate driver
 */

#include "debug_log.h"
#include "sys_time.h"

extern "C" {
#include "timer.h"      // definitions being reimplemented


void TimerInit(TimerEvent_t *obj, void ( *callback )(void *context)) {
    obj->Timestamp = 0;
    obj->ReloadValue = 0;
    obj->IsStarted = false;
    obj->IsNext2Expire = false;
    obj->Callback = callback;
    obj->Context = NULL;

    // Use lora next pointer to hold handles for system timers
    // pointers are same size, and Next was used only by driver itself
    obj->Next = (struct TimerEvent_s *) sys_time::createTimer();

    STRONG_ASSERT(obj->Next != nullptr, "timer create failed");
}

void TimerSetContext(TimerEvent_t *obj, void *context) {
    obj->Context = context;
}

void TimerStart(TimerEvent_t *obj) {
    sys_time::startTimer(obj->Next, obj->ReloadValue,
                         (sys_time::TimerCallback) obj->Callback,
                         (size_t) obj->Context);
}

bool TimerIsStarted(TimerEvent_t *obj) {
    return sys_time::isRunning(obj->Next);
}

void TimerStop(TimerEvent_t *obj) {
    sys_time::stopTimer(obj->Next);
}

void TimerReset(TimerEvent_t *obj) {
    sys_time::stopTimer(obj->Next);
    sys_time::startTimer(obj->Next, obj->ReloadValue,
                         (sys_time::TimerCallback) obj->Callback,
                         (size_t) obj->Context);
}

void TimerSetValue(TimerEvent_t *obj, uint32_t value) {
    TimerStop(obj);
    obj->Timestamp = value;
    obj->ReloadValue = value;
}

TimerTime_t TimerGetCurrentTime() {
    return sys_time::getTime();
}

TimerTime_t TimerGetElapsedTime(TimerTime_t past) {
    return sys_time::getElapsed(past);
}

TimerTime_t TimerTempCompensation(TimerTime_t period, float temperature) {
    STRONG_ASSERT(0, "Not implemented");
    // TODO: clock correction for RTC, LORA class B
}

void TimerProcess() {
    STRONG_ASSERT(0, "shouldn't call this");
}

}