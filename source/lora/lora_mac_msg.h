
#ifndef lora_mac_msg_h
#define lora_mac_msg_h

#include "LoRaMac.h"

constexpr const char *MacStatusStrings[] = {
    [LORAMAC_STATUS_OK] = "OK",
    [LORAMAC_STATUS_BUSY] = "Busy",
    [LORAMAC_STATUS_SERVICE_UNKNOWN] = "Service unknown",
    [LORAMAC_STATUS_PARAMETER_INVALID] = "Parameter invalid",
    [LORAMAC_STATUS_FREQUENCY_INVALID] = "Frequency invalid",
    [LORAMAC_STATUS_DATARATE_INVALID] = "Datarate invalid",
    [LORAMAC_STATUS_FREQ_AND_DR_INVALID] = "Frequency or datarate invalid",
    [LORAMAC_STATUS_NO_NETWORK_JOINED] = "No network joined",
    [LORAMAC_STATUS_LENGTH_ERROR] = "Length error",
    [LORAMAC_STATUS_REGION_NOT_SUPPORTED] = "Region not supported",
    [LORAMAC_STATUS_SKIPPED_APP_DATA] = "Skipped APP data",
    [LORAMAC_STATUS_DUTYCYCLE_RESTRICTED] = "Duty-cycle restricted",
    [LORAMAC_STATUS_NO_CHANNEL_FOUND] = "No channel found",
    [LORAMAC_STATUS_NO_FREE_CHANNEL_FOUND] = "No free channel found",
    [LORAMAC_STATUS_BUSY_BEACON_RESERVED_TIME] = "Busy beacon reserved time",
    [LORAMAC_STATUS_BUSY_PING_SLOT_WINDOW_TIME] = "Busy ping-slot window time",
    [LORAMAC_STATUS_BUSY_UPLINK_COLLISION] = "Busy uplink collision",
    [LORAMAC_STATUS_CRYPTO_ERROR] = "Crypto error",
    [LORAMAC_STATUS_FCNT_HANDLER_ERROR] = "FCnt handler error",
    [LORAMAC_STATUS_MAC_COMMAD_ERROR] = "MAC command error",
    [LORAMAC_STATUS_CLASS_B_ERROR] = "ClassB error",
    [LORAMAC_STATUS_CONFIRM_QUEUE_ERROR] = "Confirm queue error",
    [LORAMAC_STATUS_ERROR] = "Unknown error",
};


constexpr const char *EventInfoStatusStrings[] = {
    [LORAMAC_EVENT_INFO_STATUS_OK] = "OK",
    [LORAMAC_EVENT_INFO_STATUS_ERROR] = "Error",
    [LORAMAC_EVENT_INFO_STATUS_TX_TIMEOUT] = "Tx timeout",
    [LORAMAC_EVENT_INFO_STATUS_RX1_TIMEOUT] = "Rx 1 timeout",
    [LORAMAC_EVENT_INFO_STATUS_RX2_TIMEOUT] = "Rx 2 timeout",
    [LORAMAC_EVENT_INFO_STATUS_RX1_ERROR] = "Rx1 error",
    [LORAMAC_EVENT_INFO_STATUS_RX2_ERROR] = "Rx2 error",
    [LORAMAC_EVENT_INFO_STATUS_JOIN_FAIL] = "Join failed",
    [LORAMAC_EVENT_INFO_STATUS_DOWNLINK_REPEATED] = "Downlink repeated",
    [LORAMAC_EVENT_INFO_STATUS_TX_DR_PAYLOAD_SIZE_ERROR] = "Tx DR payload size error",
    [LORAMAC_EVENT_INFO_STATUS_DOWNLINK_TOO_MANY_FRAMES_LOSS] = "Downlink too many frames loss",
    [LORAMAC_EVENT_INFO_STATUS_ADDRESS_FAIL] = "Address fail",
    [LORAMAC_EVENT_INFO_STATUS_MIC_FAIL] = "MIC fail",
    [LORAMAC_EVENT_INFO_STATUS_MULTICAST_FAIL] = "Multicast fail",
    [LORAMAC_EVENT_INFO_STATUS_BEACON_LOCKED] = "Beacon locked",
    [LORAMAC_EVENT_INFO_STATUS_BEACON_LOST] = "Beacon lost",
    [LORAMAC_EVENT_INFO_STATUS_BEACON_NOT_FOUND] = "Beacon not found"
};


#endif
