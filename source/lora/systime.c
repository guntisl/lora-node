
#include "sys_time.h"       // microtik
#include "systime.h"        // lora system time headr

SysTime_t unix_time_offset;

SysTime_t SysTimeGetMcuTime() {
    SysTime_t cur_time;
    SysTime up_time = getTime();
    cur_time.Seconds = up_time / 1000;
    cur_time.SubSeconds = (int16_t)(up_time % 1000);
    return cur_time;
}

SysTime_t SysTimeGet() {
    SysTime_t unix_time = SysTimeGetMcuTime();
    unix_time.Seconds += unix_time_offset.Seconds;
    unix_time.SubSeconds += unix_time_offset.SubSeconds;
    return unix_time;
}

SysTime_t SysTimeAdd(SysTime_t a, SysTime_t b) {
    a.Seconds += b.Seconds;
    a.SubSeconds += b.SubSeconds;
    if(a.SubSeconds >= 1000) {
        a.Seconds++;
        a.SubSeconds -= 1000;
    }
    return a;
}

SysTime_t SysTimeSub(SysTime_t a, SysTime_t b) {
    a.Seconds -= b.Seconds;
    a.SubSeconds -= b.SubSeconds;
    if(a.SubSeconds < 0) {
        a.Seconds--;
        a.SubSeconds += 1000;
    }
    return a;
}

void SysTimeSet(SysTime_t sysTime) {
    SysTime_t cur_time = SysTimeGetMcuTime();
    unix_time_offset.Seconds = sysTime.Seconds - cur_time.Seconds;
    unix_time_offset.SubSeconds = sysTime.SubSeconds - cur_time.SubSeconds;
}




