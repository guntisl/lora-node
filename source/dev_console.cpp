
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>
#include "tests.h"
#include <assert.h>
#include <config/nv_config.h>
#include "version.h"
#include "stm32wbxx.h"
#include "dev_console.h"
#include "dev_config.h"
#include "debug_config.h"
#include "debug_log.h"
#include <LoRaMac.h>
#include <sensors/sensor_data.h>
#include <sensors/env_sensors.h>

using namespace term;

constexpr size_t str_len(const char* str) {
    return *str == 0 ? 0 : 1 + str_len(str + 1);
}

#define CMD(str)                str, str_len(str)

int32_t consoleOutput(const char *str);

// --- console command list ---
bool cmdHelp(Terminal& term, char *arg_str);
bool cmdVersion(Terminal& term, char *arg_str);
bool cmdReset(Terminal& term, char *arg_str);

// APP
bool cmdAppConf(Terminal& term, char *arg_str);

// LORA
bool cmdLoraConf(Terminal& term, char *arg_str);
bool cmdLoraJoinEUI(Terminal& term, char *arg_str);
bool cmdLoraGetEUI(Terminal& term, char *arg_str);
bool cmdLoraGetKey(Terminal& term, char *arg_str);

// SENSORS
bool cmdEnvEnTrig(Terminal& term, char *arg_str);
bool cmdEnvSampleInt(Terminal& term, char *arg_str);
bool cmdEnvTempLev(Terminal& term, char *arg_str);
bool cmdEnvHumLev(Terminal& term, char *arg_str);
bool cmdEnvCO2Lev(Terminal& term, char *arg_str);
bool cmdEnvIllumLev(Terminal& term, char *arg_str);

bool cmdDioTrigPol(Terminal& term, char *arg_str);
bool cmdDioEnTrig(Terminal& term, char *arg_str);
bool cmdDioEnCntr(Terminal& term, char *arg_str);
bool cmdDioEnReportInt(Terminal& term, char *arg_str);

bool cmdTrigSensor(Terminal& term, char *arg_str);


constexpr term::CmdList console_cmd_list[] = {
    CONSOLE_TEST_CMD_LIST
    { CMD("help"), nullptr, cmdHelp },
    { CMD("?"), nullptr, cmdHelp },
    { CMD("v"), nullptr, cmdVersion },
    { CMD("app-conf"), "wait_n_reports", cmdAppConf },

    { CMD("lora-conf"), "otaa_mode public_netw duty_limit_enable adr_enable "
                        "retransmit_count dev_class drop_server_cnt",
                        cmdLoraConf },
    { CMD("lora-conf-joineui"), "B[7] B[6] B[n] B[0] | hex", cmdLoraJoinEUI },
    { CMD("lora-dev-eui"), " - read DevEUI", cmdLoraGetEUI },
    { CMD("lora-key"), " - read AppKey", cmdLoraGetKey },

    { CMD("env-en-trig"), "temp hum illum co2", cmdEnvEnTrig },
    { CMD("env-sample-int"), "t[min]", cmdEnvSampleInt },
    { CMD("env-temp-lev"), "min max", cmdEnvTempLev },
    { CMD("env-hum-lev"), "min max", cmdEnvHumLev },
    { CMD("env-co2-lev"), "min max", cmdEnvCO2Lev },
    { CMD("env-illum-lev"), "min max", cmdEnvIllumLev },
    { CMD("dio-trig-pol"), "In1 In2 In3 In4", cmdDioTrigPol },
    { CMD("dio-en-trig"), "In1 In2 In3 In4", cmdDioEnTrig },
    { CMD("dio-en-counter"), "In1 In2 In3 In4 Div1 Div2 Div3 Div4",
      cmdDioEnCntr },
    { CMD("trig-sens"), nullptr, cmdTrigSensor },

    { CMD("reset"), "", cmdReset }
};

const size_t cmd_count = sizeof(console_cmd_list) / sizeof(term::CmdList);

// global console for device
#ifdef EN_SERIAL_CONSOLE
term::Terminal serial_console(consoleOutput, console_cmd_list, cmd_count);
#endif


// --- console command implementation ---

bool cmdAppConf(Terminal& term, char *arg_str) {
    // app-conf wait_n_reports

    size_t wait_n_reports;
    auto count = sscanf(arg_str, "%d", &wait_n_reports);

    if(count == 1) {
        term.fmtSend(LF("write"));

        if(wait_n_reports > sensor::data::report_log.getCapacity()) {
            wait_n_reports = sensor::data::report_log.getCapacity();

            term.fmtSend(LF("rqstd > capacity"));
            return false;
        }
        app::user_conf.wait_n_reports = (uint8_t)wait_n_reports;

        // update lora config
        nvconf::writeBankSafe(NVC_BANK_APP, 0, app::user_conf);
    }
    term.fmtSend(LF("%d"), app::user_conf.wait_n_reports);
    return true;
}

bool cmdEnvEnTrig(Terminal& term, char *arg_str) {
    using namespace sensor;

    size_t temp = 0;
    size_t hum = 0;
    size_t illum = 0;
    size_t co2 = 0;

    if(sscanf(arg_str, "%d %d %d %d", &temp, &hum, &illum, &co2) == 4) {
        term.fmtSend(LF("write"));

        temp = temp ? 1u << env::EnvSensConfig::User::TEMP_TRIG_OBIT : 0;
        hum = hum ? 1u << env::EnvSensConfig::User::HUMID_TRIG_OBIT : 0;
        illum = illum ? 1u << env::EnvSensConfig::User::ILLUM_TRIG_OBIT : 0;
        co2 = co2 ? 1u << env::EnvSensConfig::User::CO2_TRIG_OBIT : 0;

        sensor::env::user_conf.en_triggers_bits = temp | hum | illum | co2;

        // update lora config
        nvconf::writeBankSafe(NVC_BANK_ENV_SENS, 0, env::user_conf);
    }

    term.fmtSend(LF("env trig bits %01X"), env::user_conf.en_triggers_bits);
    return true;
}

bool cmdEnvSampleInt(Terminal& term, char *arg_str) {
    using namespace sensor;

    size_t sampling_inerval = 5;

    if(sscanf(arg_str, "%d", &sampling_inerval) == 1) {
        term.fmtSend(LF("write"));

        sensor::env::user_conf.sampling_inerval = sampling_inerval;

        // update lora config
        nvconf::writeBankSafe(NVC_BANK_ENV_SENS, 0, env::user_conf);
    }

    term.fmtSend(LF("%d"), env::user_conf.sampling_inerval);
    return true;
}

bool cmdEnvTempLev(Terminal& term, char *arg_str) {
    using namespace sensor;

    size_t max, min;

    if(sscanf(arg_str, "%d %d", &min, &max) == 2) {
        term.fmtSend(LF("write"));

        if(max < min) {
            term.fmtSend(LF("max < min"));
            return false;
        }

        if(max > 1500 || min > 1500) {
            term.fmtSend(LF("max limit"));
            return false;
        }

        sensor::env::user_conf.temp_trig_high = max;
        sensor::env::user_conf.temp_trig_low = min;

        // update lora config
        nvconf::writeBankSafe(NVC_BANK_ENV_SENS, 0, env::user_conf);
    }

    term.fmtSend(LF("%d..%d"), sensor::env::user_conf.temp_trig_low,
                 sensor::env::user_conf.temp_trig_high);
    return true;
}

bool cmdEnvHumLev(Terminal& term, char *arg_str) {
    using namespace sensor;

    size_t max, min;

    if(sscanf(arg_str, "%d %d", &min, &max) == 2) {
        term.fmtSend(LF("write"));

        if(max < min) {
            term.fmtSend(LF("max < min"));
            return false;
        }

        if(max > 100 || min > 100) {
            term.fmtSend(LF("max limit"));
            return false;
        }

        sensor::env::user_conf.humid_trig_high = max;
        sensor::env::user_conf.humid_trig_low = min;

        // update lora config
        nvconf::writeBankSafe(NVC_BANK_ENV_SENS, 0, env::user_conf);
    }

    term.fmtSend(LF("%d..%d"), sensor::env::user_conf.humid_trig_low,
                 sensor::env::user_conf.humid_trig_high);
    return true;
}

bool cmdEnvCO2Lev(Terminal& term, char *arg_str) {
    using namespace sensor;

    size_t max, min;

    if(sscanf(arg_str, "%d %d", &min, &max) == 2) {
        term.fmtSend(LF("write"));

        if(max < min) {
            term.fmtSend(LF("max < min"));
            return false;
        }

        sensor::env::user_conf.co2_trig_high = max;
        sensor::env::user_conf.co2_trig_low = min;

        // update lora config
        nvconf::writeBankSafe(NVC_BANK_ENV_SENS, 0, env::user_conf);
    }

    term.fmtSend(LF("%d..%d"), sensor::env::user_conf.co2_trig_low,
                 sensor::env::user_conf.co2_trig_high);
    return true;
}

bool cmdEnvIllumLev(Terminal& term, char *arg_str) {
    using namespace sensor;

    size_t max, min;

    if(sscanf(arg_str, "%d %d", &min, &max) == 2) {
        term.fmtSend(LF("write"));

        if(max < min) {
            term.fmtSend(LF("max < min"));
            return false;
        }

        if(max > 85000 || min > 85000) {
            term.fmtSend(LF("max limit"));
            return false;
        }

        sensor::env::user_conf.illum_trig_high = max;
        sensor::env::user_conf.illum_trig_low = min;

        // update lora config
        nvconf::writeBankSafe(NVC_BANK_ENV_SENS, 0, env::user_conf);
    }

    term.fmtSend(LF("%d..%d"), sensor::env::user_conf.illum_trig_low,
        sensor::env::user_conf.illum_trig_high);
    return true;
}

bool cmdDioTrigPol(Terminal& term, char *arg_str) {
    using namespace sensor;

    size_t pol1, pol2, pol3, pol4;

    if(sscanf(arg_str, "%d %d %d %d", &pol1, &pol2, &pol3, &pol4) == 4) {
        term.fmtSend(LF("write"));
        // update lora config
    }

    return true;
}

bool cmdDioEnTrig(Terminal& term, char *arg_str) {
    using namespace sensor;

    size_t in1, in2, in3, in4;

    if(sscanf(arg_str, "%d %d %d %d", &in1, &in2, &in3, &in4) == 4) {
        term.fmtSend(LF("write"));


        // update lora config
    }

    return true;
}

bool cmdDioEnCntr(Terminal& term, char *arg_str) {
    using namespace sensor;

    size_t in1, in2, in3, in4;
    size_t div1, div2, div3, div4;

    if(sscanf(arg_str, "%d %d %d %d %d %d %d %d", &in1, &in2, &in3, &in4,
              &div1, &div2, &div3, &div4) == 8) {
        term.fmtSend(LF("write"));


        // update lora config
    }

    return true;
}

bool cmdDioEnReportInt(Terminal& term, char *arg_str) {
    using namespace sensor;

    size_t sampling_inerval = 5;

    if(sscanf(arg_str, "%d", &sampling_inerval) == 1) {
        term.fmtSend(LF("write"));


        // update lora config
    }

    return true;
}

bool cmdTrigSensor(Terminal& term, char *arg_str) {
    MUTE(0);
    sensor::data::trigSensorEvent(sensor::data::SensorTrigger::ENV_TRIG);
    return true;
}

bool cmdLoraConf(Terminal& term, char *arg_str) {
    size_t otaa_mode, public_netw, duty_limit_enable, adr_enable;
    size_t retransmit_count, dev_class, drop_server_cnt;

    auto count = sscanf(arg_str, "%d %d %d %d %d %d %d",
                        &otaa_mode, &public_netw, &duty_limit_enable,
                        &adr_enable, &retransmit_count, &dev_class,
                        &drop_server_cnt);

    if(count == 7) {
        term.fmtSend(LF("write"));

        // check if data valid
        if(dev_class > CLASS_C) {
            term.fmtSend(LF("invalid dev_class"));
            return false;
        }

        if(retransmit_count > 20) {
            term.fmtSend(LF("invalid retransmit_count"));
            return false;
        }

        lora::user_conf.otaa_mode = otaa_mode != 0;
        lora::user_conf.public_netw = public_netw != 0;
        lora::user_conf.duty_limit_enable = duty_limit_enable != 0;
        lora::user_conf.adr_enable = adr_enable != 0;
        lora::user_conf.dev_class = (uint8_t)dev_class;
        lora::user_conf.retransmit_count = (uint8_t)retransmit_count;
        lora::user_conf.drop_server_cnt = drop_server_cnt;

        // update lora config
        nvconf::writeBankSafe(NVC_BANK_LORA, 0, lora::user_conf);
    }

    term.fmtSend(LF("otaa_mode %d"), lora::user_conf.otaa_mode);
    term.fmtSend(LF("public_netw %d"), lora::user_conf.public_netw);
    term.fmtSend(LF("duty_limit_enable %d"), lora::user_conf.duty_limit_enable);
    term.fmtSend(LF("adr_enable %d"), lora::user_conf.adr_enable);
    term.fmtSend(LF("drop_server_cnt %d"), lora::user_conf.drop_server_cnt);
    term.fmtSend(LF("retransmit_count %d"), lora::user_conf.retransmit_count);
    term.fmtSend(LF("dev_class %d"), lora::user_conf.dev_class);
    return true;
}

bool cmdLoraJoinEUI(Terminal& term, char *arg_str) {
    size_t join_eui[8];

    auto count = sscanf(arg_str, "%X %X %X %X %X %X %X %X",
                        &join_eui[0], &join_eui[1], &join_eui[2], &join_eui[3],
                        &join_eui[4], &join_eui[5], &join_eui[6], &join_eui[7]);

    if(count == LORA_EUI_LEN) {
        term.fmtSend(LF("write"));
        // set new join EUI
        for(size_t i = 0; i < LORA_EUI_LEN; i++) {
            lora::user_conf.join_eui[i] = (uint8_t)join_eui[i];
        }

        // update lora config
        nvconf::writeBankSafe(NVC_BANK_LORA, 0, lora::user_conf);
    }

    term.fmtSend("JoinEUI: msb { ");
    for(auto &cur_join_eui : lora::user_conf.join_eui) {
        term.fmtSend("%02X ", cur_join_eui);
    }
    term.fmtSend(LF("}"));

    return true;
}

bool cmdLoraGetEUI(Terminal& term, char *arg_str) {
    term.fmtSend("DevEUI: msb { ");
    for (auto &byte : config.lora.fixed.dev_eui) {
        serial_console.fmtSend("%02X ", byte);
    }
    term.fmtSend("}\r\n");
    return true;
}

bool cmdLoraGetKey(Terminal& term, char *arg_str) {
    term.fmtSend("AppKey: msb { ");
    for(auto &byte : config.lora.fixed.app_key)
    {
        serial_console.fmtSend("%02X ", byte);
    }
    term.fmtSend("}" NEWLINE);
    return true;
}



bool cmdHelp(Terminal& term, char *arg_str) {
    term.fmtSend("usage information" NEWLINE);
    term.fmtSend("command structure: cmd_name arg1 arg2 ... argn" NEWLINE);
    term.fmtSend("on = 1; off = 0" NEWLINE);
    term.fmtSend("command list:" NEWLINE);
    for(size_t i = 0; i < cmd_count; i++) {
        term.fmtSend("'");
        term.fmtSend(console_cmd_list[i].cmd_str);
        term.fmtSend("'");

        if(console_cmd_list[i].usage) {
            term.fmtSend(" usage: '");
            term.fmtSend(console_cmd_list[i].cmd_str);
            term.fmtSend(" ");
            term.fmtSend(console_cmd_list[i].usage);
            term.fmtSend("'" NEWLINE);
        }
        else {
            term.fmtSend(NEWLINE);
        }
    }
    return true;
}

bool cmdVersion(Terminal& term, char *arg_str) {
    serial_console.fmtSend("git sha = %s" NEWLINE, version.git_sha);
    return true;
}


bool cmdReset(Terminal& term, char *arg_str) {
    term.fmtSend("reseting device...");
    NVIC_SystemReset();
    return true;
}

int32_t consoleOutput(const char *str) {
    // redirect to serial port
    return write(0, str, strlen(str));
}
