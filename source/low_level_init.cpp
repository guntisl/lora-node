
#include "sys_config.h"
#include "sys_time.h"
#include "i2c_bus.h"
#include "debug_log.h"
#include "version.h"

extern "C" void resetSysClocks(void);

/**
 * @brief perform most basic initialization that constructors can use
 */
extern "C" void lowLevelInit() {
    // WARNING: order of configuration is important
    resetSysClocks();
    configMisc();
    configClocks();
    sys_time::startSysTimer();
    configPins();
    configUART();
    configI2C();
    LOG(LF(LINE_BREAK));
    LOG(LF(LINE_BREAK));
    LOG("LoRaNode8" NEWLINE);
    LOG("git-sha: %s" NEWLINE, version.git_sha);
    LOG(LF(LINE_BREAK));
}
