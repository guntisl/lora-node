
#ifndef misc_crypto_h
#define misc_crypto_h

#include <array>

namespace random {

void init();

uint32_t getRND();
uint32_t getTrueRND();
uint32_t getPseudoRND();
void seedRND(std::array<uint32_t, 4> &seed);

}

namespace crc {

uint16_t crc16_CCITT(const void *data, size_t len, uint16_t init = 0xFFFF);
uint32_t hash_djb2(void *data, size_t Len);

}


#endif

