
#include <stm32wbxx.h>
#include <stm32wbxx_ll_rng.h>
#include <random>
#include <config/nv_config.h>
#include <debug_config.h>
#include <sys/debug_log.h>
#include <config/dev_config.h>
#include "misc_crypto.h"


#define RNG_TIMEOUT                     1000

namespace random {

// George Marsaglia KISS generator implementation
uint32_t z = 362436069;
uint32_t w = 521288629;
uint32_t jsr = 123456789;
uint32_t jcong = 380116160;

static uint32_t znew() { return (z = 36969 * (z & 65535) + (z >> 16)); }
static uint32_t wnew() { return (w = 18000 * (w & 65535)+ (w >> 16)); }
static uint32_t MWC() { return ((znew() << 16) + wnew()); }
static uint32_t SHR3() { return (jsr ^= (jsr << 17), jsr ^= (jsr >> 13), jsr ^= (jsr << 5)); }
static uint32_t CONG() { return (jcong = 69069 * jcong + 1234567); }
static uint32_t KISS() { return ((MWC() ^ CONG()) + SHR3()); }


void init() {
    // read device stored seed
    bool valid;
    auto seed = nvconf::readBankSafe<uint32_t>(NVC_BANK_RND, 0, valid);
    if(!valid) {
        // init from defaults
        seed = config.rnd_seed;
    }

    // get HW rnd seed
    auto hw_rnd = getTrueRND();

    // mix both seeds
    seed = seed ^ hw_rnd;

    // update rnd generator
    z ^= seed & 0xFF;
    w ^= seed & 0xFF00;
    jsr ^= seed & 0xFF0000;
    jcong ^= seed & 0xFF000000;

    // make next seed
    seed = getRND();
    nvconf::writeBankSafe(NVC_BANK_RND, 0, seed);
}

uint32_t getRND() {
    return getPseudoRND();
}

uint32_t getTrueRND()
{
    sys_time::SysTime start_time = sys_time::getTime();

    LL_RNG_EnableClkErrorDetect(RNG);
    LL_RNG_Enable(RNG);

    // maximum 3 errors before throwing EXCEPTION
    for(size_t n = 0; n < 3; n++) {
        // wait data ready
        while (!LL_RNG_IsActiveFlag_DRDY(RNG)) {
            if(sys_time::getElapsed(start_time) > RNG_TIMEOUT) {
                FATAL_ERROR("RNG timeout");
            }
        }

        // check for error status
        if(!LL_RNG_IsActiveFlag_SEIS(RNG)) {
            LL_RNG_Disable(RNG);
            return LL_RNG_ReadRandData32(RNG);
        }

        // RNG error detected
        do {
            LL_RNG_ClearFlag_SEIS(RNG);
            // read 12 words
            for (size_t i = 0; i < 12; i++) {
                LL_RNG_ReadRandData32(RNG);
            }
            if(sys_time::getElapsed(start_time) > RNG_TIMEOUT) {
                FATAL_ERROR("RNG timeout");
            }
        } while (LL_RNG_IsActiveFlag_SEIS(RNG));
    }

    FATAL_ERROR("RNG generator failed");
}

void seedRND(std::array<uint32_t, 4> &seed) {
    z = seed[0];
    w = seed[1];
    jsr = seed[2];
    jcong = seed[3];
}

uint32_t getPseudoRND() {
    return KISS();
}


}

namespace crc {

//Lookup table for CRC-CCITT(XModem) calculation,
// generated <For Polynomial 0x1021>
const uint16_t CRC_CCITT_TABLE[256] = {
    0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
    0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
    0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
    0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
    0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
    0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
    0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
    0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
    0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
    0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
    0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
    0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
    0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
    0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
    0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
    0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
    0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
    0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
    0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
    0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
    0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
    0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
    0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
    0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
    0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
    0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
    0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
    0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
    0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
    0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
    0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
    0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};

uint16_t crc16_CCITT(const void *data, size_t len, uint16_t init)
{
    const uint8_t* ptr = (uint8_t*)data;
    uint8_t local_data;
    uint16_t remainder = init;
    uint32_t i;

    for(i = 0; i < len; i++)
    {
        local_data = ptr[i] ^ (remainder >> 8);
        remainder = CRC_CCITT_TABLE[local_data] ^ (remainder << 8);
    }
    return remainder;
}

uint32_t hash_djb2(void *data, size_t Len)
{
    uint8_t* ptr = (uint8_t* )data;
    uint32_t hash = 5381;
    uint32_t c;

    while (Len-- > 0)
    {
        c = *ptr++;
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
    }

    return hash;
}

}










