
#ifndef version_h
#define version_h

#include <stdint-gcc.h>

struct VersionInfo {
    const char *git_sha;        //!< git commit sha
};

struct ChipID {
    uint32_t ID96_1;
    uint32_t ID96_2;
    uint32_t ID96_3;
    uint64_t FLASH_UID64;
};

ChipID getChipUID();

extern const VersionInfo version;

#endif
