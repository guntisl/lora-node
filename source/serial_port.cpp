
#include <stm32wbxx_ll_usart.h>
#include <cstring>
#include "sys_defines.h"
#include "serial_port.h"
#include "sys_time.h"
#include "debug_log.h"
#include "dev_console.h"

#define TIMEOUT                 1024u

/// device has 8 byte FIFO
#define RX_BUFFER_SIZE          8

namespace serial {

static void tx_send(const uint8_t *ptr, uint16_t len);
static void procRxData();

int write(const uint8_t *ptr, uint16_t len) {
    tx_send(ptr, len);
    return len;
}

int print_str(const char* str) {
    uint16_t str_len = strlen(str);
    tx_send((const uint8_t*)str, str_len);
    return str_len;
}

static void tx_send(const uint8_t *ptr, uint16_t len) {
    // wait for previous transfer to complete
    auto start = sys_time::getTime();
    while(!LL_USART_IsActiveFlag_TC(EXT_UART)) {
        if(sys_time::getTime() - start > TIMEOUT) {
            FATAL_ERROR("USART1 tx failed. TC error");
        }
    }

    while (len > 0) {
        // wait free space for TX
        start = sys_time::getTime();
        while(!LL_USART_IsActiveFlag_TXE(EXT_UART)) {
            if(sys_time::getTime() - start > TIMEOUT) {
                FATAL_ERROR("USART1 tx failed. TXE error");
            }
        }
        LL_USART_TransmitData8(EXT_UART, *ptr);
        len--;
        ptr++;
    }
}

extern "C" void USART1_IRQHandler() {
    if (LL_USART_IsActiveFlag_RTO(EXT_UART)) {
        LL_USART_ClearFlag_RTO(EXT_UART);
        procRxData();
    }
    if (LL_USART_IsActiveFlag_RXFT(EXT_UART)) {
        procRxData();
    }
}

/**
 * @brief Read out all UART rx data and send to all recipients
 */
static void procRxData() {
    static uint8_t rx_buffer[RX_BUFFER_SIZE];
    size_t idx = 0;

    while (LL_USART_IsActiveFlag_RXNE_RXFNE(EXT_UART)) {
        rx_buffer[idx] = LL_USART_ReceiveData8(EXT_UART);
        idx++;
        if (idx >= RX_BUFFER_SIZE) {
#ifdef EN_SERIAL_CONSOLE
            serial_console.inputText(rx_buffer, idx);
#endif
            // add other recipients of serial port data here
            idx = 0;
        }
    }
    if (idx > 0) {
#ifdef EN_SERIAL_CONSOLE
        serial_console.inputText(rx_buffer, idx);
#endif
        // add other recipients of serial port data here
    }
}

}
