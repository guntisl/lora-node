
#include <debug_config.h>
#include <debug_log.h>
#include <led.h>
#include <sys_time.h>
#include <stm32wbxx_ll_tim.h>
#include <lora/lora.h>
#include <cstring>
#include <version.h>
#include <sys_config.h>
#include "dev_config.h"
#include "tests.h"

#define TEST_TIMER                  TIM1
#define LORA_PROC_PERIOD_ms         15

namespace tests {


bool cmdTestLoraMac(term::Terminal& term, char *arg_str) {
    bool on_off = true;
    MUTE(0);
    MSG_WARN(TST, "lora test can't be stopped - issue reset cmd");

    // enable disable lora mac
    if(on_off) {
        lora::init();
    }
    else {
        // TODO: implement disable lora
    }
    return true;
}


}
