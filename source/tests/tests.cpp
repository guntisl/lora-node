
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <sensors/sensor_data.h>
#include "radio.h"
#include "sys_time.h"
#include "nv_config.h"
#include "dev_config.h"
#include "HDC2010.h"
#include "debug_log.h"
#include "tests.h"

extern "C" {
#include "board.h"
}

using namespace term;

namespace tests {

void initTests() {
    sensor::hdc2010::init();
//    sensor::digio::init();
}


bool cmdLoraRxRssi(Terminal& term, char *arg_str) {
    uint32_t time_s = 0;
    if(strlen(arg_str) > 0) {
        time_s = strtoul(arg_str, nullptr, 10);
    }
    return tests::sx1261_rxRssiTest(time_s);
}

bool cmdMeasTempHum(term::Terminal& term, char *arg_str) {
    term.fmtSend("HDC2010 temperature/humidity sensor test" NEWLINE);

    auto measure_count = 1;
    if(strlen(arg_str) > 0) {
        measure_count = strtoul(arg_str, nullptr, 10);
    }

    term.fmtSend("%d measurements" NEWLINE, measure_count);

    do {
        sensor::hdc2010::startMeasure(config.sensors.hdc2010);

        sys_time::SysTime start_time = sys_time::getTime();
        while (!sensor::hdc2010::isDataReady()) {
            sys_time::delay(1);
            if (sys_time::getElapsed(start_time) > 20) {
                term.fmtSend("timeout" NEWLINE);
                return false;
            }
            MUTE(TERMINAL_LOG_MUTE_TIMEOUT);
        }

        auto env = sensor::hdc2010::getLastSample();
        term.fmtSend("t = %d.%03u h = %d.%03u" NEWLINE, env.temp_whole,
                     env.temp_mili_fraction, env.humidity_whole,
                     env.humidity_fraction);

        measure_count--;
        if(measure_count) {
            sys_time::delay(1000);
        }
    }while (measure_count > 0);

    return true;
}


bool cmdTestSwGasSensor(term::Terminal& term, char *arg_str) {
    using namespace sensor;

    int on_off = 0;
    int mode = 1;
    sscanf(arg_str, "%d %d", &on_off, &mode);

    term.fmtSend("CCS811 switch %s" NEWLINE, on_off ? "on" : "off");
    if(on_off) {
        ccs811::init(ccs811::Mode::OFF);
        ccs811::switchMode((ccs811::Mode)mode, false);
    }
    else {
        ccs811::switchMode(ccs811::Mode::OFF, false);
    }
    return true;
}

bool cmdTestSmplGasSensor(term::Terminal& term, char *arg_str) {
    using namespace sensor;

    if(ccs811::isDataReady()) {
        auto sample = ccs811::getLastSample();

        term.fmtSend("eCO2: %d; eTVOC: %d; valid: %d" NEWLINE,
                     sample.eCO2_ppm, sample.eTVOC_ppb, sample.is_valid);
        return true;
    }
    return false;
}

bool cmdGasDiscBaseline(term::Terminal &term, char *arg_str) {
    using namespace sensor;
    ccs811::discardBaseline();
    return true;
}

bool cmdSetTermMute(term::Terminal& term, char *arg_str) {
    int on_char = TERMINAL_LOG_MUTE_TIMEOUT;
    int on_cmd = CMD_DONE_MUTE_TIMEOUT;
    sscanf(arg_str, "%d %d", &on_char, &on_cmd);

    term.fmtSend("set terminal mute conf %d; %d" NEWLINE, on_char, on_cmd);
    term.setMuteTimeout(on_char, on_cmd);
    return true;
}

bool cmdTestLinkList(term::Terminal& term, char *arg_str) {
    term.fmtSend("Singly linked list test" NEWLINE);
    SinglyLinkedList<uint32_t> list;

    decltype(list)::Container item[10];

    for(size_t i = 0; i < 10; i++) {
        item[i].data = i;
    }

    if(list.size() != 0) {
        term.fmtSend("ERROR" NEWLINE);
        return false;
    }

    list.printState();
    list.pushBack(item[0]);
    list.printState();
    list.pushBack(item[1]);
    list.printState();
    list.pushBack(item[2]);
    list.printState();
    list.pushFront(item[3]);
    list.printState();
    list.pushFront(item[4]);
    list.printState();
    list.pushFront(item[5]);
    list.printState();
    list.pushSorted(item[7]);
    list.printState();
    list.pushSorted(item[8]);
    list.printState();
    list.pushSorted(item[6]);
    list.printState();
    list.pushFront(item[9]);
    list.printState();
    if(list.size() != 10) {
        term.fmtSend("ERROR" NEWLINE);
        return false;
    }

    list.remove(item[0]);
    list.printState();
    list.remove(item[9]);
    list.printState();
    list.remove(item[8]);
    list.printState();
    list.remove(item[7]);
    list.printState();
    list.remove(item[6]);
    list.printState();
    list.remove(item[5]);
    list.printState();
    list.remove(item[4]);
    list.printState();
    list.remove(item[3]);
    list.printState();
    list.remove(item[2]);
    list.printState();
    list.remove(item[1]);
    list.printState();
    if(list.size() != 0) {
        term.fmtSend("ERROR" NEWLINE);
        return false;
    }

    list.pushFront(item[9]);
    list.printState();
    if(list.size() != 1) {
        term.fmtSend("ERROR" NEWLINE);
        return false;
    }

    term.fmtSend("PASSED" NEWLINE);
    return true;
}

struct TimerData {
    sys_time::TimerElement timer;
    uint32_t period;
    uint32_t id;
    uint32_t repeats;
};

void timer_callback(size_t param) {
    TimerData *data = (TimerData *) param;
    if(data->repeats > 0) {
        sys_time::startTimer(&data->timer, data->period, timer_callback, param);
    }
    MSG_INFO(TST, "tim %d exp", data->id);
    if(data->repeats > 0) data->repeats--;
}

bool cmdTestSwTimers(term::Terminal& term, char *arg_str) {
    term.fmtSend("Low power SW timer test" NEWLINE);
    int timer_count = 3;
    int period = 50;
    int count = 10;

    sscanf(arg_str, "%d %d %d", &timer_count, &period, &count);

    MUTE(0);

    TimerData timer[timer_count];

    for(size_t i = 0; i < (uint32_t)abs(timer_count); i++) {
        timer[i].period = (uint32_t)abs(period);
        timer[i].id = i;
        timer[i].repeats = (uint32_t)abs(count);
        sys_time::startTimer(&timer[i].timer, (uint32_t) abs(period),
                             timer_callback, (size_t)&timer[i]);
    }

    // wait all timers to expire
    bool done;
    do {
        done = true;
        for(size_t i = 0; i < (uint32_t)timer_count; i++) {
            if(timer[i].repeats > 0 || sys_time::isRunning(&timer[i].timer)) {
                done = false;
            }
        }
    }while (!done);

    term.fmtSend("PASSED" NEWLINE);
    return true;
}


static RadioEvents_t RadioEvents;

bool sx1261_interfaceTest();

bool doAllTests() {
    sx1261_simpleTest();
    sx1261_pingPongTest(false, false);
    return true;
}

/**
 * @brief Test if SX1261 chip responds
 */
bool sx1261_simpleTest() {
    MSG_INFO(TST, "simple sx1261 chip test");
    MSG_INFO(TST, "init lora driver");
    BoardInitMcu();

    MSG_INFO(TST, "radio init");
    Radio.Init(&RadioEvents);

    auto status = Radio.GetStatus();
    MSG_INFO(TST, "state = %d", status);

    auto res = sx1261_interfaceTest();
    if(!res) return res;

    return res;
}

bool nvConfigTest() {
    MSG_INFO(TST, "non volatile configuration test");

    MSG_INFO(TST, "erasing all config");
    nvconf::configReset();

    MSG_INFO(TST, "write 1B from 0 to 600 in same location");
    MSG_INFO(TST, "will trigger page cleanup");

    for(size_t i = 0; i < 600; i++) {
        nvconf::write(0, &i, 2);
    }

    MSG_INFO(TST, "reading last value to check");
    uint16_t value;
    nvconf::read(0, &value, sizeof(value));

    if(value != 599) {
        MSG_ERROR(TST, "test failed");
        return false;
    }
    MSG_ERROR(TST, "test ok");


    MSG_INFO(TST, "write address 0..%d check if whole region will work",
             nvconf::getMemorySize());
    for(uint16_t i = 0; i < nvconf::getMemorySize(); i++) {
        // write LSB
        nvconf::write(i, &i, 1);
    }

    MSG_INFO(TST, "reading back to verify");
    for(uint16_t i = 0; i < nvconf::getMemorySize(); i++) {
        uint8_t rb_value;
        nvconf::read(i, &rb_value, sizeof(rb_value));
        if(rb_value != (i & 0xFF)) {
            MSG_ERROR(TST, "verify error addr = %08X, red %02X, expected %02X",
                i, rb_value, i & 0xFF);
            MSG_ERROR(TST, "test failed");
            return false;
        }
    }

    MSG_INFO(TST, "verify ok");
    return true;
}

bool nvConfigBankTest() {
    MSG_INFO(TST, "non volatile configuration test with banks");

    MSG_INFO(TST, "erasing all config");
    nvconf::configReset();

    MSG_INFO(TST, "write same address diff banks");

    uint32_t value = 1;
    nvconf::writeBank(NVC_BANK_MAIN, 0, &value, sizeof(value));
    value = 2;
    nvconf::writeBank(NVC_BANK_LORA_MAC, 0, &value, sizeof(value));

    uint32_t val_bank_1, val_bank_2;
    MSG_INFO(TST, "read to check");

    nvconf::readBank(NVC_BANK_MAIN, 0, &val_bank_1, sizeof(val_bank_1));
    nvconf::readBank(NVC_BANK_LORA_MAC, 0, &val_bank_2, sizeof(val_bank_2));

    if(val_bank_1 != 1 && val_bank_2 != 2) {
        MSG_ERROR(TST, "test failed");
    }
    MSG_INFO(TST, "verify ok");
    return true;
}


bool sx1261_interfaceTest() {
    MSG_INFO(TST, "register read-write-read test");

    uint16_t address = 0x06C1;
    uint8_t value_wr = 0;
    uint8_t value_rd = 0;

    for(size_t i = 0; i < 5; i++) {
        MSG_INFO(TST, "write <%04X> = %01X", address, value_wr);
        Radio.Write(address, value_wr);

        value_rd = Radio.Read(address);
        MSG_INFO(TST, "read <%04X> = %01X", address, value_rd);

        if(value_rd != value_wr) {
            MSG_ERROR(TST, "register read/write failed");
            return false;
        }

        value_wr++;
    }

    return true;
}


}

