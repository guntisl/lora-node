
#ifndef tests_h
#define tests_h

#include <cstdint>
#include <terminal.h>

namespace tests {

bool doAllTests();
bool sx1261_simpleTest();

bool sx1261_rxTest(bool on_off, uint32_t frequency, uint32_t bw, uint32_t dr, uint32_t cr);
bool sx1261_cwTest(bool on_off, int8_t out_pwr, uint32_t frequency);
bool sx1261_rxRssiTest(size_t time_s);

void lora_macTest(bool on_off);

bool sx1261_pingPongTest(bool use_modem_lora, bool is_master);
bool nvConfigTest();
bool nvConfigBankTest();


void initTests();

bool cmdLoraSetRx(term::Terminal& term, char *arg_str);
bool cmdLoraSetCw(term::Terminal& term, char *arg_str);
bool cmdLoraSetCwSeq(term::Terminal& term, char *arg_str);
bool cmdLoraTxSeqTest(term::Terminal& term, char *arg_str);
bool cmdLoraPingPong(term::Terminal& term, char *arg_str);
bool cmdLoraPing(term::Terminal& term, char *arg_str);
bool cmdLoraRxRssi(term::Terminal& term, char *arg_str);
bool cmdMeasTempHum(term::Terminal& term, char *arg_str);
bool cmdTestLinkList(term::Terminal& term, char *arg_str);
bool cmdTestSwTimers(term::Terminal& term, char *arg_str);
bool cmdTestLoraMac(term::Terminal& term, char *arg_str);
bool cmdTestSwGasSensor(term::Terminal& term, char *arg_str);
bool cmdTestSmplGasSensor(term::Terminal& term, char *arg_str);
bool cmdGasDiscBaseline(term::Terminal &term, char *arg_str);
bool cmdSetTermMute(term::Terminal& term, char *arg_str);

#ifdef EN_TEST_CMDS
#define CONSOLE_TEST_CMD_LIST                                               \
    { CMD("tst-lora-rx"), "on|off freq[Hz] BW, DR, CR",                     \
        tests::cmdLoraSetRx },                                              \
    { CMD("tst-lora-cw"), "on|off pwr[dBm] freq[Hz]",                       \
        tests::cmdLoraSetCw },                                              \
    { CMD("tst-lora-cws"), "pwr[dBm] f_start[Hz] f_stop[Hz] step[Hz]"       \
        "time[ms]", tests::cmdLoraSetCwSeq },                               \
    { CMD("tst-lora-tx"), "pwr[dBm] f_start[Hz] f_stop[Hz] step[Hz]"        \
        "time[ms] BW, DR, CR", tests::cmdLoraTxSeqTest },                   \
    { CMD("tst-lora-ping"), "pwr[dBm] f[Hz] BW, DR, CR",                    \
        tests::cmdLoraPing },                                               \
    { CMD("tst-lora-rx-rssi"), "time[s]", tests::cmdLoraRxRssi },           \
    { CMD("tst-lora-ppt"), "time[s]", tests::cmdLoraPingPong },             \
    { CMD("tst-lora-mac"), "", tests::cmdTestLoraMac },                     \
    { CMD("tst-th"), "count", tests::cmdMeasTempHum },                      \
    { CMD("tst-list"), "", tests::cmdTestLinkList },                        \
    { CMD("tst-swtim"), "N T[ms] reps", tests::cmdTestSwTimers },           \
    { CMD("tst-co2-sw"), "on|off mode", tests::cmdTestSwGasSensor },        \
    { CMD("tst-co2-samp"), "", tests::cmdTestSmplGasSensor },               \
    { CMD("tst-co2-del-bline"), "", tests::cmdGasDiscBaseline },            \
    { CMD("tst-set-mute"), "on_input on_cmd", tests::cmdSetTermMute },
#else
#define CONSOLE_TEST_CMD_LIST
#endif

void testLinkedList();
void testSwLpTimers();


}

#endif
