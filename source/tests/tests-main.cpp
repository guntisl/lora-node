
#include <cstdint>
#include <debug_config.h>
#include <debug_log.h>
#include <led.h>
#include <scheduler.h>
#include <tests.h>
#include <sensors/BUTTON.h>
#include <sensors/sensor_data.h>

STACK_ALIGN uint32_t app_stack[200];

void testApplication();


int main() {
    // set log levels for application
    SET_LEVEL(LORA, LEV_DEBUG);
    SET_LEVEL(SCO2, LEV_OFF);
    SET_LEVEL(TST, LEV_DEBUG);

    blinkLED(25, 5);

    MSG_INFO(TST, "Delay 2s to unbrick MCU");
    sys_time::delay(2000);

    MSG_INFO(TST, "Test software init");
    tests::initTests();

    sys::sch::startScheduler("tst-app", app_stack, sizeof(app_stack), 1, testApplication);
}

void testApplication() {
    MSG_INFO(TST, "Waiting commands");
    while (1) {
        sys::sch::threadSleep(1000, sys::PS_MODE_STOP2);
        blinkLED(100, 1);
    }
}


