
#include <debug_config.h>
#include <debug_log.h>
#include <led.h>
#include <sys_time.h>
#include <cstdio>
#include "tests.h"

extern "C" {
#include <sx126x/sx126x.h>
#include <string.h>
#include "board.h"
#include "gpio.h"
#include "delay.h"
#include "timer.h"
#include "radio.h"
}


#define RF_FREQUENCY                                868000000 // Hz

// defines for LORA modem mode
#define LORA_BANDWIDTH                              2         // [0: 125 kHz,
                                                              //  1: 250 kHz,
                                                              //  2: 500 kHz,
                                                              //  3: Reserved]
#define LORA_SPREADING_FACTOR                       7         // [SF7..SF12]
#define LORA_CODINGRATE                             1         // [1: 4/5,
                                                              //  2: 4/6,
                                                              //  3: 4/7,
                                                              //  4: 4/8]
#define LORA_PREAMBLE_LENGTH                        8         // Same for Tx and Rx
#define LORA_SYMBOL_TIMEOUT                         0         // Symbols
#define LORA_FIX_LENGTH_PAYLOAD_ON                  false
#define LORA_IQ_INVERSION_ON                        false


// defines for FSK modem mode
#define FSK_FDEV                                    25000     // Hz
#define FSK_DATARATE                                50000     // bps

#define FSK_BANDWIDTH                               100000    // Hz >> DSB in sx126x
#define FSK_AFC_BANDWIDTH                           166666    // Hz >> Unused in sx126x

#define FSK_PREAMBLE_LENGTH                         5         // Same for Tx and Rx
#define FSK_FIX_LENGTH_PAYLOAD_ON                   false


#define RX_TIMEOUT_VALUE                            1000

namespace tests {
/*!
 * Radio events function pointer
 */
static RadioEvents_t RadioEvents;

bool rx_mode = false;
volatile bool tx_done;
volatile bool tx_timeout;

void rxTestOnRxDone(uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr);
void rxTestOnRxError();
void txDone();
void txTimeout();

bool sx1261_cwTest(bool on_off, int8_t out_pwr, uint32_t frequency) {
    BoardInitMcu();

    Radio.Init(&RadioEvents);
    MSG_INFO(TST, "radio init done");

    if(out_pwr > 14) {
        MSG_WARN(TST, "max out power is 14 dBm");
        out_pwr = 14;
    }
    if (on_off) {
        MSG_INFO(TST, "enable sx1261 CW test, F=%d Hz, P=%d dBm", frequency,
            out_pwr);
        Radio.SetTxContinuousWave(frequency, out_pwr, 3600);
    }
    else {
        MSG_INFO(TST, "disable sx1261 CW test");
        Radio.Standby();
        MSG_INFO(TST, "radio in standby");
    }

    return true;
}

bool cmdLoraSetRx(term::Terminal& term, char *arg_str) {
    // on|off freq[Hz] BW, DR, CR
    int on_off = 0;
    int freq = 868100000;
    int lora_bw = 0;    // 125k
    int lora_dr = 12;   // SF12
    int lora_cr = 1;    // 4/5

    term.fmtSend(LF("description"));
    term.fmtSend(LF("BW [0: 125 kHz, 1: 250 kHz, 2: 500 kHz, 3: Reserved]"));
    term.fmtSend(LF("DR [6: 64, 7: 128, 8: 256, 9: 512, 10: 1024, 11: 2048, "
                    "12: 4096  chips]"));
    term.fmtSend(LF("CR [1: 4/5, 2: 4/6, 3: 4/7, 4: 4/8]"));

    sscanf(arg_str, "%d %d %d %d %d", &on_off, &freq, &lora_bw, &lora_dr,
        &lora_cr);

    term.fmtSend(LF("used settings:"));
    term.fmtSend(LF("freq %d Hz"), freq);
    term.fmtSend(LF("on/off = %d"), on_off);
    term.fmtSend(LF("BW %d"), lora_bw);
    term.fmtSend(LF("DR %d"), lora_dr);
    term.fmtSend(LF("CR %d"), lora_cr);

    return tests::sx1261_rxTest(on_off, freq, lora_bw, lora_dr, lora_cr);
}

bool cmdLoraSetCw(term::Terminal& term, char *arg_str) {
    int on_off = 0;
    int power = 0;
    int freq = 868000000;
    sscanf(arg_str, "%d %d %d", &on_off, &power, &freq);

    term.fmtSend(LF("used settings:"));
    term.fmtSend(LF("P=%d dBm"), power);
    term.fmtSend(LF("freq %d Hz"), freq);
    term.fmtSend(LF("on/off = %d"), on_off);

    return tests::sx1261_cwTest(on_off, power, freq);
}

bool cmdLoraSetCwSeq(term::Terminal& term, char *arg_str) {
    MUTE(TERMINAL_LOG_MUTE_TIMEOUT);

    int power = 0;
    int f_min = 868000000;
    int f_max = 869000000;
    int step = 100000;
    int step_time = 1000;

    sscanf(arg_str, "%d %d %d %d %d", &power, &f_min, &f_max, &step,
           &step_time);

    term.fmtSend(LF("used settings:"));
    term.fmtSend(LF("P=%d dBm"), power);
    term.fmtSend(LF("Start freq %d Hz"), f_min);
    term.fmtSend(LF("Stop freq %d Hz"), f_max);
    term.fmtSend(LF("Step size %d Hz"), step);
    term.fmtSend(LF("Dwell time %d ms"), step_time);

    if (f_max <= f_min) {
        return false;
    }

    size_t step_cnt = (f_max - f_min) / step;
    uint32_t freq = f_min;

    for(size_t i = 0; i < step_cnt; i++, freq += step) {
        MUTE(2 * step_time);
        term.fmtSend(LF("Step %d/%d at F = %d"), i + 1, step_cnt, freq);
        // set CW on
        tests::sx1261_cwTest(1, power, freq);
        sys_time::delay(step_time);
        tests::sx1261_cwTest(0, power, freq);
        sys_time::delay(10);
    }

    MUTE(0);

    return true;
}

bool cmdLoraTxSeqTest(term::Terminal& term, char *arg_str) {
    // pwr[dBm] f_start[Hz] f_stop[Hz] step[Hz] BW, DR, CR

    int power = 0;
    int lora_bw = 0;    // 125k
    int lora_dr = 12;   // SF12
    int lora_cr = 1;    // 4/5
    int f_min = 868000000;
    int f_max = 869000000;
    int step = 100000;
    int step_time = 1000;

    term.fmtSend(LF("description"));
    term.fmtSend(LF("BW [0: 125 kHz, 1: 250 kHz, 2: 500 kHz, 3: Reserved]"));
    term.fmtSend(LF("DR [6: 64, 7: 128, 8: 256, 9: 512, 10: 1024, 11: 2048, "
                    "12: 4096  chips]"));
    term.fmtSend(LF("CR [1: 4/5, 2: 4/6, 3: 4/7, 4: 4/8]"));

    sscanf(arg_str, "%d %d %d %d %d %d %d %d", &power, &f_min, &f_max, &step,
           &step_time, &lora_bw, &lora_dr, &lora_cr);

    term.fmtSend(LF("used settings:"));
    term.fmtSend(LF("P=%d dBm"), power);
    term.fmtSend(LF("Start freq %d Hz"), f_min);
    term.fmtSend(LF("Stop freq %d Hz"), f_max);
    term.fmtSend(LF("Step size %d Hz"), step);
    term.fmtSend(LF("Dwell time %d ms"), step_time);
    term.fmtSend(LF("BW %d"), lora_bw);
    term.fmtSend(LF("DR %d"), lora_dr);
    term.fmtSend(LF("CR %d"), lora_cr);

    // Target board initialization
    BoardInitMcu();

    // Radio initialization
    RadioEvents.TxDone = txDone;
    RadioEvents.TxTimeout = txTimeout;
    Radio.Init(&RadioEvents);
    Radio.SetPublicNetwork(true);
    Radio.Sleep();

    Radio.SetTxConfig(MODEM_LORA, power, 0, lora_bw, lora_dr, lora_cr, 1,
                      false, false, false, 10, false, 5000);

    if (f_max <= f_min) {
        return false;
    }

    size_t step_cnt = (f_max - f_min) / step;
    uint32_t freq = f_min;

    uint8_t dummy_data[256];
    memset(dummy_data, 0xAA, sizeof(dummy_data));

    for (size_t i = 0; i < step_cnt; i++, freq += step) {
        MUTE(20000);
        term.fmtSend("Step %d/%d at F = %d TX..", i + 1, step_cnt, freq);

//        auto start = sys_time::getTime();
/*
        do {
            tx_done = false;
            tx_timeout = false;

            Radio.SetChannel(freq);
            Radio.Send(dummy_data, sizeof(dummy_data));

            while (!tx_timeout && !tx_done) {
                if (Radio.IrqProcess != NULL) {
                    Radio.IrqProcess();
                }
            }
        } while (sys_time::getElapsed(start) < step_time);
*/
        term.fmtSend(LF("OK"));
    }

    MUTE(TERMINAL_LOG_MUTE_TIMEOUT);

    MUTE(0);

    return true;
}

bool cmdLoraPing(term::Terminal& term, char *arg_str) {
    // pwr[dBm] f[Hz] BW, DR, CR

    int power = 0;
    int freq = 868000000;
    int lora_bw = 0;    // 125k
    int lora_dr = 12;   // SF12
    int lora_cr = 1;    // 4/5

    term.fmtSend(LF("description"));
    term.fmtSend(LF("BW [0: 125 kHz, 1: 250 kHz, 2: 500 kHz, 3: Reserved]"));
    term.fmtSend(LF("DR [6: 64, 7: 128, 8: 256, 9: 512, 10: 1024, 11: 2048, "
                    "12: 4096  chips]"));
    term.fmtSend(LF("CR [1: 4/5, 2: 4/6, 3: 4/7, 4: 4/8]"));

    sscanf(arg_str, "%d %d %d %d %d", &power, &freq, &lora_bw, &lora_dr,
        &lora_cr);

    term.fmtSend(LF("used settings:"));
    term.fmtSend(LF("P=%d dBm"), power);
    term.fmtSend(LF("Freq %d Hz"), freq);
    term.fmtSend(LF("BW %d"), lora_bw);
    term.fmtSend(LF("DR %d"), lora_dr);
    term.fmtSend(LF("CR %d"), lora_cr);

    // Target board initialization
    BoardInitMcu();

    // Radio initialization
    RadioEvents.TxDone = txDone;
    RadioEvents.TxTimeout = txTimeout;
    Radio.Init(&RadioEvents);
    Radio.SetPublicNetwork(true);
    Radio.Sleep();

    Radio.SetTxConfig(MODEM_LORA, power, 0, lora_bw, lora_dr, lora_cr, 8,
                      false, true, false, 0, false, 5000);

    uint8_t dummy_data[17];
    memset(dummy_data, 0xAA, sizeof(dummy_data));

    term.fmtSend("TX at F = %d TX..", freq);

    tx_done = false;
    tx_timeout = false;

    Radio.SetChannel(freq);
    Radio.Send(dummy_data, sizeof(dummy_data));

    while (!tx_timeout && !tx_done) {
        if (Radio.IrqProcess != NULL) {
            Radio.IrqProcess();
        }
    }

    term.fmtSend(LF("OK"));

    MUTE(TERMINAL_LOG_MUTE_TIMEOUT);

    MUTE(0);

    return true;
}

bool sx1261_rxTest(bool on_off, uint32_t frequency, uint32_t bw, uint32_t dr,
        uint32_t cr) {

    // Target board initialization
    BoardInitMcu();

    // Radio initialization
    RadioEvents.RxDone = rxTestOnRxDone;
    RadioEvents.RxTimeout = rxTestOnRxError;
    Radio.Init(&RadioEvents);
    Radio.SetPublicNetwork(true);
    Radio.Sleep();

    Radio.SetChannel(frequency);

    Radio.SetRxConfig(MODEM_LORA, bw, dr, cr, 0, LORA_PREAMBLE_LENGTH,
                      LORA_SYMBOL_TIMEOUT, LORA_FIX_LENGTH_PAYLOAD_ON,
                      0, false, 0, 0, LORA_IQ_INVERSION_ON, true);

    if (on_off) {
        MSG_INFO(TST, "enable sx1261 rx test, F=%d Hz", frequency);
        Radio.Rx(RX_TIMEOUT_VALUE);
        MSG_INFO(TST, "reception is on");
        blinkLED(100);
        rx_mode = true;

        while (1) {
            if (Radio.IrqProcess != NULL) {
                Radio.IrqProcess();
            }
        }
    }
    else {
        MSG_INFO(TST, "disable sx1261 rx test");
        Radio.Standby();
        MSG_INFO(TST, "radio in standby");
        blinkLED(100);
    }

    return true;
}

bool sx1261_rxRssiTest(size_t time_s) {
    if(rx_mode) {
        MSG_INFO(TST, "RSSI = %d dBm", Radio.Rssi(MODEM_LORA));
        for(size_t i = 0; i < time_s * 2; i++) {
            sys_time::delay(500);
            MSG_INFO(TST, "RSSI = %d dBm", Radio.Rssi(MODEM_LORA));
        }
        return true;
    }
    else {
        MSG_ERROR(TST, "must be in rx mode");
    }
    return false;
}


void rxTestOnRxDone(uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr) {
    blinkLED(200);
    MSG_WARN(TST, "RX [RSSI %d, SNR %d, len %d]", rssi, snr, size);
    HEX_MSG(LEV_WARN, TST, payload, size);
}

void rxTestOnRxError() {
    blinkLED(50);
    MSG_ERROR(TST, "radio error");
}

void txDone() {
    tx_done = true;
}

void txTimeout() {
    tx_timeout = true;
}



}
