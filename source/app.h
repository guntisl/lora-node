
#ifndef app_h
#define app_h

#include <cstdlib>
#include <sys/scheduler.h>

namespace app {

struct __attribute__((aligned(4))) Config {
    struct __attribute__((aligned(4))) User {
        uint8_t wait_n_reports;
    };

    User user;
};

extern Config::User user_conf;
extern STACK_ALIGN uint32_t app_stack[200];
void sendGreeting();
void sendReports();

void application();

}

#endif

