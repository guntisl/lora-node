/**
 ******************************************************************************
 * @file    tm.c
 * @author  MCD Application Team
 * @brief   Transparent mode
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */


/* Includes ------------------------------------------------------------------*/
#include "app_common.h"

#include "tl.h"
#include "mbox_def.h"
#include "lhci.h"
#include "shci.h"
#include "stm_list.h"
#include "scheduler.h"
#include "tm.h"
#include "lpm.h"
#include "shci_tl.h"

#include "vcp.h"

/* Private typedef -----------------------------------------------------------*/
/* Private defines -----------------------------------------------------------*/
/* Private macros ------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
PLACE_IN_SECTION("MB_MEM2") ALIGN(4) static TL_CmdPacket_t BleCmdBuffer;
PLACE_IN_SECTION("MB_MEM2") ALIGN(4) static uint8_t HciAclDataBuffer[sizeof(TL_PacketHeader_t) + 5 + 251];

static TL_CmdPacket_t SysLocalCmd;
static tListNode HostTxQueue;
static TL_EvtPacket_t *pTxToHostPacket;
static MB_RefTable_t * p_RefTable;
static uint8_t SysLocalCmdStatus;


static uint8_t VcpRxBuffer[sizeof(TL_CmdSerial_t)]; /* Received Data over USB are stored in this buffer */
static uint8_t VcpTxBuffer[sizeof(TL_EvtPacket_t) + 254]; /* Transmit buffer over USB */

/* Global variables ----------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
static void HostTxCb( void );
static void TxVcpDone( void );
static void TM_SysLocalCmd( void );
static void TM_TxToHost( void );
static void TM_BleEvtRx( TL_EvtPacket_t *phcievt );
static void TM_AclDataAck( void );

/* Functions Definition ------------------------------------------------------*/

void TM_Init( void  )
{
  TL_BLE_InitConf_t tl_ble_init_conf;
  uint32_t ipccdba;
  SHCI_C2_Ble_Init_Cmd_Packet_t ble_init_cmd_packet =
      {
          {{0,0,0}},                              /**< Header unused */
          {0,                                  /** pBleBufferAddress not used */
          0,                                  /** BleBufferSize not used */
          CFG_BLE_NUM_GATT_ATTRIBUTES,
          CFG_BLE_NUM_GATT_SERVICES,
          CFG_BLE_ATT_VALUE_ARRAY_SIZE,
          CFG_BLE_NUM_LINK,
          CFG_BLE_DATA_LENGTH_EXTENSION,
          CFG_BLE_PREPARE_WRITE_LIST_SIZE,
          CFG_BLE_MBLOCK_COUNT,
          CFG_BLE_MAX_ATT_MTU,
          CFG_BLE_SLAVE_SCA,
          CFG_BLE_MASTER_SCA,
          CFG_BLE_LSE_SOURCE,
          CFG_BLE_MAX_CONN_EVENT_LENGTH,
          CFG_BLE_HSE_STARTUP_TIME,
          CFG_BLE_VITERBI_MODE,
          CFG_BLE_LL_ONLY,
          0},
      };

  ipccdba = READ_BIT( FLASH->IPCCBR, FLASH_IPCCBR_IPCCDBA );
  p_RefTable = (MB_RefTable_t*)((ipccdba<<2) + SRAM2A_BASE);

  tl_ble_init_conf.p_cmdbuffer = (uint8_t*)&BleCmdBuffer;
  tl_ble_init_conf.p_AclDataBuffer = HciAclDataBuffer;
  tl_ble_init_conf.IoBusEvtCallBack = TM_BleEvtRx;
  tl_ble_init_conf.IoBusAclDataTxAck = TM_AclDataAck;
  TL_BLE_Init( (void*) &tl_ble_init_conf );

  LPM_SetStopMode(1 << CFG_LPM_APP, LPM_StopMode_Dis);

  SysLocalCmdStatus = 0;

  SHCI_C2_BLE_Init( &ble_init_cmd_packet );

  SCH_RegTask(CFG_TASK_SYS_LOCAL_CMD_ID, TM_SysLocalCmd);
  SCH_RegTask( CFG_TASK_BLE_HCI_CMD_ID, (void (*)( void )) TL_BLE_SendCmd);
  SCH_RegTask(CFG_TASK_TX_TO_HOST_ID, TM_TxToHost);
  SCH_RegTask( CFG_TASK_SYS_HCI_CMD_ID, (void (*)( void )) TL_SYS_SendCmd);
  SCH_RegTask( CFG_TASK_HCI_ACL_DATA_ID, (void (*)( void )) TL_BLE_SendAclData);

  pTxToHostPacket = 0;

  LST_init_head(&HostTxQueue);

  VCP_Init(&VcpTxBuffer[0], &VcpRxBuffer[0]);

  return;
}

void TM_SysCmdRspCb (TL_EvtPacket_t * p_cmd_resp)
{
  if(SysLocalCmdStatus != 0)
  {
    SysLocalCmdStatus = 0;
    SCH_SetEvt( 1<< CFG_IDLEEVT_SYSTEM_HCI_CMD_EVT_RSP_ID );
  }
  else
  {
    LST_insert_tail (&HostTxQueue, (tListNode *)p_cmd_resp);

    SCH_SetTask( 1<<CFG_TASK_TX_TO_HOST_ID,CFG_SCH_PRIO_0);
  }

  return;
}


/*************************************************************
 *
 * EXTERN FUNCTIONS
 *
 *************************************************************/
/**
 * @brief  CDC_Itf_DataRx
 *         Data received over USB OUT endpoint are sent over CDC interface
 *         through this function.
 * @param  Buf: Buffer of data to be transmitted
 * @param  Len: Number of data received (in bytes)
 * @retval Result of the operation: USBD_OK if all operations are OK else USBD_FAIL
 */
void VCP_DataReceived( uint8_t* Buf , uint32_t *Len )
{
  uint8_t *pdest_buffer;
  uint8_t packet_indicator;

  packet_indicator = Buf[0];

  switch (packet_indicator)
  {
    case TL_ACL_DATA_PKT_TYPE:
      pdest_buffer = (uint8_t*) &(((TL_AclDataPacket_t*)(p_RefTable->p_ble_table->phci_acl_data_buffer))->AclDataSerial);
      memcpy(pdest_buffer, Buf, *Len);
      SCH_SetTask(1 << CFG_TASK_HCI_ACL_DATA_ID, CFG_SCH_PRIO_0);
      break;

    case TL_SYSCMD_PKT_TYPE:
      pdest_buffer = (uint8_t*) &(((TL_CmdPacket_t*)(p_RefTable->p_sys_table->pcmd_buffer))->cmdserial);
      memcpy(pdest_buffer, Buf, *Len);
      SCH_SetTask(1 << CFG_TASK_SYS_HCI_CMD_ID, CFG_SCH_PRIO_0);
      break;

    case TL_LOCCMD_PKT_TYPE:
      pdest_buffer = (uint8_t*) &SysLocalCmd.cmdserial;
      memcpy(pdest_buffer, Buf, *Len);
      SCH_SetTask(1 << CFG_TASK_SYS_LOCAL_CMD_ID, CFG_SCH_PRIO_0);
      break;

    default:
      pdest_buffer = (uint8_t*) &(((TL_CmdPacket_t*)(p_RefTable->p_ble_table->pcmd_buffer))->cmdserial);
      memcpy(pdest_buffer, Buf, *Len);
      SCH_SetTask(1 << CFG_TASK_BLE_HCI_CMD_ID, CFG_SCH_PRIO_0);
      break;
  }

  return;
}

/*************************************************************
 *
 * LOCAL FUNCTIONS
 *
 *************************************************************/

static void TM_TxToHost( void )
{
  SCH_PauseTask(1 << CFG_TASK_TX_TO_HOST_ID);

  LST_remove_head(&HostTxQueue, (tListNode **) &pTxToHostPacket);

  if(pTxToHostPacket->evtserial.type == TL_ACL_DATA_PKT_TYPE)
  {
    VCP_SendData((uint8_t *) &pTxToHostPacket->evtserial, ((TL_AclDataPacket_t *)pTxToHostPacket)->AclDataSerial.length + 5, TxVcpDone);
  }
  else
  {
    VCP_SendData((uint8_t *)&((TL_AclDataPacket_t *)pTxToHostPacket)->AclDataSerial, pTxToHostPacket->evtserial.evt.plen + TL_EVT_HDR_SIZE, TxVcpDone);
  }

  return;
}

static void TM_SysLocalCmd( void )
{
  switch (SysLocalCmd.cmdserial.cmd.cmdcode)
  {
    case LHCI_OPCODE_C1_WRITE_REG:
      LHCI_C1_Write_Register(&SysLocalCmd);
      break;

    case LHCI_OPCODE_C1_READ_REG:
      LHCI_C1_Read_Register(&SysLocalCmd);
      break;

    case LHCI_OPCODE_C1_DEVICE_INF:
      LHCI_C1_Read_Device_Information(&SysLocalCmd);
      break;

    default:
      ((TL_CcEvt_t *)(((TL_EvtPacket_t*)&SysLocalCmd)->evtserial.evt.payload))->cmdcode = SysLocalCmd.cmdserial.cmd.cmdcode;
      ((TL_CcEvt_t *) (((TL_EvtPacket_t*) &SysLocalCmd)->evtserial.evt.payload))->payload[0] = 0x01;
      ((TL_CcEvt_t *) (((TL_EvtPacket_t*) &SysLocalCmd)->evtserial.evt.payload))->numcmd = 1;
      ((TL_EvtPacket_t*) &SysLocalCmd)->evtserial.type = TL_BLEEVT_PKT_TYPE;
      ((TL_EvtPacket_t*) &SysLocalCmd)->evtserial.evt.evtcode = TL_BLEEVT_CC_OPCODE;
      ((TL_EvtPacket_t*) &SysLocalCmd)->evtserial.evt.plen = TL_EVT_CS_PAYLOAD_SIZE;

      break;
  }

  LST_insert_tail(&HostTxQueue, (tListNode *) &SysLocalCmd);
  SCH_SetTask(1 << CFG_TASK_TX_TO_HOST_ID, CFG_SCH_PRIO_0);

  return;
}

static void HostTxCb( void )
{
  if( (pTxToHostPacket >= (TL_EvtPacket_t *)(p_RefTable->p_mem_manager_table->blepool)) && (pTxToHostPacket < ( (TL_EvtPacket_t *)((p_RefTable->p_mem_manager_table->blepool) + p_RefTable->p_mem_manager_table->blepoolsize))))
  {
    TL_MM_EvtDone(pTxToHostPacket);
  }

  if ( LST_is_empty( &HostTxQueue ) == FALSE )
  {
    SCH_SetTask(1 << CFG_TASK_TX_TO_HOST_ID, CFG_SCH_PRIO_0);
  }

  return;
}

static void TM_BleEvtRx( TL_EvtPacket_t *phcievt )
{
  LST_insert_tail (&HostTxQueue, (tListNode *)phcievt);

  SCH_SetTask( 1<<CFG_TASK_TX_TO_HOST_ID,CFG_SCH_PRIO_0);

  return;
}

static void TxVcpDone( void )
{
  HostTxCb();
  SCH_ResumeTask(1 << CFG_TASK_TX_TO_HOST_ID);

  return;
}

static void TM_AclDataAck( void )
{
  /**
   * The current implementation assumes the GUI will not send a new HCI ACL DATA packet before this ack is received
   * ( which means the CPU2 has handled the previous packet )
   * In order to implement a secure mechanism, it is required either
   * - a flow control with the GUI
   * - a local pool of buffer to store packets received from the GUI
   */
  return;
}

/*************************************************************
 *
 * WRAP FUNCTIONS
 *
 *************************************************************/
void shci_send( TL_CmdPacket_t * p_cmd, TL_EvtPacket_t * p_rsp )
{
  TL_CmdPacket_t *p_cmd_buffer;

  SysLocalCmdStatus = 1;

  p_cmd_buffer = (TL_CmdPacket_t *)(p_RefTable->p_sys_table->pcmd_buffer);

  memcpy( p_cmd_buffer, p_cmd, p_cmd->cmdserial.cmd.plen + sizeof( TL_PacketHeader_t ) + TL_CMD_HDR_SIZE );

  TL_SYS_SendCmd( 0, 0 );

  SCH_WaitEvt( 1<< CFG_IDLEEVT_SYSTEM_HCI_CMD_EVT_RSP_ID );

  memcpy( &(p_rsp->evtserial), &(p_cmd_buffer->cmdserial), ((TL_EvtPacket_t*)p_cmd_buffer)->evtserial.evt.plen + TL_BLEEVT_CS_PACKET_SIZE );

  return;
}



/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
